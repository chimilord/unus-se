if (typeof jQuery === "undefined") {
    throw new Error("UNUS-SE requires jQuery");
}

$.UnusSE = {};

$.UnusSE.options = {};

$(function() {
    'use strict';

    var $treeview = $('.treeview');
    $treeview.on('click', setCookieMenu);

    function setCookieMenu() {
        var $item = $(this).attr('id');
        Cookies.set('activeItem', $item);
    }

    function getCookieMenu() {
        var menuCookie = Cookies.get('activeItem');
        $('#' + menuCookie).addClass('active');
    }

    $("#calendar").datepicker({ language: 'es' });
    getCookieMenu();
});