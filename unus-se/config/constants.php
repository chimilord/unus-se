<?php

return [
    'CREATE' => 'create',
    'UPDATE' => 'update',
    'DELETE' => 'delete',
    'AUTH'   => 'auth',
    'email' => [
        'username' => 'unus.se.system@gmail.com',
        'noreply' => 'Unus-SE [no-reply]',
        'subject' => 'Notificación de nota'
    ]
];