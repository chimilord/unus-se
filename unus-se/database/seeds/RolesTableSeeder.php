<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
        if (App::environment() === 'production') {
            exit('I just stopped you getting fired. Love, Amo.');
        }
        DB::table('role')->truncate();
        Role::create([
            'id'            => 1,
            'name'          => 'Root',
            'description'   => 'Use this account with extreme caution. When using this account it is possible to cause irreversible damage to the system.'
        ]);
        Role::create([
            'id'            => 2,
            'name'          => 'Administrator',
            'description'   => 'Full access to create, edit, and update any.'
        ]);
        Role::create([
            'id'            => 3,
            'name'          => 'Manager',
            'description'   => 'Ability to create new documents and persons and catalogs, or edit and update any existing ones. Can not delete'
        ]);
        Role::create([
            'id'            => 4,
            'name'          => 'User',
            'description'   => 'A standard user that can list and search for any. No administrative features.'
        ]);
    }
}
