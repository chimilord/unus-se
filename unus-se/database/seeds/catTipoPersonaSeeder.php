<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\catTipoPersona;


class catTipoPersonaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment() === 'production') {
            exit('I just stopped you getting fired. Love, Amo.');
        }
        DB::table('catTipoPersonas')->truncate();
        catTipoPersona::create([
            'nombre'          => 'Diputado',
        ]);
        catTipoPersona::create([
            'nombre'          => 'Senador',
        ]);
         catTipoPersona::create([
            'nombre'          => 'Presidente Municipal',
        ]);
          catTipoPersona::create([
            'nombre'          => 'Gobernador',
        ]);
    }    
}


