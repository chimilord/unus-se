<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\catTipoEleccion;

class catTipoEleccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          if (App::environment() === 'production') {
            exit('I just stopped you getting fired. Love, Amo.');
        }
        DB::table('catTipoEleccion')->truncate();
        CatTipoEleccion::create([
            'name'          => 'Representación Proporcional',
        ]);
        CatTipoEleccion::create([
            'name'          => 'Mayoría Relativa',
        ]);
    }
}

