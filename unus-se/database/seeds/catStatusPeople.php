<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\CatStatusPerson;

class catStatusPeople extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment() === 'production') {
	        exit('I just stopped you getting fired. Love, Amo.');
        }
        DB::table('catStatusPeople')->truncate();
        CatStatusPerson::create([
            'estatus'          => 'Activo',
        ]);
        CatStatusPerson::create([
            'estatus'          => 'Baja',
        ]);
    }
}
