<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\catComisiones;

class catComisiones2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
 if (App::environment() === 'production') {
	        exit('I just stopped you getting fired. Love, Amo.');
        }
        DB::table('catComisiones')->truncate();
catComisiones::create(['nombre'=> 'Agricultura y Sistemas de Riesgo']);
catComisiones::create(['nombre'=> 'Agua Potable y Saneamiento']);
catComisiones::create(['nombre'=> 'Asuntos Indígenas']);
catComisiones::create(['nombre'=> 'Asuntos Frontera Norte']);
catComisiones::create(['nombre'=> 'Asuntos Frontera Sur-Sureste']);
catComisiones::create(['nombre'=> 'Asuntos Migratorios']);
catComisiones::create(['nombre'=> 'Atención a Grupos Vulnerables']);
catComisiones::create(['nombre'=> 'Cambio Climático']);
catComisiones::create(['nombre'=> 'Ciencia y Tecnología']);
catComisiones::create(['nombre'=> 'Competitividad']);
catComisiones::create(['nombre'=> 'Couminicaciones']);
catComisiones::create(['nombre'=> 'Cultura y Cinematrografía']);
catComisiones::create(['nombre'=> 'Defensa Nacional']);
catComisiones::create(['nombre'=> 'Deporte']);
catComisiones::create(['nombre'=> 'Derechos Humanos']);
catComisiones::create(['nombre'=> 'Derechos de la Niñez']);
catComisiones::create(['nombre'=> 'Desarrollo Metropolitano']);
catComisiones::create(['nombre'=> 'Desarrollo Municipal']);
catComisiones::create(['nombre'=> 'Desarrrollo Rural']);
catComisiones::create(['nombre'=> 'Desarrollo Social']);
catComisiones::create(['nombre'=> 'Desarrollo Urbano y Ordenamiento Territorial']);
catComisiones::create(['nombre'=> 'Distrito Federal']);
catComisiones::create(['nombre'=> 'Economía']);
catComisiones::create(['nombre'=> 'Educación Pública y Servicios Educativos']);
catComisiones::create(['nombre'=> 'Energía']);
catComisiones::create(['nombre'=> 'Fomento Cooperativo y Economía Social']);
catComisiones::create(['nombre'=> 'Fortalecimiento al Federalismo']);
catComisiones::create(['nombre'=> 'Ganadería']);
catComisiones::create(['nombre'=> 'Gobernación']);
catComisiones::create(['nombre'=> 'Hacienda y Crédito Público']);
catComisiones::create(['nombre'=> 'Igualdad de Género']);
catComisiones::create(['nombre'=> 'Infraestructura']);
catComisiones::create(['nombre'=> 'Jurisdiccional']);
catComisiones::create(['nombre'=> 'Justicia']);
catComisiones::create(['nombre'=> 'Juventud']);
catComisiones::create(['nombre'=> 'Marina']);
catComisiones::create(['nombre'=> 'Medio Ambiente y Recursos Naturales']);
catComisiones::create(['nombre'=> 'Pesca']);
catComisiones::create(['nombre'=> 'Población']);
catComisiones::create(['nombre'=> 'Presupuesto y Cuenta Pública']);
catComisiones::create(['nombre'=> 'Protección Civil']);
catComisiones::create(['nombre'=> 'Puntos Constitucionales']);
catComisiones::create(['nombre'=> 'Radio y Televisión']);
catComisiones::create(['nombre'=> 'Recursos Hidráulicos']);
catComisiones::create(['nombre'=> 'Reforma Agraria']);
catComisiones::create(['nombre'=> 'Relaciones Exteriores']);
catComisiones::create(['nombre'=> 'Régimen, Reglamentos y Prácticas Parlamentarias']);
catComisiones::create(['nombre'=> 'Salud']);
catComisiones::create(['nombre'=> 'Seguridad Pública']);
catComisiones::create(['nombre'=> 'Seguridad Social']);
catComisiones::create(['nombre'=> 'Trabajo y Previsión Social']);
catComisiones::create(['nombre'=> 'Transparencia y Anticorrupción']);
catComisiones::create(['nombre'=> 'Transporte']);
catComisiones::create(['nombre'=> 'Turismo']);
catComisiones::create(['nombre'=> 'Vigilancia de la Auditoría Superior de la Federación']);
    }
}
