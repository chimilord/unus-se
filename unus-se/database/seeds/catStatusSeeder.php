<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\CatStatus;

class catStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment() === 'production') {
            exit('I just stopped you getting fired. Love, Amo.');
        }
        DB::table('catStatuses')->truncate();
        CatStatus::create([
            'name'          => 'Pendiente',
        ]);
        CatStatus::create([
            'name'          => 'Aprobada',
        ]);
    }
}

