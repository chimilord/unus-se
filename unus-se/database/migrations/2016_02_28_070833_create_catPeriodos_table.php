<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatecatPeriodosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('catPeriodos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre', 255);
			$table->date('fechaInicio');
			$table->date('fechaFin');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('catPeriodos');
	}

}
