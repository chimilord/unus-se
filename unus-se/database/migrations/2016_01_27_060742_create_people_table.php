<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('person', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre', 60);
			$table->string('apellidos', 60);
			$table->string('fotografia', 255);
			$table->integer('idEstado');
			$table->integer('idDistrito');
			$table->string('localidad', 255);
			$table->integer('idStatusPerson');
			$table->string('suplente', 255);
			$table->integer('idPartido');
			$table->integer('idTipoEleccion');
			$table->string('profesion', 255);
			$table->string('trayectoriaAcademica', 255);
			$table->string('experienciaLegislativa', 1024);
			$table->string('experienciaPolitica', 1024);
			$table->string('experienciaEmpresarial', 1024);
			$table->string('experienciaOtra', 1024);
			$table->string('noticias', 1024);
			$table->string('organoGobierno', 1024);
			$table->string('direccion', 128);
			$table->string('email', 255);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('person');
	}

}
