<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePersonComite extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('person_comite', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('person_id')->unsigned();
            // Change this for person table
            $table->foreign('person_id')->references('id')->on('person');

            $table->integer('comite_id')->unsigned();

            $table->foreign('comite_id')->references('id')->on('comites');

            $table->integer('position_id')->unsigned();
            $table->foreign('position_id')->references('id')->on('positions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
