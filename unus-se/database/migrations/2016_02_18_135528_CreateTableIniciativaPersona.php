<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableIniciativaPersona extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('person_iniciativa', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('person_id')->unsigned();
            // Change this for person table
            $table->foreign('person_id')->references('id')->on('person');

            $table->integer('iniciativa_id')->unsigned();

            $table->foreign('iniciativa_id')->references('id')->on('initiatives');

            $table->integer('position_id')->unsigned();
            $table->foreign('position_id')->references('id')->on('positions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
