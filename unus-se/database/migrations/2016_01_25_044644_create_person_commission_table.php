<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonCommissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person_commission', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('person_id')->unsigned();
            // Change this for person table
            $table->foreign('person_id')->references('id')->on('people');
            $table->integer('comision_id')->unsigned();
            $table->foreign('comision_id')->references('id')->on('comisions');
            $table->foreign('position_id')->unsigned();
            $table->integer('position_id')->references('id')->on('positions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('person_commission');
    }
}
