<section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
        <div class="pull-left image">
            @if (Auth::user()->avatar)
                <img src="{{ URL::asset('uploads/user_' . Auth::user()->id . '.png') }}" class="user-image" alt="User Image">
            @else
                <img src="{{ URL::asset('uploads/fallback-user-avatar.png') }}" class="user-image" alt="User Image">
            @endif
        </div>
        <div class="pull-left info">
            <p>{!! Auth::user()->name !!}</p>
            <!-- Status -->
            <a href="#">
                <i class="fa fa-circle text-success"></i>
                @if (Auth::user()->role_id == 1)
                    Root
                @elseif(Auth::user()->role_id == 2)
                    Administrador
                @elseif(Auth::user()->role_id == 3)
                    Manager
                @elseif(Auth::user()->role_id == 4)
                    User
                @else
                    Who are you?
                @endif
            </a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
        <li class="header">Navegación principal</li>
        <!-- Optionally, you can add icons to the links -->
        <li><a href="{{ URL::to('home') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        @if (Auth::user()->role_id == 2)
            <li class="treeview" id="sidebarUser">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Usuarios</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ URL::to('register') }}"><i class="fa fa-circle-o"></i> <span>Registrar</span></a></li>
                    <li><a href="{{ URL::to('users') }}"><i class="fa fa-circle-o"></i> <span>Usuarios</span></a></li>
                </ul>
            </li>
        @endif
        <li class="treeview" id="sidebarSearch">
            <a href="#">
                <i class="fa fa-search"></i>
                <span>Búsquedas</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ URL::to('search/entity') }}"><i class="fa fa-circle-o"></i>Entidad</a></li>
                <li><a href="{{ URL::to('search') }}"><i class="fa fa-circle-o"></i>Persona</a></li>
            </ul>
        </li>
        <li class="treeview" id="sidebarCatalog">
            <a href="#">
                <i class="fa fa-cube"></i>
                <span>Catálogos</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ URL::to('catCamaras') }}"><i class="fa fa-circle-o"></i>Cámara</a></li>
                <li><a href="{{ URL::to('positions') }}"><i class="fa fa-circle-o"></i>Cargo | Posición</a></li>
                <li><a href="{{ URL::to('catComisiones') }}"><i class="fa fa-circle-o"></i>Comisión</a></li>
                <li><a href="{{ URL::to('catComites') }}"><i class="fa fa-circle-o"></i>Comité</a></li>
                <li><a href="{{ URL::to('catDistritos') }}"><i class="fa fa-circle-o"></i>Distrito</a></li>
                <li><a href="{{ URL::to('estados') }}"><i class="fa fa-circle-o"></i>Entidad Federativa</a></li>
                <li><a href="{{ URL::to('catStatusPeople') }}"><i class="fa fa-circle-o"></i>Estatus Persona</a></li>
                <li><a href="{{ URL::to('catStatus') }}"><i class="fa fa-circle-o"></i>Estatus Entidad</a></li>
                <li><a href="{{ URL::to('catOrganoGobiernos') }}"><i class="fa fa-circle-o"></i>Órgano de Gobierno</a></li>
                <li><a href="{{ URL::to('partidos') }}"><i class="fa fa-circle-o"></i>Partido Político</a></li>
                <li><a href="{{ URL::to('catPeriodos') }}"><i class="fa fa-circle-o"></i>Periódo</a></li>
                <li><a href="{{ URL::to('catTipoComisions') }}"><i class="fa fa-circle-o"></i>Tipo Comisión</a></li>
                <li><a href="{{ URL::to('catTipoEleccions') }}"><i class="fa fa-circle-o"></i>Tipo Elección</a></li>
                <li><a href="{{ URL::to('catTipoPersonas') }}"><i class="fa fa-circle-o"></i>Tipo Persona</a></li>
                <li><a href="{{ URL::to('catStatusTurnados') }}"><i class="fa fa-circle-o"></i>Turno</a></li>
            </ul>
        </li>
        <li class="treeview" id="sidebarEntity">
            <a href="#">
                <i class="fa fa-cubes"></i>
                <span>Entidades</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ URL::to('comisions') }}"><i class="fa fa-circle-o"></i>Comisión</a></li>
                <li><a href="{{ URL::to('comites') }}"><i class="fa fa-circle-o"></i>Comité</a></li>
                <li><a href="{{ URL::to('initiatives') }}"><i class="fa fa-circle-o"></i>Iniciativa</a></li>
                <li><a href="{{ URL::to('notes') }}"><i class="fa fa-circle-o"></i>Nota</a></li>
                <li><a href="{{ URL::to('organoGobiernos') }}"><i class="fa fa-circle-o"></i>Órgano de Gobierno</a></li>
                <li><a href="{{ URL::to('person') }}"><i class="fa fa-circle-o"></i>Persona</a></li>
            </ul>
        </li>
    </ul><!-- /.sidebar-menu -->
</section>