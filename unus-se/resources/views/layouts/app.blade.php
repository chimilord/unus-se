<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>UNUS - S(earch)E(ngine)</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="{{ URL::asset('dist/img/favicon.png') }}">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}">
    <!-- Icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Plugins -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{{ URL::asset('plugins/jQuery-tagEditor/jquery.tag-editor.css') }}">
    <!-- Theme -->
    <link rel="stylesheet" href="{{ URL::asset('dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('dist/css/skins/skin-blue.min.css') }}">
    <!-- Date picker -->
    <link rel="stylesheet" href="{{ URL::asset('plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('plugins/daterangepicker/daterangepicker-bs3.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('plugins/select2/select2.min.css') }}">
    <!-- Full calendar -->
    <link rel="stylesheet" href="{{ URL::asset('plugins/fullcalendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper" id="unusSe">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>US</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>UNUS</b> - SE</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-file-text-o"></i>
                            {{--<span class="label label-warning"></span>--}}
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">Notas</li>
                            <li>
                                <!-- Inner Menu: contains the notifications -->
                                <ul class="menu">
                                    <li><!-- start notification -->
                                        <a href="{{ URL::to('notes/create') }}">
                                            <i class="fa fa-pencil text-aqua"></i> Crear nota
                                        </a>
                                    </li><!-- end notification -->
                                </ul>
                            </li>
                            <li class="footer"><a href="{{ URL::to('notes') }}">Ver notas</a></li>
                        </ul>
                    </li>
                    <!-- Messages: style can be found in dropdown.less-->
                    <!-- Notifications Menu -->
                    <li class="dropdown notifications-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">{{ $countNotes }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">Notificaciones</li>
                            <li>
                                <!-- Inner Menu: contains the notifications -->
                                <ul class="menu">
                                    @foreach($userNotes as $userNote)
                                        <li>
                                            <a href="{{ URL::to('notes') }}/{{$userNote->id}}">
                                                @if ($userNote->prioridad == 0)
                                                    <i class="fa fa-warning text-gray"></i>
                                                @elseif($userNote->prioridad == 1)
                                                    <i class="fa fa-warning text-yellow"></i>
                                                @elseif($userNote->prioridad == 2)
                                                    <i class="fa fa-warning text-red"></i>
                                                @endif
                                                {{ $userNote->titulo }}</a>
                                        </li>
                                    @endforeach
                                    {{--<li><!-- start notification -->
                                        <a href="#">

                                        </a>
                                    </li><!-- end notification -->--}}
                                </ul>
                            </li>
                            <li class="footer"><a href="{{ URL::to('notes') }}">Ver más</a></li>
                        </ul>
                    </li>
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            @if (Auth::user()->avatar)
                                <img src="{{ URL::asset('uploads/user_' . Auth::user()->id . '.png') }}" class="user-image" alt="User Image">
                            @else
                                <img src="{{ URL::asset('uploads/fallback-user-avatar.png') }}" class="user-image" alt="User Image">
                            @endif
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{!! Auth::user()->name . ' ' . Auth::user()->lastname !!}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                @if (Auth::user()->avatar)
                                    <img src="{{ URL::asset('uploads/user_' . Auth::user()->id . '.png') }}" class="img-circle" alt="User Image">
                                @else
                                    <img src="{{ URL::asset('uploads/fallback-user-avatar.png' . Auth::user()->avatar) }}" class="img-circle" alt="User Image">
                                @endif
                                <p>
                                    {!! Auth::user()->name . ' ' . Auth::user()->lastname !!}
                                    <small>
                                        @if (Auth::user()->role_id == 1)
                                            Root
                                        @elseif (Auth::user()->role_id == 2)
                                            Administador
                                        @elseif (Auth::user()->role_id == 3)
                                            Manager
                                        @elseif (Auth::user()->role_id == 4)
                                            Editor
                                        @endif
                                    </small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{ URL::to('users') }}/{{ Auth::user()->id }}" class="btn btn-default btn-flat">Perfil</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ URL::to('logout') }}" class="btn btn-default btn-flat">Cerrar sesión</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        @include('layouts.sidebar')
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('content')
    </div>

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            Unus-<strong>S</strong>(earch)<strong>E</strong>(ngine) v-1.1.0
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2016 <a href="#">Unus</a>.</strong> All rights reserved.
    </footer>

</div>

<!-- REQUIRED JS SCRIPTS -->
<!-- jQuery 2.1.4 -->
<script src="{{ URL::asset('plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- lodash 4.0.0 -->
<script src="{{ URL::asset('plugins/lodash/lodash.min.js') }}"></script>
<!-- Bootstrap 3.3.5 -->
<script src="{{ URL::asset('bootstrap/js/bootstrap.min.js') }}"></script>
<!-- Plugins -->
<script src="{{ URL::asset('plugins/cookies/js.cookie.js') }}"></script>
<script src="{{ URL::asset('plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ URL::asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ URL::asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ URL::asset('plugins/datepicker/locales/bootstrap-datepicker.es.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.0.2/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.0.2/js/responsive.bootstrap.min.js"></script>
<script src="{{ URL::asset('plugins/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{ URL::asset('plugins/fullcalendar/lang-all.js') }}"></script>
<script src="{{ URL::asset('plugins/jQuery-tagEditor/jquery.caret.min.js') }}"></script>
<script src="{{ URL::asset('plugins/jQuery-tagEditor/jquery.tag-editor.min.js') }}"></script>
<script src="{{ URL::asset('plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ URL::asset('plugins/select2/i18n/es.js') }}"></script>
<script src="{{ URL::asset('plugins/jsPDF/jspdf.min.js') }}"></script>
<script src="{{ URL::asset('plugins/jsPDF/jspdf.plugin.autotable.src.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::asset('dist/js/app.min.js') }}"></script>
@stack('scripts')

</body>

</html>