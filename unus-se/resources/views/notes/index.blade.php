@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Control Panel
            <small>Entidades</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Entidad</a></li>
            <li class="active">Nota</li>
        </ol>
    </section>

    <section class="content">
        @include('flash::message')
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Nota</h3>
                        <div class="pull-right">
                            <a href="{!! route('notes.create') !!}" class="btn btn-primary icon-button"><i class="fa fa-plus-circle"></i> Crear Nota</a>
                        </div>
                    </div>
                    <div class="box-body">
                        @if($notes->isEmpty())
                            <div class="well text-center">No se han encontrado notas.</div>
                        @else
                            @include('notes.table')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop