<!-- Titulo Field -->
<div class="form-group">
    {!! Form::label('titulo', 'Título', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $note->titulo !!}</p>
    </div>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripción', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $note->descripcion !!}</p>
    </div>
</div>

<div class="form-group">
    {!! Form::label('created_at', 'Fecha de creación', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $note->created_at !!}</p>
    </div>
</div>

<div class="form-group">
    {!! Form::label('updated_at', 'Fecha de actualización', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $note->updated_at !!}</p>
    </div>
</div>

<!-- Keywords Field -->
<div class="form-group">
    {!! Form::label('keywords', 'Keywords', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static" id="keywords">{!! $note->keywords !!}</p>
    </div>
</div>

<div class="form-group">
    {!! Form::label('prioridad', 'Prioridad', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">
            @if ($note->prioridad == 0)
                <small class="label label-default text-uppercase">baja</small>
            @elseif($note->prioridad == 1)
                <small class="label label-warning text-uppercase">media</small>
            @elseif($note->prioridad == 2)
                <small class="label label-danger text-uppercase">alta</small>
            @endif
        </p>
    </div>
</div>

<div class="form-group">
    {!! Form::label('status', 'Status', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">
            @if ($note->status == 0)
                <small class="label label-danger text-uppercase">abierto</small>
            @elseif($note->status == 1)
                <small class="label label-warning text-uppercase">progreso</small>
            @elseif($note->status == 2)
                <small class="label label-success text-uppercase">cerrado</small>
            @endif
        </p>
    </div>
</div>

<!-- Person Id Field -->
<div class="form-group">
    {!! Form::label('person_id', 'Persona(s)', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static"><a href="/person/{!! $note->idPersona !!}">{!! $person_name !!}</a></p>
    </div>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'Usuario', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $user_name !!}</p>
    </div>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('seguimiento_id', 'Seguimiento', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $seguimiento !!}</p>
    </div>
</div>

@push('scripts')
<script>
    $(function() {
        var formatKeys  = '';
        var $entityKeys = $('#keywords');
        var keyList     = _.split($entityKeys.html(), ',');
        _.forEach(keyList, function (value, key) {
            formatKeys += '<small class="label label-primary label-keyword">' + value + '</small>';
        });
        $entityKeys.html(formatKeys);
    });
</script>
@endpush