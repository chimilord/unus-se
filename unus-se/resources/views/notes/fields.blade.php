<!-- Titulo Field -->
<div class="form-group">
    {!! Form::label('titulo', 'Título', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripción', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Prioridad Field -->
<div class="form-group">
    {!! Form::label('prioridad', 'Prioridad', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('prioridad', array('' => '-- Seleccionar --', '0' => 'BAJA', '1' => 'MEDIA', '2' => 'ALTA'), null, array('class' => 'form-control')) !!}
    </div>
</div>

<!-- Person Id Field -->
<div class="form-group">
    {!! Form::label('idPersona', 'Persona', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('idPersona', $person_id, null, ['class' => 'form-control select2']) !!}
    </div>
</div>

<!-- Person Id Field -->
<div class="form-group">
    {!! Form::label('idSeguimiento', 'Seguimiento', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('idSeguimiento', $user_id, null, ['class' => 'form-control select2']) !!}
    </div>
</div>

<!-- Keywords Field -->
<div class="form-group">
    {!! Form::label('keywords', 'Keywords (tema)', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('keywords', null, ['id' => 'keywords', 'class' => 'form-control']) !!}
    </div>
</div>

@if ($action == 'Crear')
{{ Form::hidden('status', '0') }}
@elseif ($action == 'Actualizar')
    <div class="form-group">
        {!! Form::label('status', 'Status', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::select('status', array('' => '-- Seleccionar --', '1' => 'EN PROGRESO', '2' => 'CERRAR'), null, array('class' => 'form-control')) !!}
        </div>
    </div>
@endif

{{ Form::hidden('idUser', Auth::user()->id) }}

        <!-- Submit Field -->
<div class="box-footer">
    {!! Form::submit($action, ['class' => 'btn btn-primary pull-right']) !!}
</div>

@push('scripts')
<script>
    $(function () {
        var $entityKeys = $('#keywords');
        var $select = $('.select2');

        $entityKeys.tagEditor({forceLowercase: false, placeholder: 'keyword1, keyword2, ...'});
        $select.select2({language: "es"});
    });
</script>
@endpush