<table id="showDataTable" class="table table-striped table-bordered dt-responsive entity-tbl" cellspacing="0" width="100%">
    <thead>
    <th>Título</th>
    <th>Descripción</th>
    <th>Fecha Creación</th>
    <th>Asignación</th>
    <th>Acciones</th>
    </thead>
    <tfoot>
    <th>Título</th>
    <th>Descripción</th>
    <th>Fecha Creación</th>
    <th>Asignación</th>
    <th>Acciones</th>
    </tfoot>
</table>

@push('scripts')
<script>
    $(function() {
        $('#showDataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('notes.data') !!}',
            columns: [
                {data: 'titulo', name: 'notes.titulo', render: function(data, type, row) {
                    return '<a href="{{ url('notes') }}/' + row.id + '">' + data + '</a>';
                }},
                {data: 'descripcion', name: 'notes.descripcion', orderable: false},
                {data: 'created_at', name: 'notes.created_at'},
                {data: 'name', name: 'users.name'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron registros",
                "info": "_PAGE_ de _PAGES_",
                "infoEmpty": "Información vacía",
                "infoFiltered": "(filtrando de _MAX_ en total)",
                "search": "Buscar:",
                "processing": "Cargando...",
                paginate: {
                    previous: '‹',
                    next: '›'
                }
            }
        });
    });
</script>
@endpush