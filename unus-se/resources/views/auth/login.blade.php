@extends('layouts.auth')

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <b>UNUS</b> - SE
        </div><!-- /.login-logo -->
        <div class="login-box-body">
            <div class="alert alert-danger browser-note hidden">
                <i class="fa fa-internet-explorer"></i> Internet Explorer no soporta toda la funcionalidad de Unus-SE
            </div>
            <p class="login-box-msg">Ingresa tus credenciales</p>
            <form role="form" method="POST" action="{{ url('/login') }}">
                {!! csrf_field() !!}
                <div class="has-feedback form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="email" class="form-control" name="email" placeholder="Email"
                           value="{{ old('email') }}">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                    @endif
                </div>
                <div class="has-feedback form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" class="form-control" name="password" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                    @endif
                </div>

                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember"> Recordar
                            </label>
                        </div>
                    </div><!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
                    </div><!-- /.col -->
                </div>
            </form>

            <hr>

            <a href="{{ url('/password/reset') }}">¿Olvidé mi password?</a><br>

        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

@stop