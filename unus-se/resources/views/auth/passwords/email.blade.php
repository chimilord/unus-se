@extends('layouts.auth')

@section('content')

    <div class="login-box">
        <div class="login-logo">
            <b>UNUS</b> - SE
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Ingresa tu e-mail</p>
            <form class="form" role="form" method="POST" action="{{ url('/password/email') }}">
                {!! csrf_field() !!}
                <div class="has-feedback form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-4 pull-right">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
@endsection
