<!-- Partido Field -->
<div class="form-group">
    {!! Form::label('partido', 'Partido', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('partido', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Siglas Field -->
<div class="form-group">
    {!! Form::label('siglas', 'Siglas', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('siglas', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="box-footer">
    {!! Form::submit($action, ['class' => 'btn btn-primary pull-right']) !!}
</div>
