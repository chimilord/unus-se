<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Comisión', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Description Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Idestado Field -->
<div class="form-group">
    {!! Form::label('idTipoComision', 'Tipo de Comision', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('idTipoComision', $idTipoComision, null, array('class' => 'form-control')) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="box-footer">
    {!! Form::submit($action, ['class' => 'btn btn-primary pull-right']) !!}
</div>

