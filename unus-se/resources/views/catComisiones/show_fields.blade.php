<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $catComisiones->nombre !!}</p>
    </div>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripción', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $catComisiones->descripcion !!}</p>
    </div>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('tipoComision', 'Tipo de Comision', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $tipoComision !!}</p>
    </div>
</div>

