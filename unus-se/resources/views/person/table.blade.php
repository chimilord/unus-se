<table id="showDataTable" class="table table-striped table-bordered dt-responsive entity-tbl" cellspacing="0" width="100%">
	<thead>
	<th>Nombre</th>
	<th>Email</th>
	<th>Teléfono(s)</th>
	<th>Dirección</th>
	<th width="50px">Acciones</th>
	</thead>
	<tfoot>
	<th>Nombre</th>
	<th>Email</th>
	<th>Teléfono(s)</th>
	<th>Dirección</th>
	<th width="50px">Acciones</th>
	</tfoot>
</table>

@push('scripts')
<script>
	$(function () {
		$('#showDataTable').DataTable({
			iDisplayLength: 25,
			processing: true,
			serverSide: true,
			ajax: '{!! route('person.data') !!}',
			columns: [
				{data: 'nombre', name: 'nombre', render: function(data, type, row) {
					return '<a href="{{ url('person') }}/' + row.id + '">' + data + '</a>';
				}},
				{data: 'email', name: 'email'},
				{data: 'telefono', name: 'telefono', orderable: false, searchable: false},
				{data: 'direccion', name: 'direccion'},
				{data: 'action', name: 'action', orderable: false, searchable: false}
			],
			language: {
				"lengthMenu": "Mostrar _MENU_ registros",
				"zeroRecords": "No se encontraron registros",
				"info": "_PAGE_ de _PAGES_",
				"infoEmpty": "Información vacía",
				"infoFiltered": "(filtrando de _MAX_ en total)",
				"search": "Buscar:",
				"processing": "Cargando...",
				paginate: {
					previous: '‹',
					next: '›'
				}
			}
		});
	});
</script>
@endpush
