<!-- Fotografia Field -->
<div class="form-group">
    {!! Form::label('fotografia', 'Avatar', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::file('fotografia') !!}
    </div>
</div>

<!-- IdTipoPersona Field -->
<div class="form-group">
    {!! Form::label('idTipoPersona', 'Tipo', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('idTipoPersona', $idTipoPersona, null, array('class' => 'form-control')) !!}
    </div>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre(s)', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Apellidos Field -->
<div class="form-group">
    {!! Form::label('apellidos', 'Apellidos', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('apellidos', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Idestado Field -->
<div class="form-group">
    {!! Form::label('idEstado', 'Entidad Federativa', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('idEstado', $idEstado, null, array('class' => 'form-control')) !!}
    </div>
</div>

<!-- Iddistrito Field -->
<div class="form-group">
    {!! Form::label('idDistrito', 'Distrito', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('idDistrito', $idDistrito, null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Localidad Field -->
<div class="form-group">
    {!! Form::label('localidad', 'Localidad', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('localidad', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Idstatusperson Field -->
<div class="form-group">
    {!! Form::label('idStatusPerson', 'Estatus Persona', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('idStatusPerson', $idStatusPerson, null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Suplente Field -->
<div class="form-group">
    {!! Form::label('suplente', 'Suplente', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('suplente', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Idpartido Field -->
<div class="form-group">
    {!! Form::label('idPartido', 'Partido Político', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('idPartido',$idPartido , null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Idtipoeleccion Field -->
<div class="form-group">
    {!! Form::label('idTipoEleccion', 'Tipo de Elección', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('idTipoEleccion', $idTipoEleccion, null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Profesion Field -->
<div class="form-group">
    {!! Form::label('estudios', 'Estudios  ', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('estudios', null, ['class' => 'form-control', 'rows' => 4]) !!}
    </div>
</div>

<!-- Trayectoriaacademica Field -->
<div class="form-group">
    {!! Form::label('trayectoriaAcademica', 'Trayectoria Académica', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('trayectoriaAcademica', null, ['class' => 'form-control', 'rows' => 4]) !!}
    </div>
</div>

<!-- Experiencialegislativa Field -->
<div class="form-group">
    {!! Form::label('experienciaLegislativa', 'Experiencia Legislativa', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('experienciaLegislativa', null, ['class' => 'form-control', 'rows' => 4]) !!}
    </div>
</div>


<!-- TrayectoriaAdministrativa Field -->
<div class="form-group">
    {!! Form::label('trayectoriaAdministrativa', 'Trayectoria Administrativa', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('trayectoriaAdministrativa', null, ['class' => 'form-control', 'rows' => 4]) !!}
    </div>
</div>


<!-- Experienciapolitica Field -->
<div class="form-group">
    {!! Form::label('experienciaPolitica', 'Experiencia Política', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('experienciaPolitica', null, ['class' => 'form-control', 'rows' => 4]) !!}
    </div>
</div>

<!-- Experienciaempresarial Field -->
<div class="form-group">
    {!! Form::label('experienciaEmpresarial', 'Experiencia Empresarial', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('experienciaEmpresarial', null, ['class' => 'form-control', 'rows' => 4]) !!}
    </div>
</div>

<!-- Experienciaotra Field -->
<div class="form-group">
    {!! Form::label('experienciaOtra', 'Experiencia Otra', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('experienciaOtra', null, ['class' => 'form-control', 'rows' => 4]) !!}
    </div>
</div>

<!-- Noticias Field -->
<div class="form-group">
    {!! Form::label('datosComplementarios', 'Datos Complementarios', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('datosComplementarios', null, ['class' => 'form-control', 'rows' => 4]) !!}
    </div>
</div>

<!-- Organogobierno Field -->
{{--<div class="form-group">
    {!! Form::label('organoGobierno', 'Órgano de Gobierno', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('organoGobierno', null, ['class' => 'form-control', 'rows' => 4]) !!}
    </div>
</div>--}}

<!-- Direccion Field -->
<div class="form-group">
    {!! Form::label('direccion', 'Dirección', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Telefonos Field -->
<div class="form-group">
    {!! Form::label('telefono', 'Telefonos', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
    </div>
</div>


<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('otrasFunciones', 'Otras Funciones', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('otrasFunciones', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Seguimiento Field -->
<div class="form-group">
    {!! Form::label('seguimiento', 'Seguimiento', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::checkbox('seguimiento', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Submit Date Field -->
<div class="form-group">
    {!! Form::label('inicioPeriodo', 'Fecha Inicio Periodo', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('inicioPeriodo', null, ['class' => 'form-control input-date']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('finPeriodo', 'Fecha Fin Periodo', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('finPeriodo', null, ['class' => 'form-control input-date']) !!}
    </div>
</div>
<!-- Email Field -->
<div class="form-group">
    {!! Form::label('nombrePeriodo', 'Nombre del Periodo', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('nombrePeriodo', null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="box-footer">
    {!! Form::submit($action, ['class' => 'btn btn-primary pull-right']) !!}
</div>

@push('scripts')
<script>
    $(function () {
        var $inputDate = $('.input-date');
        var dpOptions = {
            singleDatePicker: true,
            format: 'DD/MM/YYYY',
            locale: {
                daysOfWeek: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                monthNames: [
                    'Enero',
                    'Febrero',
                    'Marzo',
                    'Abril',
                    'Mayo',
                    'Junio',
                    'Julio',
                    'Agosto',
                    'Septiembre',
                    'Octubre',
                    'Noviembre',
                    'Diciembre'
                ]
            }
        };
        $inputDate.daterangepicker(dpOptions);
    });
</script>
@endpush