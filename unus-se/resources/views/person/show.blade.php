@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Control Panel
            <small>Entidades</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/home"><i class="fa fa-cubes"></i> Entidad</a></li>
            <li><a href="/person/"><i class="fa fa-user"></i> Persona</a></li>
            <li class="active">{!! $person->nombre !!} {!! $person->apellidos !!}</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget widget-user-2">
                    <div class="widget-user-header bg-gray-light">
                        <div class="widget-user-image" id="widgetAvatar">
                            <img alt="User Avatar" src="{{ URL::asset('uploads/' . $person->id . '.png') }}"
                                 class="img-circle">
                        </div><!-- /.widget-user-image -->
                        <h3 class="widget-user-username">{!! $person->nombre !!} {!! $person->apellidos !!}</h3>
                        <h5 class="widget-user-desc">
                            {!! $tipo !!}
                            @if ($person->nombrePeriodo)
                                - {!! $person->nombrePeriodo !!}
                            @endif
                        </h5>
                        {!! Form::open(['id' => 'tracking', 'class' => 'form-horizontal']) !!}
                        <div class="pull-right">
                            @if ($person->seguimiento)
                                {{Form::button('<i class="fa fa-hand-o-left"></i> terminar seguimiento', array('type' => 'button', 'class' => 'btn btn-default pull-right btn-xs person-check'))}}
                            @elseif(!$person->seguimiento)
                                <input type="hidden" name="seguimiento" id="seguimiento" value="on">
                                {{Form::button('<i class="fa fa-hand-o-right"></i> iniciar seguimiento', array('type' => 'button', 'class' => 'btn btn-default pull-right btn-xs person-check'))}}
                            @endif
                        </div>
                        {!! Form::close() !!}
                    </div>
                    @include('person.show_fields')
                </div>
            </div>
        </div>
    </section>

@stop

@push('scripts')
<script>
    $(function () {
        $('.person-check').on('click', function () {
            var data = $('#tracking').serialize();
            $.ajax({
                url: '{!! url('person') !!}/' + '{!! $person->id !!}' + '/tracking',
                type: 'POST',
                data: data
            }).done(function (response) {
                console.log(response);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
                alert('Error al actualizar el seguimiento. Intente de nuevo por favor.')
            }).always(function() {
                document.location.reload(true);
            });
        });
    });
</script>
@endpush