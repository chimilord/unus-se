@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Control Panel
            <small>Entidades</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Entidad</a></li>
            <li class="active">Persona</li>
        </ol>
    </section>

    <section class="content">
        @include('flash::message')
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Persona</h3>
                        <div class="pull-right">
                            <a href="{!! route('person.create') !!}" class="btn btn-primary icon-button"><i class="fa fa-plus-circle"></i> Crear Persona</a>
                        </div>
                    </div>
                    <div class="box-body">
                        @if($persons->isEmpty())
                            <div class="well text-center">No se han encontrado personas.</div>
                        @else
                            @include('person.table')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--<div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">People</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('people.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($people->isEmpty())
                <div class="well text-center">No People found.</div>
            @else
                @include('people.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $people])


    </div>--}}
@stop
