@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Control Panel
            <small>Entidades</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/initiatives/"><i class="fa fa-dashboard"></i> Entidad</a></li>
            <li class="active">Iniciativa</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Iniciativa</h3>
                        <div class="pull-right">
                            <a href="{!! route('initiatives.index') !!}" class="btn btn-primary icon-button"><i class="fa fa-list-ul"></i> Catálogo completo</a>
                        </div>
                    </div>
                    <div class="box-body">
                        @include('common.errors')

                        {!! Form::open(['route' => 'initiatives.store', 'class' => 'form-horizontal']) !!}

                        @include('initiatives.fields', ['action' => 'Crear'])

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
