<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Título', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $initiative->title !!}</p>
    </div>
</div>

@if ($initiative->description)
<div class="form-group">
    {!! Form::label('description', 'Descripción', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $initiative->description !!}</p>
    </div>
</div>
@endif

@if ($initiative->submit_date)
<div class="form-group">
    {!! Form::label('submit_date', 'Presentación al Pleno', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $initiative->submit_date !!}</p>
    </div>
</div>
@endif

@if ($initiative->publication_date)
<div class="form-group">
    {!! Form::label('publication_date', 'Publicación en Gaceta', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $initiative->publication_date !!}</p>
    </div>
</div>
@endif

@if ($initiative->fechaDecreto)
<div class="form-group">
    {!! Form::label('fechaDecreto', 'Publicación del Decreto', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $initiative->fechaDecreto !!}</p>
    </div>
</div>
@endif

<div class="form-group">
    {!! Form::label('turnada', 'Turnada a la Comisión', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        @foreach($nombres as $nombre)
            @foreach($comisiones as $comision)
                @if ($nombre->id == $comision->idCatComision)
                    <p class="form-control-static"><a href="{{ URL::to('comisions')}}/{!! $comision->id !!}">{!! $nombre->nombre  !!}</a></p>
                @endif
            @endforeach
        @endforeach
    </div>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Estatus', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $status_name !!}</p>
    </div>
</div>

<!-- Person Id Field -->
<div class="form-group">
    {!! Form::label('person_id', 'Presentada por', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        @foreach($partidos as $partido)
            @foreach($persons as $person)
                @if ($partido->id == $person->idPartido)
                    <p class="form-control-static"><a href="{{ URL::to('person')}}/{!! $person->id !!}">{!! $person->nombre . ' ' . $person->apellidos !!}</a> ({!! $partido->siglas !!})</p>
                @endif
            @endforeach
        @endforeach
    </div>
</div>

@if ($initiative->link)
<div class="form-group">
    {!! Form::label('link', 'Link', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">
            <a href="{!! $initiative->link !!}">{!! $initiative->link !!} <i class="fa fa-external-link-square"></i></a>
        </p>
    </div>
</div>
@endif

@if ($initiative->decree)
<div class="form-group">
    {!! Form::label('decree', 'Decreto', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">
            @if($initiative->decree)
                <a href="{!! $initiative->decree !!}">{!! $initiative->decree !!} <i class="fa fa-external-link-square"></i></a>
            @endif
        </p>
    </div>
</div>
@endif

<!-- Keywords Field -->
<div class="form-group">
    {!! Form::label('keywords', 'Keywords', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static" id="keywords">{!! $initiative->keywords !!}</p>
    </div>
</div>

@push('scripts')
<script>
    $(function () {
        var formatKeys  = '';
        var $entityKeys = $('#keywords');
        var keyList     = _.split($entityKeys.html(), ',');
        _.forEach(keyList, function (value, key) {
            formatKeys += '<span class="label label-primary label-keyword">' + value + '</span>';
        });
        $entityKeys.html(formatKeys);
    });
</script>
@endpush