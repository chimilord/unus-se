<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Iniciativa', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Resumen', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- camara Id Field -->
<div class="form-group">
    {!! Form::label('idCamara', 'Presentada En', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('idCamara', $idCamara, null, array('class' => 'form-control')) !!}
    </div>
</div>

<!-- Submit Date Field -->
<div class="form-group">
    {!! Form::label('submit_date', 'Fecha de Presentación', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('submit_date', null, ['class' => 'form-control input-date', 'placeholder' => 'dd/mm/yyyy']) !!}
    </div>
</div>

<!-- Publication Date Field -->
<div class="form-group">
    {!! Form::label('publication_date', 'Publicación en Gaceta', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('publication_date', null, ['class' => 'form-control input-date', 'placeholder' => 'dd/mm/yyyy']) !!}
    </div>
</div>

<!-- Decreto Date Field -->
<div class="form-group">
    {!! Form::label('fechaDecreto', 'Publicación en DOF', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('fechaDecreto', null, ['class' => 'form-control input-date', 'placeholder' => 'dd/mm/yyyy']) !!}
    </div>
</div>



<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Estatus', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('status_id', $status_id, null, array('class' => 'form-control')) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('id_person', 'Persona', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::select('id_person', $persons, null, ['class' => 'form-control select2']) !!}
    </div>
    {!! Form::label('id_position', 'Cargo', ['class' => 'col-sm-1 control-label']) !!}
    <div class="col-sm-3">
        {!! Form::select('id_position',  $positions, null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-1">
        <button type="button" class="btn btn-primary add-relation">asignar</button>
    </div>
</div>

<div class="relation-initiative-person">
    @if ($action == 'Actualizar')
        @foreach($selectedPersons as $selected)
            <div class="row initiative-person">
                <input type="hidden" name="persons[]" value="{!! $selected->id !!}">
                <input type="hidden" name="positions[]" value="{!! $selected->cargo_id !!}">
                <div class="col-sm-offset-2 col-sm-5">{!! $selected->nombre . ' ' . $selected->apellidos !!}</div>
                <div class="col-sm-offset-1 col-sm-3">{!! $selected->cargo !!}</div>
                <div class="col-sm=1">
                    <i class="fa fa-times icon-red remove-relation"></i>
                </div>
            </div>
        @endforeach
    @endif
</div>


<div class="form-group">
    {!! Form::label('id_comision', 'Comision', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::select('id_comision', $comisiones, null, ['class' => 'form-control select2']) !!}
    </div>
    {!! Form::label('id_turnado', 'Turnado Para:', ['class' => 'col-sm-1 control-label']) !!}
    <div class="col-sm-3">
        {!! Form::select('id_turnado',  $turnados, null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-1">
        <button type="button" class="btn btn-primary add-relationC">asignar</button>
    </div>
</div>


<div class="relation-initiative-comision">
    @if ($action == 'Actualizar')
        @foreach($selectedComisions as $selected)
            <div class="row initiative-comision">
                <input type="hidden" name="comisiones[]" value="{!! $selected->idComision !!}">
                <input type="hidden" name="turnados[]" value="{!! $selected->idStatus !!}">
                <div class="col-sm-offset-2 col-sm-5">{!! $selected->comision !!}</div>
                <div class="col-sm-offset-1 col-sm-3">{!! $selected->status !!}</div>
                <div class="col-sm=1">
                    <i class="fa fa-times icon-red remove-relationC"></i>
                </div>
            </div>
        @endforeach
    @endif
</div>

<!-- Link Field -->
<div class="form-group">
    {!! Form::label('link', 'Link', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('link', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Decree Field -->
<div class="form-group">
    {!! Form::label('decree', 'Decreto', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('decree', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Keywords Field -->
<div class="form-group">
    {!! Form::label('keywords', 'Keywords (tema)', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('keywords', null, ['id' => 'keywords', 'class' => 'form-control']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="box-footer">
    {!! Form::submit($action, ['class' => 'btn btn-primary pull-right']) !!}
</div>

@push('scripts');
<script>
    $(function () {
        var $entityKeys = $('#keywords');
        var $inputDate = $('.input-date');
        var $select = $('.select2');
        var dpOptions = {
            singleDatePicker: true,
            format: 'DD/MM/YYYY',
            locale: {
                daysOfWeek: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                monthNames: [
                    'Enero',
                    'Febrero',
                    'Marzo',
                    'Abril',
                    'Mayo',
                    'Junio',
                    'Julio',
                    'Agosto',
                    'Septiembre',
                    'Octubre',
                    'Noviembre',
                    'Diciembre'
                ]
            }
        };

        $entityKeys.tagEditor({forceLowercase: false, placeholder: 'keyword1, keyword2, ...'});
        $inputDate.daterangepicker(dpOptions);
        $select.select2({language: "es"});
    });
</script>


<script>
    $(function () {
        var $relation = $('.add-relation');
        var $removeRel = $('.relation-initiative-person');

        var addRelation = function () {
            $person = $('#id_person');
            $position = $('#id_position');
            $relation = $('.relation-initiative-person');
            $relation.append('<div class="row initiative-person" data-person-id="' + $person.val() + '">'
                    + '<input type="hidden" name="persons[]" value="' + $person.val() + '">'
                    + '<input type="hidden" name="positions[]" value="' + $position.val() + '">'
                    + '<div class="col-sm-offset-2 col-sm-5" data-person="' + $person.val() + '">'
                    + $('#id_person option:selected').text()
                    + '</div><div class="col-sm-offset-1 col-sm-3" data-position="' + $position.val() + '">'
                    + $('#id_position option:selected').text()
                    + '</div><div class="col-sm=1"><i class="fa fa-times icon-red remove-relation"></i></div></div>');
        };

        var removeRelation = function (event) {
            $(this).closest('.row').remove();
        };

        $relation.on('click', addRelation);
        $removeRel.on('click', 'i', removeRelation);

    });
</script>


<script>
    $(function () {
        var $relationC = $('.add-relationC');
        var $removeRelC = $('.relation-initiative-comision');

        var addRelationC = function () {
            $comision = $('#id_comision');
            $turnado = $('#id_turnado');
            $relationC = $('.relation-initiative-comision');
            $relationC.append('<div class="row initiative-comision" data-comision-id="' + $comision.val() + '">'
                    + '<input type="hidden" name="comisiones[]" value="' + $comision.val() + '">'
                    + '<input type="hidden" name="turnados[]" value="' + $turnado.val() + '">'
                    + '<div class="col-sm-offset-2 col-sm-5" data-comision="' + $comision.val() + '">'
                    + $('#id_comision option:selected').text()
                    + '</div><div class="col-sm-offset-1 col-sm-3" data-turnado="' + $turnado.val() + '">'
                    + $('#id_turnado option:selected').text()
                    + '</div><div class="col-sm=1"><i class="fa fa-times icon-red remove-relationC"></i></div></div>');
        };

        var removeRelationC = function (event) {
            $(this).closest('.row').remove();
        };

        $relationC.on('click', addRelationC);
        $removeRelC.on('click', 'i', removeRelationC);

    });
</script>
@endpush