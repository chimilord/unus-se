<table id="showDataTable" class="table table-striped table-bordered dt-responsive entity-tbl" cellspacing="0" width="100%">
    <thead>
    <th width="25%">Iniciativa</th>
    <th>Descripción</th>
    <th><i class="fa fa-external-link icon-small"></i> Link</th>
    <th width="50px">Acciones</th>
    </thead>
    <tfoot>
    <th>Iniciativa</th>
    <th>Descripción</th>
    <th><i class="fa fa-external-link icon-small"></i> URL</th>
    <th width="50px">Acciones</th>
    </tfoot>
    <tbody>
</table>

@push('scripts')
<script>
    $(function () {
        $('#showDataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('initiatives.data') !!}',
            columns: [
                {data: 'title', name: 'title', render: function(data, type, row) {
                    return '<a href="{{ url('initiatives') }}/' + row.id + '">' + data + '</a>';
                }},
                {data: 'description', name: 'description'},
                {data: 'link', name: 'link', className: 'text-center', render: function(data, type, row) {
                    return '<a href="' + data + '" target="_blank"><i class="fa fa-file-pdf-o"></i></a>';
                }},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron registros",
                "info": "_PAGE_ de _PAGES_",
                "infoEmpty": "Información vacía",
                "infoFiltered": "(filtrando de _MAX_ en total)",
                "search": "Buscar:",
                "processing": "Cargando...",
                paginate: {
                    previous: '‹',
                    next: '›'
                }
            }
        });
    });
</script>
@endpush