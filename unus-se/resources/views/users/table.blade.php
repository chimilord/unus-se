<table id="dataTable" class="table table-bordered table-striped">
	<thead>
	<th>Nombre</th>
	<th>Role</th>
	<th>Email</th>
	<th>Teléfono</th>
	<th width="50px">Acciones</th>
	</thead>
	<tfoot>
	<th>Nombre</th>
	<th>Role</th>
	<th>Email</th>
	<th>Teléfono</th>
	<th width="50px">Acciones</th>
	</tfoot>
</table>

@push('scripts')
<script>
	$(function () {
		$('#dataTable').DataTable({
			processing: true,
			serverSide: true,
			ajax: '{!! route('users.data') !!}',
			columns: [
				{data: 'name', name: 'users.name', render: function(data, type, row) {
					return '<a href="{{ url('users') }}/' + row.id + '">' + data + '</a>';
				}},
				{data: 'roleName', name: 'role.roleName'},
				{data: 'email', name: 'users.email'},
				{data: 'phone', name: 'users.phone', orderable: false, searchable: false},
				{data: 'action', name: 'action', orderable: false, searchable: false}
			],
			language: {
				"lengthMenu": "Mostrar _MENU_ registros",
				"zeroRecords": "No se encontraron registros",
				"info": "_PAGE_ de _PAGES_",
				"infoEmpty": "Información vacía",
				"infoFiltered": "(filtrando de _MAX_ en total)",
				"search": "Buscar:",
				"processing": "Cargando...",
				paginate: {
					previous: '‹',
					next: '›'
				}
			}
		});
	});
</script>
@endpush