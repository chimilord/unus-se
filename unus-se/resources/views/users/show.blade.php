@extends('layouts.app')

@section('content')

	<section class="content-header">
		<h1>
			Control Panel
			<small>Entidades</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="/home"><i class="fa fa-dashboard"></i>Entidad</a></li>
			<li class="active">Usuarios</li>
		</ol>
	</section>

	 <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-light-blue">
                        <h3 class="widget-user-username">{!! $user->name !!} {!! $user->lastname !!}</h3>
                        <h5 class="widget-user-desc">{!! $user->role->name!!}</h5>
                    </div>
                    <div class="widget-user-image">
                        @if ($user->avatar)
                            <img alt="User Avatar" src="{{ URL::asset('uploads/user_' . $user->id . '.png') }}" class="img-circle">
                        @else
                            <img alt="User Avatar" src="{{ URL::asset('uploads/fallback-user-avatar.png') }}" class="img-circle">
                        @endif
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-6 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">e-mail</h5>
                                    <span class="description-text"><a href="mailto:{!! $user->email !!}">{!! $user->email !!}</a></span>
                                </div><!-- /.description-block -->
                            </div><!-- /.col -->
                            <div class="col-sm-6 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">teléfono</h5>
                                    <span class="description-text"><a href="tel:55{!! $user->phone !!}">{!! $user->phone !!}</a></span>
                                </div><!-- /.description-block -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div>
                </div><!-- /.widget-user -->
            </div>

        </div>
    </section>

@stop