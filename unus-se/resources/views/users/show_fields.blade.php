

                                                
<div class="form-group">
    {!! Form::label('Email', 'Email', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $user->email !!}</p>
    </div>
</div>

                                            
<div class="form-group">
    {!! Form::label('Teléfono', 'Teléfono', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $user->phone !!}</p>
    </div>
</div>
          

{{ Form::hidden('idUser', Auth::user()->id) }}
    