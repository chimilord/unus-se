<div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
    @if ($action == 'Actualizar')
        <label class="col-sm-2 control-label">Avatar</label>
        <div class="col-sm-10">
            <input type="file" name="avatar" value="anterior">
            @if ($user->avatar)
                <div class="widget-user-image unus-profile-avatar" id="widgetAvatar">
                    <img width="50" height="50" alt="User Avatar"
                         src="{{ URL::asset('uploads/user_' . $user->id . '.png') }}" class="img-circle">
                </div>
            @endif
            @else
                <label class="col-sm-2 control-label">Avatar</label>
                <div class="col-sm-10">
                    <input type="file" name="avatar">
                </div>
            @endif
        </div>
</div>

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label">Nombre</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="name" value="{!! $user->name !!}">
        @if ($errors->has('name'))
            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label">Apellidos</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="lastname" value="{!! $user->lastname !!}">
        @if ($errors->has('lastname'))
            <span class="help-block"><strong>{{ $errors->first('lastname') }}</strong></span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label">E-Mail</label>
    <div class="col-sm-10">
        <input type="email" class="form-control" name="email" value="{!! $user->email !!}">

        @if ($errors->has('email'))
            <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label">Teléfono</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" name="phone" value="{!! $user->phone !!}">
        @if ($errors->has('phone'))
            <span class="help-block"><strong>{{ $errors->first('phone') }}</strong></span>
        @endif
    </div>
</div>

@if ($action == 'Actualizar')
    <div class="form-group{{ $errors->has('oldpassword') ? ' has-error' : '' }}">
        <label class="col-sm-2 control-label">Password</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" name="oldpassword">
            @if ($errors->has('oldpassword'))
                <span class="help-block"><strong>{{ $errors->first('oldpassword') }}</strong></span>
            @endif
        </div>
    </div>
@endif

@if ($action == 'Actualizar')
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label class="col-sm-2 control-label">Password Nuevo</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" name="password">
            @if ($errors->has('password'))
                <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
        </div>
    </div>
@else
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label class="col-sm-2 control-label">Password</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" name="password">
            @if ($errors->has('password'))
                <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
        </div>
    </div>
@endif

<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label">Confirmar Password</label>
    <div class="col-sm-10">
        <input type="password" class="form-control" name="password_confirmation">
        @if ($errors->has('password_confirmation'))
            <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
        @endif
    </div>
</div>

<div class="form-group">
    {!! Form::label('role_id', 'Role', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('role_id', $roles, null, array('class' => 'form-control')) !!}
    </div>
</div>
{{ Form::hidden('idUser', Auth::user()->id) }}

        <!-- Submit Field -->
<div class="box-footer">
    {!! Form::submit($action, ['class' => 'btn btn-primary pull-right']) !!}
</div>