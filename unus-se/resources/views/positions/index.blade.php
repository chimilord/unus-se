@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Control Panel
            <small>Catálogos</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Catálogo</a></li>
            <li class="active">Cargos</li>
        </ol>
    </section>

    <section class="content">
        @include('flash::message')
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cargos</h3>
                        <div class="pull-right">
                            <a href="{!! route('positions.create') !!}" class="btn btn-primary icon-button"><i class="fa fa-plus-circle"></i> Crear Cargo</a>
                        </div>
                    </div>
                    <div class="box-body">
                        @if($positions->isEmpty())
                            <div class="well text-center">No se han encontrado cargos.</div>
                        @else
                            @include('positions.table')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop