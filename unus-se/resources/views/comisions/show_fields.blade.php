<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('catComision', 'Comision', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $catComision->nombre !!}</p>
    </div>
</div>

<div class="form-group">
    {!! Form::label('tipoComision', 'Tipo Comisión', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $catComision->tipoComision !!}</p>
    </div>
</div>

@if (count($persons) > 0)
    <div class="form-group">
        {!! Form::label('person_id', 'Conformada por', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            <p class="form-control-static">
                <a href="{{ URL::to('person')}}/{!! $persons[0]->id !!}">{!! $persons[0]->nombre . ' ' . $persons[0]->apellidos !!}</a> - ({!! $persons[0]->siglas !!}) {!! $persons[0]->cargo !!}
            @if (count($persons) > 1)
                    <a href="javascript:;" class="see-all"><i class="fa fa-angle-right margin-left-sm"></i></a>
                @endif
            </p>
            <div class="association-component hidden">
                @foreach($persons as $key => $person)
                    <p class="form-control-static @if ($key == 0) hidden @endif"><a href="{{ URL::to('person')}}/{!! $person->id !!}">{!! $person->nombre . ' ' . $person->apellidos !!}</a> - ({!! $person->siglas !!}) {!! $person->cargo !!}</p>
                @endforeach
            </div>
        </div>
    </div>
@endif

@if (count($iniciativas) > 0)
    <div class="form-group">
        {!! Form::label('iniciativa_id', 'Iniciativas turnadas', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            <p class="form-control-static">
                <a href="{{ URL::to('initiatives')}}/{!! $iniciativas[0]->id !!}">{!! $iniciativas[0]->title !!}</a> ({!! $estatus[0]->name !!})
                @if (count($iniciativas) > 1)
                    <a href="javascript:;" class="see-all"><i class="fa fa-angle-right margin-left-sm"></i></a>
                @endif
            </p>
            <div class="association-component hidden">
                @foreach($estatus as $estati)
                    @foreach($iniciativas as $iniciativa)
                        @if ($iniciativa->status_id == $estati->id)
                            <p class="form-control-static @if ($iniciativa == $iniciativas->first()) hidden @endif"><a href="{{ URL::to('initiatives')}}/{!! $iniciativa->id !!}">{!! $iniciativa->title !!}</a> ({!! $estati->name !!})</p>
                        @endif
                    @endforeach
                @endforeach
            </div>
        </div>
    </div>
    @endif

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('periodo', 'Periodo', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $periodo !!}</p>
    </div>
</div>

<!-- Keywords Field -->
<div class="form-group">
    {!! Form::label('keywords', 'Keywords', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static" id="keywords">{!! $comision->keywords !!}</p>
    </div>
</div>

@push('scripts')
<script>
    $(function () {
        var formatKeys  = '';
        var $entityKeys = $('#keywords');
        var keyList     = _.split($entityKeys.html(), ',');
        var $seeAll     = $('.see-all');
        _.forEach(keyList, function (value, key) {
            formatKeys += '<span class="label label-primary label-keyword">' + value + '</span>';
        });
        $entityKeys.html(formatKeys);
        $seeAll.on('click', function() {
            $(this).find('i').toggleClass("fa-angle-right fa-angle-down");
            $(this).parents().eq(1).find($('.association-component')).toggleClass('hidden')
        });
    });
</script>
@endpush
