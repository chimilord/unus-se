

<!-- IdTipoComision Field -->
<div class="form-group">
    {!! Form::label('idCatComision', 'Comision', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('idCatComision', $idCatComision, null, array('class' => 'form-control')) !!}
    </div>
</div>
<!-- Keywords Field -->
<div class="form-group">
    {!! Form::label('keywords', 'Keywords (tema)', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('keywords', null, ['id' => 'keywords', 'class' => 'form-control']) !!}
    </div>
</div>
<!-- IdTipoComision Field -->
<div class="form-group">
    {!! Form::label('idPeriodo', 'Periodo', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('idPeriodo', $idPeriodo, null, array('class' => 'form-control')) !!}
    </div>
</div>


<div class="form-group">
    {!! Form::label('id_person', 'Persona', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::select('id_person', $persons, null, ['class' => 'form-control select2']) !!}
    </div>
    {!! Form::label('id_position', 'Cargo', ['class' => 'col-sm-1 control-label']) !!}
    <div class="col-sm-3">
        {!! Form::select('id_position',  $positions, null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-1">
        <button type="button" class="btn btn-primary add-relation">asignar</button>
    </div>
</div>

<div class="relation-comision-person">
    @if ($action == 'Actualizar')
        @foreach($selectedPersons as $selected)
            <div class="row initiative-person">
                <input type="hidden" name="persons[]" value="{!! $selected->id !!}">
                <input type="hidden" name="positions[]" value="{!! $selected->cargo_id !!}">
                <div class="col-sm-offset-2 col-sm-5">{!! $selected->nombre . ' ' . $selected->apellidos !!}</div>
                <div class="col-sm-offset-1 col-sm-3">{!! $selected->cargo !!}</div>
                <div class="col-sm=1">
                    <i class="fa fa-times icon-red remove-relation"></i>
                </div>
            </div>
        @endforeach
    @endif
</div>


<!-- Submit Field -->
<div class="box-footer">
    {!! Form::submit($action, ['class' => 'btn btn-primary pull-right']) !!}
</div>

@push('scripts')
<script>
    $(function () {
        var $entityKeys = $('#keywords');
        var $relation  = $('.add-relation');
        var $removeRel = $('.relation-comision-person');
        var $select = $('.select2');

        var addRelation = function() {
            $person   = $('#id_person');
            $position = $('#id_position');
            $relation = $('.relation-comision-person');
            $relation.append('<input type="hidden" name="persons[]" value="' + $person.val() + '"><input type="hidden" name="positions[]" value="' + $position.val() + '"><div class="row comision-person" data-person-id="' + $person.val() + '"><div class="col-sm-offset-2 col-sm-5" data-person="' + $person.val() + '">'
                    + $('#id_person option:selected').text()
                    + '</div><div class="col-sm-offset-1 col-sm-3" data-position="' + $position.val() + '">'
                    + $('#id_position option:selected').text()
                    + '</div><div class="col-sm=1"><i class="fa fa-times icon-red remove-relation"></i></div></div>');
        };

        var removeRelation = function(event) {
            console.log('this', $(this).closest('.row').remove());

        };

        $entityKeys.tagEditor({forceLowercase: false, placeholder: 'keyword1, keyword2, ...'});
        $relation.on('click', addRelation);
        $removeRel.on('click', 'i', removeRelation);
        $select.select2({language: "es"});

    });
</script>
@endpush