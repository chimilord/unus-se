<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nombre', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="box-footer">
    {!! Form::submit($action, ['class' => 'btn btn-primary pull-right']) !!}
</div>