<table id="showDataTable" class="table table-striped table-bordered dt-responsive entity-tbl" cellspacing="0" width="100%">
    <thead>
    <th>Nombre</th>
    <th width="50px">Acciones</th>
    </thead>
    <tfoot>
    <th>Nombre</th>
    <th width="50px">Acciones</th>
    </tfoot>
</table>

@push('scripts')
<script>
    $(function () {
        $('#showDataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('catCamaras.data') !!}',
            columns: [
                {data: 'name', name: 'name'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron registros",
                "info": "_PAGE_ de _PAGES_",
                "infoEmpty": "Información vacía",
                "infoFiltered": "(filtrando de _MAX_ en total)",
                "search": "Buscar:",
                "processing": "Cargando...",
                paginate: {
                    previous: '‹',
                    next: '›'
                }
            }
        });
    });
</script>
@endpush