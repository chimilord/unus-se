@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Control Panel
            <small>Búsqueda</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Búsqueda</a></li>
            <li class="active">Entidad</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Your Page Content Here -->
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Criterios de búsqueda</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    {!! Form::open(['id' => 'search', 'class' => 'form-horizontal']) !!}
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="searchTags">Keyword(s)</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><input type="checkbox" name="logical"></span>
                                    <input id="searchTags" type="text" class="form-control key-control" placeholder="keyword1, keyword2, ..." name="keywords">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="entity">Entidad</label>
                            <div class="col-sm-10">
                                <select class="form-control key-control" id="entity" name="entity">
                                    <option value="0">-- Elegir --</option>
                                    <option value="initiatives">Iniciativas</option>
                                    <option value="comisions">Comisiones</option>
                                    {{--Check the function of note search--}}
                                    {{--<option value="notes">Nota</option>--}}
                                </select>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <button class="btn btn-default clear-fields" type="button">Restablecer</button>
                        <button class="btn btn-primary pull-right" type="button" id="launchSearch">Buscar</button>
                    </div><!-- /.box-footer -->
                    {!! Form::close() !!}
                </div><!-- /.box -->

                <div class="unus-search-loading">
                    <img src="{{ URL::asset('dist/img/ripple.svg') }}">
                </div>

                <div id="criteriaMatch">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Resultados</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-group" id="accordion"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section><!-- /.content -->

@stop

@push('scripts')
<script>
    $(function () {
        var $search = $('#launchSearch');
        var $fields = $('.clear-fields');
        var $entity = $('#entity');
        var $tags = $('#searchTags');
        var $keyCodes = $('.key-control');

        $keyCodes.on('keydown', function (e) {
            if (e.which === 13) {
                search(e);
            }
        });
        $search.on('click', search);
        $fields.on('click', clearFields);

        function clearFields() {
            $(this).closest('form').find('input[type=text]').val('');
            $(this).closest('form').find('select').val('0');
        }

        function search(e) {
            e.preventDefault();
            var $id = $('#search');
            var $spinner = $('.unus-search-loading');
            var data = $id.serialize();

            if (!$tags.val()) {
                alert('Por favor ingrese al menos un keyword');
                return;
            }

            if ($entity.val() === '0') {
                alert('Por favor seleccione una Entidad');
                return;
            }

            $spinner.css('display', 'block');
            $.ajax({
                url: '{!! url('search/entity') !!}',
                type: 'POST',
                data: data
            }).done(function (response) {
                drawAcordion(response);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log('error', jqXHR, textStatus, errorThrown);
            }).always(function () {
                $spinner.css('display', 'none');
                clearFields();
            });
        }

        function drawAcordion(data) {
            var entities = data.entities;
            var type = data.type;
            var item = '';
            _.forEach(entities, function (value, key) {
                var entity = normalizeEntity(value, type);
                item += '<div class="panel box box-primary">';
                item += '<div class="box-header with-border">';
                item += '<h4 class="box-title">';
                item += '<a data-toggle="collapse" data-parent="#accordion" href="#collapse' + key + '">';
                item += entity.titulo;
                item += '</a>';
                item += '</h4>';
                item += '</div>';
                item += '<div id="collapse' + key + '" class="panel-collapse collapse">';
                item += '<div class="box-body">';
                item += '<table class="no-border" id="result' + key + '">';
                item += '<thead><tr><th>';
                item += '<p class="lead"><a href="' + entity.url + '/' + entity.id + '">' + entity.titulo + '</a></p>';
                item += '</th></tr></thead>';
                item += '<tbody><tr><td>';
                item += '<p class="text-muted">' + entity.descripcion + '</p>';
                item += '</td></tr>';

                item += '<tr><td><p class="text-muted"><span style="font-weight: lighter">Conformada por:</span></p></td></tr>';
                _.forEach(entity.partidos, function(partido) {
                    _.forEach(entity.personas, function (persona) {
                        _.forEach(entity.cargos, function (cargo) {
                            if (parseInt(partido.id) === parseInt(persona.idPartido)) {
                                if (parseInt(cargo.id) === parseInt(persona.pivot.position_id)) {
                                    item += '<tr><td><p><a href="{{ URL::to('person') }}/' + persona.id + '" class="padding-left-md">' + persona.nombre + ' ' + persona.apellidos + '</a> - (' + partido.siglas + ') ' + cargo.nombre + '</p></td></tr>';
                                }
                            }
                        });
                    });
                });

                if (type === 'initiatives') {
                    item += '<tr><td><p class="text-muted"><span style="font-weight: lighter">Presentación al pleno:</span> ' + entity.fechaPleno + '</p></td></tr>';
                    item += '<tr><td><p class="text-muted"><span style="font-weight: lighter">Publicación en gaceta:</span> ' + entity.fechaPublicacion + '</a></td></tr>';
                    if (entity.fechaDecreto) {
                        item += '<tr><td><p class="text-muted"><span style="font-weight: lighter">Publicación del decreto:</span> ' + entity.fechaDecreto + '</a></td></tr>';
                    }
                    if (entity.link) {
                        item += '<tr><td><p class="text-muted"><span style="font-weight: lighter">Link: </span><a href="' + entity.link + '" target="_blank">' + entity.link + '</a></p></td></tr>';
                    }
                    if (entity.decreto) {
                        item += '<tr><td><p class="text-muted"><span style="font-weight: lighter">Decreto: </span><a href="' + entity.decreto + '" target="_blank">' + entity.decreto + '</a></p></td></tr>';
                    }
                    item += '<tr><td><p class="text-muted"><span style="font-weight: lighter">Turnada a la Comisión:</span></p></td></tr>';
                    _.forEach(entity.turnados, function (turnado) {
                        _.forEach(entity.comisiones, function(comision) {
                            if (parseInt(turnado.id) === parseInt(comision.idCatComision)) {
                                item += '<tr><td><p><a href="{{ URL::to('comisions') }}/' + comision.id + '" class="padding-left-md">' + turnado.nombre + '</a></p></td></tr>';
                            }
                        });
                    });
                } else if (type === 'comisions') {
                    item += '<tr><td><p class="text-muted"><span style="font-weight: lighter">Iniciativas turnadas:</span></p></td></tr>';
                    _.forEach(entity.iniciativas, function (iniciativa) {
                        _.forEach(entity.estatus, function (estati) {
                            if (parseInt(estati.id) === parseInt(iniciativa.status_id)) {
                                item += '<tr><td><p><a href="{{ URL::to('initiatives') }}/' + iniciativa.id + '" class="padding-left-md">' + iniciativa.title + '</a> (' + estati.name + ')</p></td></tr>';
                            }
                        });
                    });
                } else if (type === 'notes') {

                }

                item += '</table>';
                item += '<p class="text-right"><a href="#" class="printer" id="' + key + '">Imprimir <i class="fa fa-file-pdf-o"></i></a></p>';
                item += '</div>';
                item += '</div>';
                item += '</div>';
            });
            $('#criteriaMatch').css('display', 'block');
            $('#accordion').html(item);

            $('.printer').on('click', function(event) {
                var paneId = event.target.id;
                generatePDF(paneId);
            });

        }

        function normalizeEntity(data, type) {
            var entity = {
                titulo: (data.titulo || '') || (data.title || '') || (data.nombre || '') || (data.name || '') || (data.nameCommission || ''),
                descripcion: (data.descripcion || '') || (data.description || ''),
                personas: data.persons,
                turnados: data.turnados,
                comisiones: data.comisiones,
                partidos: data.partidos,
                cargos: data.cargos,
                id: data.id
            };
            if (type === 'initiatives') {
                entity.fechaPleno = data.submit_date;
                entity.fechaPublicacion = data.publication_date;
                entity.fechaDecreto = data.fechaDecreto;
                entity.link = data.link;
                entity.decreto = data.decree;
                entity.estatus = data.status;
                entity.url = '{!! url('initiatives') !!}';
            } else if (type === 'comisions') {
                entity.iniciativas = data.iniciativas;
                entity.estatus = data.estatus;
                entity.url = '{!! url('comisions') !!}';
            } else if (type === 'notes') {
                entity.url = '{!! url('notes') !!}';
            }
            return entity;
        }

        function generatePDF(item) {
            var doc = new jsPDF('p', 'pt');
            var res = doc.autoTableHtmlToJson(document.getElementById('result' + item), true);
            doc.autoTable(res.columns, res.data, {
                theme: 'striped',
                margin: {
                    top: 50
                },
                styles: {
                    overflow: 'linebreak',
                    cellPadding: 5
                },
                beforePageContent: function(data) {
                    doc.setFontSize(12);
                }
            });
            doc.save('entityGenerated_' + item + '.pdf');
        }

    });
</script>
@endpush