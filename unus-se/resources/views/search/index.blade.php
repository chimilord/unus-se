@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Control Panel
            <small>Búsqueda</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Búsqueda</a></li>
            <li class="active">Persona</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Your Page Content Here -->
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Criterios de búsqueda</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    {{--<form class="form-horizontal">--}}
                    {!! Form::open(['id' => 'search', 'class' => 'form-horizontal']) !!}
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="nombre">Nombre</label>
                                <div class="col-sm-10">
                                    {!! Form::text('nombre', null, ['id' => 'nombre', 'class' => 'form-control text-uppercase']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="apellidos">Apellidos</label>
                                <div class="col-sm-10">
                                    {!! Form::text('apellidos', null, ['id' => 'apellidos', 'class' => 'form-control text-uppercase']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="idTipoPersona">Tipo Persona</label>
                                <div class="col-sm-10">
                                    {!! Form::select('idTipoPersona', array_merge(['0' => '-- Seleccionar --'], $tipos), null, array('id' => 'idTipoPersona', 'class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="idEstado">Entidad Federativa</label>
                                <div class="col-sm-10">
                                    {!! Form::select('idEstado', array_merge(['0' => '-- Seleccionar --'], $estados), null, array('id' => 'idEstado', 'class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="idEstatus">Estatus</label>
                                <div class="col-sm-10">
                                    {!! Form::select('idEstatus', array_merge(['0' => '-- Seleccionar --'], $estatus), null, array('id' => 'idEstatus', 'class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="idPartido">Partido político</label>
                                <div class="col-sm-10">
                                    {!! Form::select('idPartido', array_merge(['0' => '-- Seleccionar --'], $partidos), null, array('id' => 'idPartido', 'class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="idTipoEleccion">Principio de elección</label>
                                <div class="col-sm-10">
                                    {!! Form::select('idEleccion', array_merge(['0' => '-- Seleccionar --'], $elecciones), null, array('id' => 'idTipoEleccion', 'class' => 'form-control')) !!}
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-default clear-fields" type="button">Restablecer campos</button>
                            <button class="btn btn-primary pull-right" type="button" id="launchSearch">Buscar</button>
                        </div><!-- /.box-footer -->
                    {{--</form>--}}
                    {!! Form::close() !!}
                </div><!-- /.box -->

                <div class="unus-search-loading">
                    <img src="dist/img/ripple.svg">
                </div>

                <div class="box" id="searchResult">
                    <div class="box-header">
                        <h3 class="box-title">Resultados</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="searchDataTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <td>Dirección</td>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <td>Dirección</td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@push('scripts')
<script>
    $(function () {
        var $search = $('#launchSearch');
        var $fields = $('.clear-fields');

        $fields.on('click', clearFields);
        $search.on('click', search);

        function clearFields() {
            $(this).closest('form').find('input[type=text]').val('');
            $(this).closest('form').find('select').val('0');
        }

        function search() {
            var $id = $('#search');
            var $spinner = $('.unus-search-loading');
            var $tipoPersona = $('#idTipoPersona');
            var data = $id.serialize();

            if ($tipoPersona.val() <= 0) {
                alert('Por favor seleccione un Tipo Persona');
                return;
            }

            $spinner.css('display', 'block');
            $.ajax({
                url: '{!! url('search') !!}',
                type: 'POST',
                data: data
            }).done(function(response) {
                drawDataDT(response);
            }).fail(function(jqXHR, textStatus, errorThrown) {
                console.log('error', jqXHR, textStatus, errorThrown);
            }).always(function() {
                $spinner.css('display', 'none');
                clearFields();
            });
        }

        function drawDataDT(dataSet) {
            var $searchDT = $('#searchDataTable');
            var $searchResult = $('#searchResult');

            $searchDT.removeClass('hidden');
            $searchResult.css('display', 'block');

            $searchDT.DataTable({
                destroy: true,
                aaData: dataSet,
                aoColumns: [
                    {
                        "mData": "nombre",
                        "mRender": function (data, type, full) {
                            return '<a href="{{ url('person') }}/' + full.id + '">' + full.nombre + ' ' + full.apellidos + '</a>';
                        }
                    },
                    {"mData": "email"},
                    {"mData": "direccion"}
                ],
                language: {
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "zeroRecords": "No se encontraron registros",
                    "info": "_PAGE_ de _PAGES_",
                    "infoEmpty": "Información vacía",
                    "infoFiltered": "(filtrando de _MAX_ en total)",
                    "search": "Buscar:",
                    "processing": "Cargando...",
                    paginate: {
                        previous: '‹',
                        next: '›'
                    }
                }
            });
        }

    });
</script>
@endpush