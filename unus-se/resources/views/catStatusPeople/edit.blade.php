@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Control Panel
            <small>Catálogos</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Catálogos</a></li>
            <li class="active">Estatus Persona</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Estatus Persona</h3>
                        <div class="pull-right">
                            <a href="{!! route('catStatusPeople.index') !!}" class="btn btn-primary icon-button"><i class="fa fa-list-ul"></i> Catálogo completo</a>
                        </div>
                    </div>
                    <div class="box-body">
                        @include('common.errors')

                        {!! Form::model($catStatusPerson, ['route' => ['catStatusPeople.update', $catStatusPerson->id], 'method' => 'patch', 'class' => 'form-horizontal']) !!}

                        @include('catStatusPeople.fields', ['action' => 'Actualizar'])

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop