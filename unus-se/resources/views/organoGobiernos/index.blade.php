@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Control Panel
            <small>Entidades</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Entidad</a></li>
            <li class="active">Organo</li>
        </ol>
    </section>

    <section class="content">
        @include('flash::message')
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Organo</h3>
                        <div class="pull-right">
                            <a href="{!! route('organoGobiernos.create') !!}" class="btn btn-primary icon-button"><i class="fa fa-plus-circle"></i> Crear Organo</a>
                        </div>
                    </div>
                    <div class="box-body">
                        @if($organoGobiernos->isEmpty())
                            <div class="well text-center">No se han encontrado Organos de Gobierno.</div>
                        @else
                            @include('organoGobiernos.table')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop

