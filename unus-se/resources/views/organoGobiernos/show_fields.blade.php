<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('catOrgano', 'Organo de Gobierno', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $catOrgano->nombre !!}</p>
    </div>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('periodo', 'Periodo', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <p class="form-control-static">{!! $periodo !!}</p>
    </div>
</div>

<!-- Person Id Field -->
<div class="form-group">
    {!! Form::label('person_id', 'Presentada por', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        @foreach($persons as $person)
            <p class="form-control-static"><a href="{{ URL::to('person')}}/{!! $person->id !!}">{!! $person->nombre . ' ' . $person->apellidos !!}</a></p>
        @endforeach
    </div>
</div>