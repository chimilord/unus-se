@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Control Panel
            <small>Dashboard</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-md-8">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Últimas modificaciones</h3>
                        <div class="box-tools pull-right">
                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        @if (count($updates) > 0)
                            <div class="table-responsive">
                                <table class="table table-hover no-margin last-updates">
                                    <thead>
                                    <tr>
                                        <th>Usuario</th>
                                        <th>Acciones</th>
                                        <th>Fecha</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($updates as $update)
                                        <tr>
                                            <td>{{ $update->name }} {{ $update->lastname }}</td>
                                            <td>{{ $update->text }}</td>
                                            <td>{{ date('d/m/Y H:i:s', strtotime($update->updated_at)) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div><!-- /.table-responsive -->
                        @else
                            No hay actualizaciones disponibles.
                        @endif
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>

            <div class="col-md-4">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Últimas sesiones</h3>
                        <div class="box-tools pull-right">
                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-hover no-margin last-sessions">
                                <thead>
                                <tr>
                                    <th>Usuario</th>
                                    <th>Fecha</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($sessions as $session)
                                    <tr>
                                        <td>{{ $session->name }} {{ $session->lastname }}</td>
                                        <td>{{ date('d/m/Y H:i:s', strtotime($session->updated_at)) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div><!-- /.table-responsive -->
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Últimas notas</h3>
                        <div class="box-tools pull-right">
                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        @if (count($notes) > 0)
                        <div class="table-responsive">
                            <table class="table table-hover no-margin">
                                <thead>
                                <tr>
                                    <th>Título</th>
                                    <th>Usuario</th>
                                    <th>Fecha de creación</th>
                                    <th>Prioridad</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($notes as $note)
                                    <tr>
                                        <td><a href="{{ URL::to('notes') }}/{{$note->id}}">{{ $note->titulo }}</a></td>
                                        <td>{{ $note->name }} {{ $note->lastname }}</td>
                                        <td>{{ date('d/m/Y H:i:s', strtotime($note->created_at)) }}</td>
                                        <td>
                                            @if ($note->prioridad == 0)
                                                <small class="label label-default text-uppercase">baja</small>
                                            @elseif($note->prioridad == 1)
                                                <small class="label label-warning text-uppercase">media</small>
                                            @elseif($note->prioridad == 2)
                                                <small class="label label-danger text-uppercase">alta</small>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($note->status == 0)
                                                <small class="label label-danger text-uppercase">abierto</small>
                                            @elseif($note->status == 1)
                                                <small class="label label-warning text-uppercase">progreso</small>
                                            @elseif($note->status == 2)
                                                <small class="label label-success text-uppercase">cerrado</small>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div><!-- /.table-responsive -->
                        @else
                            No hay notas disponibles.
                        @endif
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
            <div class="col-sm-4">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Seguimiento</h3>
                        <div class="box-tools pull-right">
                            <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        @if (count($trackings) > 0)
                            <div class="table-responsive">

                                <table class="table table-hover no-margin">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Seguimiento</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach ($trackings as $tracking)
                                        <tr>
                                            <td>
                                                <a href="{{ URL::to('person') }}/{{$tracking->id}}">{{ $tracking->nombre }} {{ $tracking->apellidos }}</a>
                                            </td>
                                            <td>{{ date('d/m/Y H:i:s', strtotime($tracking->updated_at)) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div><!-- /.table-responsive -->
                        @else
                            No hay seguimientos disponibles.
                        @endif
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8">
                <div class="box box-default">
                    <div class="box-body no-padding">
                        <!-- THE CALENDAR -->
                        <div id="homeCalendar"></div>
                    </div><!-- /.box-body -->
                </div><!-- /. box -->
            </div>
            <div class="col-sm-4">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h4 class="box-title">Prioridad</h4>
                    </div>
                    <div class="box-body">
                        <!-- the events -->
                        <div id="external-events">
                            <div class="event label-default" data-priority="0">Baja</div>
                            <div class="event label-warning" data-priority="1">Media</div>
                            <div class="event label-danger" data-priority="2">Alta</div>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /. box -->
            </div>
        </div>

        <!-- END ALERTS AND CALLOUTS -->
    </section>

@stop

@push('scripts')
<script>
    $(function () {
        $('#homeCalendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            buttonText: {
                today: 'hoy',
                month: 'mes',
                week: 'semana',
                day: 'día'
            },
            lang: 'es',
            timezone: 'America/Mexico_City',
            events: '{!! url('home/notes') !!}',
            editable: true,
            droppable: false
        });
    });
</script>
@endpush