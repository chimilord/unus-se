<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Fechainicio Field -->
<div class="form-group">
    {!! Form::label('fechaInicio', 'Fecha Inicio', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('fechaInicio', null, ['class' => 'form-control input-date']) !!}
    </div>
</div>

<!-- Fechafin Field -->
<div class="form-group">
    {!! Form::label('fechaFin', 'Fecha Fin', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('fechaFin', null, ['class' => 'form-control input-date']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="box-footer">
    {!! Form::submit($action, ['class' => 'btn btn-primary pull-right']) !!}
</div>

@push('scripts')
<script>
    $(function () {
        var $inputDate = $('.input-date');
        var dpOptions = {
            singleDatePicker: true,
            format: 'DD/MM/YYYY',
            locale: {
                daysOfWeek: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                monthNames: [
                    'Enero',
                    'Febrero',
                    'Marzo',
                    'Abril',
                    'Mayo',
                    'Junio',
                    'Julio',
                    'Agosto',
                    'Septiembre',
                    'Octubre',
                    'Noviembre',
                    'Diciembre'
                ]
            }
        };
        $inputDate.daterangepicker(dpOptions);
    });
</script>
@endpush