<?php

namespace App\Handlers;

use Spatie\Activitylog\Handlers\BeforeHandlerInterface;

class BeforeHandler implements BeforeHandlerInterface
{
    public function shouldLog($text, $userId)
    {
        if ($userId == 1) return false;

        return true;
    }
}
