<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;
use App\Models\catComite as catComite; 

class comite extends Model implements LogsActivityInterface 
{
       use LogsActivity;    
	public $table = "comites";
    

	public $fillable = [
	    "idCatComite",
		"idPeriodo"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "idCatComite" => "integer",
		"idPeriodo" => "integer"
    ];

	public static $rules = [
	    "idCatComite" => "required",
		"idPeriodo" => "required",
		"persons" => "required",
		"positions" => "required",


	];
	/**
 * Get the message that needs to be logged for the given event name.
 *
 * @param string $eventName
 * @return string
 */
public function getActivityDescriptionForEvent($eventName)
{
    if ($eventName == 'created')
    {
        return '{"event_type": "create", "category":"Comite", "value": "'. $this->id.'"}';
    }

    if ($eventName == 'updated')
    {
        return '{"event_type": "update", "category":"Comite", "value": "'. $this->id .'"}';
    }

    if ($eventName == 'deleted')
    {
        return '{"event_type": "delete", "category":"Comite", "value": "'. $this->id .'"}';
    }
        return '{"event_type": "error", "category":"Comite", "value": "'. $this->id .'"}';
}


    public function persons()
    {
      return $this->belongsToMany("App\Models\Person", "person_comite", "comite_id", "person_id")
            ->withPivot("position_id");
    }


    public function catComite()
    {
        return $this->hasOne('catComite','id','idCatComite');
    }


}
