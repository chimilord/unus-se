<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;
class catStatusTurnado extends Model implements LogsActivityInterface 
{
       use LogsActivity;    
    
	public $table = "catStatusTurnados";
    

	public $fillable = [
	    "nombre"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "nombre" => "string"
    ];

	public static $rules = [
	    "nombre" => "required"
	];

    public function iniciativas()
    {
        return $this->belongsToMany('App\Models\Initiative');
    }

    public function comisiones()
    {
        return $this->belongsToMany('App\Models\Comision');
    }


         /**
     * Get the message that needs to be logged for the given event name.
     *
     * @param string $eventName
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return '{"event_type": "create", "category":"Status Turnado", "value": "'. $this->estatus .'"}';
        }

        if ($eventName == 'updated')
        {
            return '{"event_type": "update", "category":"Status Turnado", "value": "'. $this->estatus .'"}';
        }

        if ($eventName == 'deleted')
        {
            return '{"event_type": "delete", "category":"Status Turnado", "value": "'. $this->estatus .'"}';
        }
            return '{"event_type": "error", "category":"Status Turnado", "value": "'. $this->estatus .'"}';
    }

}
