<?php namespace App\Models;

use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;



use Illuminate\Database\Eloquent\Model as Model;

class Estado extends Model  implements LogsActivityInterface 
{
   use LogsActivity;    
	public $table = "estados";
    

	public $fillable = [
	    "nombre",
		"capital"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "nombre" => "string",
		"capital" => "string"
    ];

	public static $rules = [
	    "nombre" => "required",
		"capital" => "required"
	];

	/**
 * Get the message that needs to be logged for the given event name.
 *
 * @param string $eventName
 * @return string
 */
public function getActivityDescriptionForEvent($eventName)
{
    if ($eventName == 'created')
    {
        return '{"event_type": "create", "category":"Estado", "value": "'. $this->nombre .'"}';
    }

    if ($eventName == 'updated')
    {
        return '{"event_type": "update", "category":"Estado", "value": "'. $this->nombre .'"}';
    }

    if ($eventName == 'deleted')
    {
        return '{"event_type": "delete", "category":"Estado", "value": "'. $this->nombre .'"}';
    }
        return '{"event_type": "error", "category":"Estado", "value": "'. $this->nombre .'"}';
}
}
