<?php namespace App\Models;

use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;

use Illuminate\Database\Eloquent\Model as Model;

class Partido extends Model implements LogsActivityInterface 
{
       use LogsActivity;    
    
	public $table = "partidos";
    

	public $fillable = [
	    "partido",
		"siglas"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "partido" => "string",
		"siglas" => "string"
    ];

	public static $rules = [
	    "partido" => "required",
		"siglas" => "required"
	];

	
/**
     * Get the message that needs to be logged for the given event name.
     *
     * @param string $eventName
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return '{"event_type": "create", "category":"Partido", "value": "'. $this->partido .'"}';
        }

        if ($eventName == 'updated')
        {
            return '{"event_type": "update", "category":"Partido", "value": "'. $this->partido .'"}';
        }

        if ($eventName == 'deleted')
        {
            return '{"event_type": "delete", "category":"Partido", "value": "'. $this->partido .'"}';
        }
            return '{"event_type": "error", "category":"Partido", "value": "'. $this->partido .'"}';
    }


}
