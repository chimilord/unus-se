<?php namespace App\Models;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;
use Illuminate\Database\Eloquent\Model as Model;

class Person extends Model implements LogsActivityInterface 
{
       use LogsActivity;    
    
    
	public $table = "person";
    

	public $fillable = [
	    "nombre",
		"apellidos",
		"fotografia",
		"idEstado",
		"idDistrito",
		"localidad",
		"idStatusPerson",
		"suplente",
		"idPartido",
		"idTipoEleccion",
		"estudios",
		"trayectoriaAcademica",
		"experienciaLegislativa",
		"experienciaPolitica",
		"experienciaEmpresarial",
		"experienciaOtra",
		"datosComplementarios",
		"organoGobierno",
		"direccion",
		"email",
		"telefono",
		"trayectoriaAdministrativa",
		"seguimiento",
		"idTipoPersona",
		"otrasFunciones",
		"seguimiento",
		"idTipoPersona",
		"inicioPeriodo",
		"finPeriodo",
		"nombrePeriodo",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "nombre" => "string",
		"apellidos" => "string",
		"fotografia" => "string",
		"idEstado" => "integer",
		"idDistrito" => "integer",
		"localidad" => "string",
		"idStatusPerson" => "integer",
		"suplente" => "string",
		"idPartido" => "integer",
		"idTipoEleccion" => "integer",
		"estudios" => "string",
		"trayectoriaAcademica" => "string",
		"experienciaLegislativa" => "string",
		"experienciaPolitica" => "string",
		"experienciaEmpresarial" => "string",
		"experienciaOtra" => "string",
		"datosComplementarios" => "string",
		"organoGobierno" => "string",
		"direccion" => "string",
		"email" => "string",
		"telefono"=> "string",
		"trayectoriaAdministrativa"=> "string",
		"seguimiento"=> "boolean",
		"idTipoPersona"=> "integer",
		"otrasFunciones"=> "string",

    ];

	public static $rules = [
	    "nombre" => "required|max:60",
		"apellidos" => "required|max:60",
		"idStatusPerson" => "required",
		"suplente" => "required",
		"idPartido" => "required",
		"idTipoEleccion" => "required",
		"estudios" => "required",
		"email" => "email",
		"idTipoPersona"=> "required",
	];


/**
     * Get the message that needs to be logged for the given event name.
     *
     * @param string $eventName
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return '{"event_type": "create", "category":"Persona", "value": "'. $this->nombre .'"}';
        }

        if ($eventName == 'updated')
        {
            return '{"event_type": "update", "category":"Persona", "value": "'. $this->nombre .'"}';
        }

        if ($eventName == 'deleted')
        {
            return '{"event_type": "delete", "category":"Persona", "value": "'. $this->nombre .'"}';
        }
            return '{"event_type": "error", "category":"Persona", "value": "'. $this->nombre .'"}';
    }

	/**
	 * @return fullname
	 */
	public function getFullNameAttribute()
	{
		return $this->attributes['nombre'] . ' ' . $this->attributes['apellidos'];
	}


 public function PersonInitiative()
    {
        return $this->belongsToMany('App\Models\Initiative', "person_iniciativa","person_id","initiatives_id")->withPivot("position_id");
    }

    public function PersonComision()
    {
        return $this->belongsToMany('App\Models\Initiative', "person_commission","person_id","comision_id")->withPivot("position_id");
    }
  	public function PersonOrgano()
    {
        return $this->belongsToMany('App\Models\organoGobierno', "person_organo","person_id","organo_id")->withPivot("position_id");
    }
}
