<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;

class OrganoGobierno extends Model implements LogsActivityInterface 
{
   use LogsActivity;    
    
	public $table = "organoGobiernos";
    

	public $fillable = [
	    "idCatOrganoGobierno",
		"idPeriodo"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "idCatOrganoGobierno" => "integer",
		"idPeriodo" => "integer"
    ];

	public static $rules = [
	    "idCatOrganoGobierno" => "required",
		"idPeriodo" => "required"
	];

	/**
     * Get the message that needs to be logged for the given event name.
     *
     * @param string $eventName
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return '{"event_type": "create", "category":"Organo Gobierno", "value": "'. $this->idCatOrganoGobierno .'"}';
        }

        if ($eventName == 'updated')
        {
            return '{"event_type": "update", "category":"Organo Gobierno", "value": "'. $this->idCatOrganoGobierno .'"}';
        }

        if ($eventName == 'deleted')
        {
            return '{"event_type": "delete", "category":"Organo Gobierno", "value": "'. $this->idCatOrganoGobierno .'"}';
        }
            return '{"event_type": "error", "category":"Organo Gobierno", "value": "'. $this->idCatOrganoGobierno .'"}';
    }

	public function persons()
    {
        return $this->belongsToMany("App\Models\Person", "person_organo", "organo_id", "person_id")
            ->withPivot("position_id");
    }
}
