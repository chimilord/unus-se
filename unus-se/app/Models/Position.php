<?php namespace App\Models;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;
use Illuminate\Database\Eloquent\Model as Model;

class Position extends Model implements LogsActivityInterface 
{
    use LogsActivity;    
	public $table = "positions";
    

	public $fillable = [
	    "nombre",
		"descripcion"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "nombre" => "string",
		"descripcion" => "string"
    ];

	public static $rules = [
	    "nombre" => "required"
	];

	public function commissions()
	{
		return $this->belongsToMany('App\Comision');
	}

	public function persons()
	{
		return $this->belongsToMany('App\Models\Person');
	}

	/**
     * Get the message that needs to be logged for the given event name.
     *
     * @param string $eventName
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return '{"event_type": "create", "category":"Posicion", "value": "'. $this->nombre .'"}';
        }

        if ($eventName == 'updated')
        {
            return '{"event_type": "update", "category":"Posicion", "value": "'. $this->nombre .'"}';
        }

        if ($eventName == 'deleted')
        {
            return '{"event_type": "delete", "category":"Posicion", "value": "'. $this->nombre .'"}';
        }
            return '{"event_type": "error", "category":"Posicion", "value": "'. $this->nombre .'"}';
    }
}
