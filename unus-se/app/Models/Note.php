<?php namespace App\Models;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;
use Illuminate\Database\Eloquent\Model as Model;

class Note extends Model implements LogsActivityInterface 
{
   use LogsActivity;    
	public $table = "notes";
    

	public $fillable = [
	    "titulo",
		"descripcion",
		"keywords",
		"prioridad",
		"status",
		"idPersona",
		"idUser",
		"idSeguimiento"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "titulo" => "string",
		"descripcion" => "string",
		"keywords" => "string"
    ];

	public static $rules = [
	    "titulo" => "required",
		"descripcion" => "required",
		"keywords" => "required"
	];

/**
     * Get the message that needs to be logged for the given event name.
     *
     * @param string $eventName
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return '{"event_type": "create", "category":"Nota", "value": "'. $this->titulo .'"}';
        }

        if ($eventName == 'updated')
        {
            return '{"event_type": "update", "category":"Nota", "value": "'. $this->titulo .'"}';
        }

        if ($eventName == 'deleted')
        {
            return '{"event_type": "delete", "category":"Nota", "value": "'. $this->titulo .'"}';
        }
            return '{"event_type": "error", "category":"Nota", "value": "'. $this->titulo .'"}';
    }

}
