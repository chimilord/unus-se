<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;

class catTipoComision extends Model implements LogsActivityInterface 
{
       use LogsActivity;    
    
	public $table = "catTipoComision";
    

	public $fillable = [
	    "nombre"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "nombre" => "string"
    ];

	public static $rules = [
	    "nombre" => "required"
	];


         /**
     * Get the message that needs to be logged for the given event name.
     *
     * @param string $eventName
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return '{"event_type": "create", "category":"Tipo Comision", "value": "'. $this->estatus .'"}';
        }

        if ($eventName == 'updated')
        {
            return '{"event_type": "update", "category":"Tipo Comision", "value": "'. $this->estatus .'"}';
        }

        if ($eventName == 'deleted')
        {
            return '{"event_type": "delete", "category":"Tipo Comision", "value": "'. $this->estatus .'"}';
        }
            return '{"event_type": "error", "category":"Tipo Comision", "value": "'. $this->estatus .'"}';
    }

}
