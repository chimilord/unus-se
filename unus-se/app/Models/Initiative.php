<?php namespace App\Models;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;
use Illuminate\Database\Eloquent\Model as Model;

class Initiative extends Model implements LogsActivityInterface 
{
       use LogsActivity;    
    
	public $table = "initiatives";
    

	public $fillable = [
	    "title",
		"description",
		"submit_date",
		"publication_date",
		"status_id",
		"link",
		"decree",
		"keywords",
		"fechaDecreto",
		"idCamara",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "status_id" => "integer",
		"link" => "string",
		"idCamara" => "integer",
    ];

	public static $rules = [
	    "title" => "required|max:2048",
		"description" => "required",
		"submit_date" => "required",
		"publication_date" => "required",
		"status_id" => "required",
		"persons" => "required",
		"link" => "required|max:255",
		"decree" => "max:255",
		"keywords" => "required",
		"idCamara" => "required",
		"comisiones" => "required"
	];


/**
     * Get the message that needs to be logged for the given event name.
     *
     * @param string $eventName
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return '{"event_type": "create", "category":"Iniciativa", "value": "'. $this->title .'"}';
        }

        if ($eventName == 'updated')
        {
            return '{"event_type": "update", "category":"Iniciativa", "value": "'. $this->title .'"}';
        }

        if ($eventName == 'deleted')
        {
            return '{"event_type": "delete", "category":"Iniciativa", "value": "'. $this->title .'"}';
        }
            return '{"event_type": "error", "category":"Iniciativa", "value": "'. $this->title .'"}';
    }



	public function persons()
    {
        return $this->belongsToMany("App\Models\Person", "person_iniciativa", "initiatives_id", "person_id")
            ->withPivot("position_id");
    }

    public function comisiones()
    {
        return $this->belongsToMany("App\Models\Comision", "iniciativa_comision", "iniciativa_id", "comision_id")
            ->withPivot("idStatusTurnado");
    }

}
