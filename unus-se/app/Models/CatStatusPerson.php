<?php namespace App\Models;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;

use Illuminate\Database\Eloquent\Model as Model;

class CatStatusPerson extends Model implements LogsActivityInterface 
{
       use LogsActivity;    
    
	public $table = "catStatusPeople";
    

	public $fillable = [
	    "estatus"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "estatus" => "string"
    ];

	public static $rules = [
	    "estatus" => "required"
	];

         /**
     * Get the message that needs to be logged for the given event name.
     *
     * @param string $eventName
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return '{"event_type": "create", "category":"Status Persona", "value": "'. $this->estatus .'"}';
        }

        if ($eventName == 'updated')
        {
            return '{"event_type": "update", "category":"Status Persona", "value": "'. $this->estatus .'"}';
        }

        if ($eventName == 'deleted')
        {
            return '{"event_type": "delete", "category":"Status Persona", "value": "'. $this->estatus .'"}';
        }
            return '{"event_type": "error", "category":"Status Persona", "value": "'. $this->estatus .'"}';
    }

}
