<?php namespace App\Models;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;

use Illuminate\Database\Eloquent\Model as Model;

class catCamara extends Model implements LogsActivityInterface 
{
       use LogsActivity;    

	public $table = "catCamaras";
    

	public $fillable = [
	    "name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string"
    ];

	public static $rules = [
	    "name" => "required|max:100"
	];

        /**
 * Get the message that needs to be logged for the given event name.
 *
 * @param string $eventName
 * @return string
 */
public function getActivityDescriptionForEvent($eventName)
{
    if ($eventName == 'created')
    {
        return '{"event_type": "create", "category":"Camara", "value": "'. $this->name .'"}';
    }

    if ($eventName == 'updated')
    {
        return '{"event_type": "update", "category":"Camara", "value": "'. $this->name .'"}';
    }

    if ($eventName == 'deleted')
    {
        return '{"event_type": "delete", "category":"Camara", "value": "'. $this->name .'"}';
    }
        return '{"event_type": "error", "category":"Camara", "value": "'. $this->name .'"}';
}

}
