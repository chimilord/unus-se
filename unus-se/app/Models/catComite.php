<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;
class catComite extends Model implements LogsActivityInterface 
{
       use LogsActivity;    
    
	public $table = "catComites";
    

	public $fillable = [
	    "nombre"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "nombre" => "string"
    ];

	public static $rules = [
	    "nombre" => "required"
	];


          /**
     * Get the message that needs to be logged for the given event name.
     *
     * @param string $eventName
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return '{"event_type": "create", "category":"Cat Comite", "value": "'. $this->nombre .'"}';
        }

        if ($eventName == 'updated')
        {
            return '{"event_type": "update", "category":"Cat Comite", "value": "'. $this->nombre .'"}';
        }

        if ($eventName == 'deleted')
        {
            return '{"event_type": "delete", "category":"Cat Comite", "value": "'. $this->nombre .'"}';
        }
            return '{"event_type": "error", "category":"Cat Comite", "value": "'. $this->nombre .'"}';
    }
}
