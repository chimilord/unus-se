<?php namespace App\Models;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;
use Illuminate\Database\Eloquent\Model as Model;

class Comision extends Model implements LogsActivityInterface 
{
       use LogsActivity;    
    
	public $table = "comisions";
    

	public $fillable = [
        "keywords",
        "idCatComision",
        "idPeriodo",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "idCatComision"=>"integer",
        "idPeriodo"=>"integer"
    ];

	public static $rules = [
        "persons" => "required",
        "idCatComision"=>"required",
        "idPeriodo"=>"required",
        "persons" => "required",
        "positions" => "required",
	];

/**
     * Get the message that needs to be logged for the given event name.
     *
     * @param string $eventName
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return '{"event_type": "create", "category":"Comision", "value": "'. $this->catComisiones->nombre .'"}';
        }

        if ($eventName == 'updated')
        {
            return '{"event_type": "update", "category":"Comision", "value": "'. $this->catComisiones->nombre .'"}';
        }

        if ($eventName == 'deleted')
        {
            return '{"event_type": "delete", "category":"Comision", "value": "'. $this->catComisiones->nombre .'"}';
        }
            return '{"event_type": "error", "category":"Comision", "value": "'. $this->catComisiones->nombre .'"}';
    }


    public function persons()
    {
      return $this->belongsToMany("App\Models\Person", "person_commission", "comision_id", "person_id")
            ->withPivot("position_id");
    }

      public function iniciativas()
    {
        return $this->belongsToMany("App\Models\Initiative", "iniciativa_comision", "comision_id", "iniciativa_id")
            ->withPivot("idStatusTurnado");
    }


    public function catComisiones()
    {
        return $this->hasOne('App\Models\catComisiones','id','idCatComision');
    }
}
