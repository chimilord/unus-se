<?php namespace App\Models;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;

use Illuminate\Database\Eloquent\Model as Model;

class catComisiones extends Model implements LogsActivityInterface 
{
       use LogsActivity;    
    
	public $table = "catComisiones";
    

	public $fillable = [
	    "nombre",
        "idTipoComision", 
        "descripcion"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "nombre" => "string",
        "idTipoComision" => "integer", 
        "descripcion"=>"string"

        
    ];

	public static $rules = [
	    "nombre" => "required",
        "idTipoComision" => "required"
	];

          /**
     * Get the message that needs to be logged for the given event name.
     *
     * @param string $eventName
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return '{"event_type": "create", "category":"Cat Comision", "value": "'. $this->nombre .'"}';
        }

        if ($eventName == 'updated')
        {
            return '{"event_type": "update", "category":"Cat Comision", "value": "'. $this->nombre .'"}';
        }

        if ($eventName == 'deleted')
        {
            return '{"event_type": "delete", "category":"Cat Comision", "value": "'. $this->nombre .'"}';
        }
            return '{"event_type": "error", "category":"Cat Comision", "value": "'. $this->nombre .'"}';
    }
}
