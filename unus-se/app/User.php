<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;

class User extends Authenticatable implements LogsActivityInterface 
{
        protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','lastname','avatar','phone', 'role_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $rules = [
        "name" => "required|max:60",
        "email" => "required",
      //  "password" => "required",
        "role_id" => "required"
    ];
       /**
 * Get the message that needs to be logged for the given event name.
 *
 * @param string $eventName
 * @return string
 */
public function getActivityDescriptionForEvent($eventName)
{
    if ($eventName == 'created')
    {
        return '{"event_type": "create", "category":"User", "value": "'. $this->name .'"}';
    }

    if ($eventName == 'updated')
    {
        return '{"event_type": "update", "category":"User", "value": "'. $this->name .'"}';
    }

    if ($eventName == 'deleted')
    {
        return '{"event_type": "delete", "category":"User", "value": "'. $this->name .'"}';
    }
        return '{"event_type": "error", "category":"User", "value": "'. $this->name .'"}';
}

public function role()
    {
        return $this->hasOne('App\Role', 'id', 'role_id');
    }

    public function hasRole($roles)
    {
        $this->have_role = $this->getUserRole();
        // Check if the user is a root account
        if($this->have_role->name == 'Root') {
            return true;
        }
        if(is_array($roles)){
            foreach($roles as $need_role){
                if($this->checkIfUserHasRole($need_role)) {
                    return true;
                }
            }
        } else{
            return $this->checkIfUserHasRole($roles);
        }
        return false;
    }
    private function getUserRole()
    {
        return $this->role()->getResults();
    }
    private function checkIfUserHasRole($need_role)
    {
        return (strtolower($need_role)==strtolower($this->have_role->name)) ? true : false;
    }

    /**
     * @return fullname
     */
    public function getFullNameAttribute()
    {
        return $this->attributes['name'] . ' ' . $this->attributes['lastname'];
    }

}
