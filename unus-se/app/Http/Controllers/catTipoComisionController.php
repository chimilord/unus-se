<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatecatTipoComisionRequest;
use App\Http\Requests\UpdatecatTipoComisionRequest;
use App\Libraries\Repositories\catTipoComisionRepository;
use App\Models\catTipoComision;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;

class catTipoComisionController extends AppBaseController
{

	/** @var  catTipoComisionRepository */
	private $catTipoComisionRepository;

	function __construct(catTipoComisionRepository $catTipoComisionRepo)
	{
        $this->middleware('rolauth');		
		$this->catTipoComisionRepository = $catTipoComisionRepo;
	}

	/**
	 * Display a listing of the catTipoComision.
	 *
	 * @return Response
	 */
	public function index()
	{
		$catTipoComisions = $this->catTipoComisionRepository->paginate(10);

		return view('catTipoComisions.index')
			->with('catTipoComisions', $catTipoComisions);
	}

	/**
	 * Show the form for creating a new catTipoComision.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('catTipoComisions.create');
	}

	/**
	 * Store a newly created catTipoComision in storage.
	 *
	 * @param CreatecatTipoComisionRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatecatTipoComisionRequest $request)
	{
		$input = $request->all();

		$catTipoComision = $this->catTipoComisionRepository->create($input);

		Flash::success('Tipo Comisión creado correctamente.');

		return redirect(route('catTipoComisions.index'));
	}

	/**
	 * Display the specified catTipoComision.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$catTipoComision = $this->catTipoComisionRepository->find($id);

		if(empty($catTipoComision))
		{
			Flash::error('Tipo Comisión no encontrado.');

			return redirect(route('catTipoComisions.index'));
		}

		return view('catTipoComisions.show')->with('catTipoComision', $catTipoComision);
	}

	/**
	 * Show the form for editing the specified catTipoComision.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$catTipoComision = $this->catTipoComisionRepository->find($id);

		if(empty($catTipoComision))
		{
			Flash::error('Tipo Comisión no encontrado.');

			return redirect(route('catTipoComisions.index'));
		}

		return view('catTipoComisions.edit')->with('catTipoComision', $catTipoComision);
	}

	/**
	 * Update the specified catTipoComision in storage.
	 *
	 * @param  int              $id
	 * @param UpdatecatTipoComisionRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatecatTipoComisionRequest $request)
	{
		$catTipoComision = $this->catTipoComisionRepository->find($id);

		if(empty($catTipoComision))
		{
			Flash::error('Tipo Comisión no encontrado.');

			return redirect(route('catTipoComisions.index'));
		}

		$this->catTipoComisionRepository->updateRich($request->all(), $id);

		Flash::success('Tipo Comisión actualizado correctamente.');

		return redirect(route('catTipoComisions.index'));
	}

	/**
	 * Remove the specified catTipoComision from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$catTipoComision = $this->catTipoComisionRepository->find($id);

		if(empty($catTipoComision))
		{
			Flash::error('Tipo Comisión no encontrado.');

			return redirect(route('catTipoComisions.index'));
		}

		$this->catTipoComisionRepository->delete($id);

		Flash::success('Tipo Comisión eliminado correctamente.');

		return redirect(route('catTipoComisions.index'));
	}

	/**
	 * @return mixed
	 */
	public function data() {
		$tipoComision = catTipoComision::select('*');
		$tipoComisionDT = Datatables::of($tipoComision)
			->addColumn('action', function($tipo) {
				$actions = '<div class="text-center button-action">';
				$actions .= '<a href="' . route('catTipoComisions.edit', [$tipo->id]) . '"><i class="fa fa-edit"></i></a>';
				$actions .= '<a href="' . route('catTipoComisions.delete', [$tipo->id]) . '" onclick="return confirm(\'¿Deseas eliminar: Tipo Comisión?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$actions .= '</div>';
				return $actions;
			})
			->make(true);
		return $tipoComisionDT;
	}
}
