<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Config;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $updates = DB::table('activity_log')
            ->join('users', 'activity_log.user_id', '=', 'users.id')
            ->select('activity_log.*', 'users.name', 'users.lastname')
            ->where('activity_log.text', 'like', '%' . Config::get('constants.CREATE') . '%')
            ->orWhere('activity_log.text', 'like', '%' . Config::get('constants.UPDATE') . '%')
            ->orWhere('activity_log.text', 'like', '%' . Config::get('constants.DELETE') . '%')
            ->orderBy('activity_log.updated_at', 'desc')->take(5)->get();
        $sessions = DB::table('activity_log')
            ->join('users', 'activity_log.user_id', '=', 'users.id')
            ->select('activity_log.*', 'users.name', 'users.lastname')
            ->where('activity_log.text', 'like', '%' . Config::get('constants.AUTH') . '%')
            ->orderBy('activity_log.updated_at', 'desc')->take(5)->get();
        $notes = DB::table('notes')
            ->join('users', 'notes.idUser', '=', 'users.id')
            ->select('notes.*', 'users.name', 'users.lastname')
            ->orderBy('notes.created_at', 'desc')->take(10)->get();
        $trackings = DB::table('person')
            ->select('person.id', 'person.nombre', 'person.apellidos', 'person.updated_at')
            ->where('person.seguimiento', '=', 1)
            ->orderBy('person.updated_at', 'desc')->take(10)->get();
        return view('home', array('updates' => $updates, 'sessions' => $sessions, 'notes' => $notes, 'trackings' => $trackings));
    }

    public function calendarNotes() {
        $events = DB::table('notes')->select('id', 'prioridad', DB::raw('titulo as title'), DB::raw('created_at as start'),
            DB::raw('CONCAT("' . url('notes') . '/", id) as url'),
            DB::raw('CASE prioridad when 0 then "#d2d6de" when 1 then "#f0ad4e" else "#d9534f" END as color'))
            ->get();
        return response()->json($events);
    }
}
