<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Input;
use Spatie\Activitylog\LogsActivityInterface;
use Spatie\Activitylog\LogsActivity;
use Auth;
use Activity;
use \Illuminate\Http\Request;
use Flash;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'lastname' => 'required|min:4',
            'avatar' => 'min:10|max:1024',
            'phone' => 'min:10',
            'role_id' => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $data['avatar']=isset($data['avatar'])?$data['avatar']:'';
         $user= User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'lastname' => $data['lastname'],
            'phone' => $data['phone'],
            'avatar' => $data['avatar'],
            'password' => bcrypt($data['password']),
            'role_id' => $data['role_id']
        ]);
         if (array_key_exists('avatar', $data) && $data['avatar']!=='') {
            $destinationPath = 'uploads'; // upload path
            Input::file('avatar')->move($destinationPath, "user_".$user->id . ".png"); // uploading file to given path
        }
        return $user;
    }
    
    protected function getLogout()
    {
        Auth::logout();
        return redirect('/login');
    }
    

    protected function postLogin(Request $request){
          $this->validate($request, [
                'email' => 'required', 'password' => 'required',
            ]);
            $credentials = $request->only('email', 'password');
            if (Auth::attempt($credentials, $request->has('remember')))
            {
                Activity::log('{"event_type": "auth","category":"session", "value": "session"}');
                return redirect()->intended($this->redirectPath());
            }
            return redirect("/login");
        }

        protected function getLogin(Request $request){
         if (Auth::check())
            {
                return redirect("/home");
            }
            else {
                return view('auth.login');
            }
        }


         public function postRegister(Request $request)
                {
                    $validator = $this->validator($request->all());

                    if ($validator->fails())
                    {
                        $this->throwValidationException(
                            $request, $validator
                        );
                    }
                    $this->create($request->all());
                    Flash::success('Usuario de sistema creado correctamente.');
                    return redirect(route("auth.register"));
                }
}

