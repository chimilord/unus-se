<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCatStatusRequest;
use App\Http\Requests\UpdateCatStatusRequest;
use App\Libraries\Repositories\CatStatusRepository;
use App\Models\CatStatus;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;

class CatStatusController extends AppBaseController
{

	/** @var  CatStatusRepository */
	private $catStatusRepository;

	function __construct(CatStatusRepository $catStatusRepo)
	{
        $this->middleware('rolauth');		
		$this->catStatusRepository = $catStatusRepo;
	}

	/**
	 * Display a listing of the CatStatus.
	 *
	 * @return Response
	 */
	public function index()
	{
		$catStatus = $this->catStatusRepository->paginate(10);

		return view('catStatus.index')
			->with('catStatuses', $catStatus);
	}

	/**
	 * Show the form for creating a new CatStatus.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('catStatus.create');
	}

	/**
	 * Store a newly created CatStatus in storage.
	 *
	 * @param CreateCatStatusRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCatStatusRequest $request)
	{
		$input = $request->all();

		$catStatus = $this->catStatusRepository->create($input);

		Flash::success('Estatus entidad creado correctamente.');

		return redirect(route('catStatus.index'));
	}

	/**
	 * Display the specified CatStatus.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$catStatus = $this->catStatusRepository->find($id);

		if(empty($catStatus))
		{
			Flash::error('Estatus entidad no encontrado.');

			return redirect(route('catStatus.index'));
		}

		return view('catStatus.show')->with('catStatus', $catStatus);
	}

	/**
	 * Show the form for editing the specified CatStatus.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$catStatus = $this->catStatusRepository->find($id);

		if(empty($catStatus))
		{
			Flash::error('Estatus entidad no encontrado.');

			return redirect(route('catStatus.index'));
		}

		return view('catStatus.edit')->with('catStatus', $catStatus);
	}

	/**
	 * Update the specified CatStatus in storage.
	 *
	 * @param  int              $id
	 * @param UpdateCatStatusRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateCatStatusRequest $request)
	{
		$catStatus = $this->catStatusRepository->find($id);

		if(empty($catStatus))
		{
			Flash::error('Estatus entidad no encontrado.');

			return redirect(route('catStatus.index'));
		}

		$this->catStatusRepository->updateRich($request->all(), $id);

		Flash::success('Estatus entidad actualizado correctamente.');

		return redirect(route('catStatus.index'));
	}

	/**
	 * Remove the specified CatStatus from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$catStatus = $this->catStatusRepository->find($id);

		if(empty($catStatus))
		{
			Flash::error('Estatus entidad no encontrado.');

			return redirect(route('catStatus.index'));
		}

		$this->catStatusRepository->delete($id);

		Flash::success('Estatus entidad eliminado correctamente.');

		return redirect(route('catStatus.index'));
	}

	/**
	 * @return mixed
	 */
	public function data() {
		$estatus = CatStatus::select('*');
		$estatusDT = Datatables::of($estatus)
			->addColumn('action', function($estatus) {
				$actions = '<div class="text-center button-action">';
				$actions .= '<a href="' . route('catStatus.edit', [$estatus->id]) . '"><i class="fa fa-edit"></i></a>';
				$actions .= '<a href="' . route('catStatus.delete', [$estatus->id]) . '" onclick="return confirm(\'¿Confirmar la eliminación de estatus?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$actions .= '</div>';
				return $actions;
			})
			->make(true);
		return $estatusDT;
	}
}
