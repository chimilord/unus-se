<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePositionRequest;
use App\Http\Requests\UpdatePositionRequest;
use App\Libraries\Repositories\PositionRepository;
use App\Models\Position;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;

class PositionController extends AppBaseController
{

	/** @var  PositionRepository */
	private $positionRepository;

	function __construct(PositionRepository $positionRepo)
	{
		$this->middleware('rolauth');
		$this->positionRepository = $positionRepo;
	}

	/**
	 * Display a listing of the Position.
	 *
	 * @return Response
	 */
	public function index()
	{
		$positions = $this->positionRepository->paginate(10);

		return view('positions.index')
			->with('positions', $positions);
	}

	/**
	 * Show the form for creating a new Position.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('positions.create');
	}

	/**
	 * Store a newly created Position in storage.
	 *
	 * @param CreatePositionRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePositionRequest $request)
	{
		$input = $request->all();

		$position = $this->positionRepository->create($input);

		Flash::success('Cargo creado correctamente.');

		return redirect(route('positions.index'));
	}

	/**
	 * Display the specified Position.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$position = $this->positionRepository->find($id);

		if(empty($position))
		{
			Flash::error('Cargo no encontrado.');

			return redirect(route('positions.index'));
		}

		return view('positions.show')->with('position', $position);
	}

	/**
	 * Show the form for editing the specified Position.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$position = $this->positionRepository->find($id);

		if(empty($position))
		{
			Flash::error('Cargo no encontrado.');

			return redirect(route('positions.index'));
		}

		return view('positions.edit')->with('position', $position);
	}

	/**
	 * Update the specified Position in storage.
	 *
	 * @param  int              $id
	 * @param UpdatePositionRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatePositionRequest $request)
	{
		$position = $this->positionRepository->find($id);

		if(empty($position))
		{
			Flash::error('Cargo no encontrado.');

			return redirect(route('positions.index'));
		}

		$this->positionRepository->updateRich($request->all(), $id);

		Flash::success('Cargo actualizado correctamente.');

		return redirect(route('positions.index'));
	}

	/**
	 * Remove the specified Position from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$position = $this->positionRepository->find($id);

		if(empty($position))
		{
			Flash::error('Cargo no encontrado.');

			return redirect(route('positions.index'));
		}

		$this->positionRepository->delete($id);

		Flash::success('Cargo eliminado correctamente.');

		return redirect(route('positions.index'));
	}

	/**
	 * @return mixed
	 */
	public function data() {
		$cargos = Position::select('*');
		$cargosDT = Datatables::of($cargos)
			->addColumn('action', function($cargo) {
				$actions = '<div class="text-center button-action">';
				$actions .= '<a href="' . route('positions.edit', [$cargo->id]) . '"><i class="fa fa-edit"></i></a>';
				$actions .= '<a href="' . route('positions.delete', [$cargo->id]) . '" onclick="return confirm(\'¿Confirmar la eliminación del Cargo?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$actions .= '</div>';
				return $actions;
			})
			->make(true);
		return $cargosDT;
	}
}
