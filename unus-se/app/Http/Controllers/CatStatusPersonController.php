<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCatStatusPersonRequest;
use App\Http\Requests\UpdateCatStatusPersonRequest;
use App\Libraries\Repositories\CatStatusPersonRepository;
use App\Models\CatStatusPerson;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;

class CatStatusPersonController extends AppBaseController
{

	/** @var  CatStatusPersonRepository */
	private $catStatusPersonRepository;

	function __construct(CatStatusPersonRepository $catStatusPersonRepo)
	{
        $this->middleware('rolauth');		
		$this->catStatusPersonRepository = $catStatusPersonRepo;
	}

	/**
	 * Display a listing of the CatStatusPerson.
	 *
	 * @return Response
	 */
	public function index()
	{
		$catStatusPeople = $this->catStatusPersonRepository->paginate(10);

		return view('catStatusPeople.index')
			->with('catStatusPeople', $catStatusPeople);
	}

	/**
	 * Show the form for creating a new CatStatusPerson.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('catStatusPeople.create');
	}

	/**
	 * Store a newly created CatStatusPerson in storage.
	 *
	 * @param CreateCatStatusPersonRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCatStatusPersonRequest $request)
	{
		$input = $request->all();

		$catStatusPerson = $this->catStatusPersonRepository->create($input);

		Flash::success('Estatus persona creado correctamente.');

		return redirect(route('catStatusPeople.index'));
	}

	/**
	 * Display the specified CatStatusPerson.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$catStatusPerson = $this->catStatusPersonRepository->find($id);

		if(empty($catStatusPerson))
		{
			Flash::error('Estatus persona no encontrado.');

			return redirect(route('catStatusPeople.index'));
		}

		return view('catStatusPeople.show')->with('catStatusPerson', $catStatusPerson);
	}

	/**
	 * Show the form for editing the specified CatStatusPerson.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$catStatusPerson = $this->catStatusPersonRepository->find($id);

		if(empty($catStatusPerson))
		{
			Flash::error('Estatus persona no encontrado.');

			return redirect(route('catStatusPeople.index'));
		}

		return view('catStatusPeople.edit')->with('catStatusPerson', $catStatusPerson);
	}

	/**
	 * Update the specified CatStatusPerson in storage.
	 *
	 * @param  int              $id
	 * @param UpdateCatStatusPersonRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateCatStatusPersonRequest $request)
	{
		$catStatusPerson = $this->catStatusPersonRepository->find($id);

		if(empty($catStatusPerson))
		{
			Flash::error('Estatus persona no encontrado.');

			return redirect(route('catStatusPeople.index'));
		}

		$this->catStatusPersonRepository->updateRich($request->all(), $id);

		Flash::success('Estatus persona actualizado correctamente.');

		return redirect(route('catStatusPeople.index'));
	}

	/**
	 * Remove the specified CatStatusPerson from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$catStatusPerson = $this->catStatusPersonRepository->find($id);

		if(empty($catStatusPerson))
		{
			Flash::error('Estatus persona no encontrado.');

			return redirect(route('catStatusPeople.index'));
		}

		$this->catStatusPersonRepository->delete($id);

		Flash::success('Estatus persona eliminado correctamente.');

		return redirect(route('catStatusPeople.index'));
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function data()
	{
		$estatus = CatStatusPerson::select('*');
		$estatusDT = Datatables::of($estatus)
			->addColumn('action', function ($state) {
				$actions = '<div class="text-center button-action">';
				$actions .= '<a href="' . route('catStatusPeople.edit', [$state->id]) . '"><i class="fa fa-edit"></i></a>';
				$actions .= '<a href="' . route('catStatusPeople.delete', [$state->id]) . '" onclick="return confirm(\'¿Confirmar la eliminación de Status Persona?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$actions .= '</div>';
				return $actions;
			})
			->make(true);
		return $estatusDT;

	}
}
