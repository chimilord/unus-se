<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePersonRequest;
use App\Http\Requests\UpdatePersonRequest;
use App\Libraries\Repositories\PersonRepository;
use App\Models\Person;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\catDistritos;
use App\Models\CatStatusPerson;
use App\Models\CatTipoEleccion;
use App\Models\Estado;
use App\Models\Partido;
use App\Models\catTipoPersona;
use Illuminate\Support\Facades\Input;
use DB;
use Request;

use Yajra\Datatables\Datatables;

class PersonController extends AppBaseController
{

    /** @var  PersonRepository */
    private $personRepository;

    function __construct(PersonRepository $personRepo)
    {
        $this->middleware('rolauth');
        $this->personRepository = $personRepo;
    }

    /**
     * Display a listing of the Person.
     *
     * @return Response
     */
    public function index()
    {
        $persons = $this->personRepository->all();
        return view('person.index')->with('persons', $persons);
    }

    /**
     * Show the form for creating a new Person.
     *
     * @return Response
     */
    public function create()
    {
        $idDistrito = catDistritos::lists('nombre', 'id');
        $idStatusPerson = CatStatusPerson::lists('estatus', 'id');
        $idTipoEleccion = CatTipoEleccion::lists('nombre', 'id');
        $idEstado = Estado::lists('nombre', 'id');
        $idPartido = Partido::lists('partido', 'id');
        $idTipoPersona = CatTipoPersona::lists('nombre', 'id');
        return view('person.create', array('idEstado' => $idEstado, 'idDistrito' => $idDistrito, 'idStatusPerson' => $idStatusPerson, 'idTipoEleccion' => $idTipoEleccion, 'idPartido' => $idPartido, 'idTipoPersona' => $idTipoPersona));
    }

    /**
     * Store a newly created Person in storage.
     *
     * @param CreatePersonRequest $request
     *
     * @return Response
     */
    public function store(CreatePersonRequest $request)
    {
        $input = $request->all();
        $input['inicioPeriodo'] = $this->formatDate($input['inicioPeriodo']);
        $input['finPeriodo'] = $this->formatDate($input['finPeriodo']);
        $person = $this->personRepository->create($input);

        if (isset($input['fotografia'])) {
            $destinationPath = 'uploads'; // upload path
            Input::file('fotografia')->move($destinationPath, $person->id . ".png"); // uploading file to given path
        }
        Flash::success('Persona creada correctamente.');

        return redirect(route('person.index'));
    }

    /**
     * Display the specified Person.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $person = $this->personRepository->find($id);

        if (empty($person)) {
            Flash::error('Persona no encontrada.');

            return redirect(route('person.index'));
        } else {
            $estado_name = DB::table('estados')->where('id', $person->idEstado)->get();
            $status_name = DB::table('catStatusPeople')->where('id', $person->idStatusPerson)->get();
            $partido_name = DB::table('partidos')->where('id', $person->idPartido)->get();
            $distrito_name = DB::table('catDistritos')->where('id', $person->idDistrito)->get();
            $eleccion_name = DB::table('catTipoEleccions')->where('id', $person->idTipoEleccion)->get();
            $tipo_name = DB::table('catTipoPersonas')->where('id', $person->idTipoPersona)->get();
            $comisiones = DB::table('person_commission')
                ->join('comisions', 'person_commission.comision_id', '=', 'comisions.id')
                ->join('catComisiones', 'comisions.idCatComision', '=', 'catComisiones.id')
                ->join('catTipoComision', 'catComisiones.idTipoComision', '=', 'catTipoComision.id')
                ->join('positions', 'person_commission.position_id', '=', 'positions.id')
                ->select('comisions.id', DB::raw('positions.nombre as cargo'), DB::raw('catComisiones.nombre as nombre'), DB::raw('catTipoComision.nombre as tipoComision'))
                ->where('person_commission.person_id', '=', $person->id)
                ->get();
            $iniciativas = DB::table('person_iniciativa')
                ->join('initiatives', 'person_iniciativa.initiatives_id', '=', 'initiatives.id')
                ->join('positions', 'person_iniciativa.position_id', '=', 'positions.id')
                ->join('catStatuses', 'initiatives.status_id', '=', 'catStatuses.id')
                ->select('initiatives.title', 'initiatives.id', DB::raw('positions.nombre as cargo'), DB::raw('catStatuses.name as estatus'))
                ->where('person_iniciativa.person_id', '=', $person->id)
                ->get();
        }

        return view('person.show', array(
            'person' => $person,
            'estado' => $estado_name[0]->nombre,
            'distrito' => isset($distrito_name[0]->nombre) ? $distrito_name[0]->nombre : '0',
            'status' => $status_name[0]->estatus,
            'partido' => $partido_name[0]->siglas,
            'eleccion' => $eleccion_name[0]->nombre,
            'tipo' => $tipo_name[0]->nombre,
            'comisiones' => $comisiones,
            'iniciativas' => $iniciativas
        ));
    }

    /**
     * Show the form for editing the specified Person.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $person = $this->personRepository->find($id);

        if (empty($person)) {
            Flash::error('Persona no encontrada.');

            return redirect(route('person.index'));
        }

        $person['inicioPeriodo'] = $this->showDate($person['inicioPeriodo']);
        $person['finPeriodo'] = $this->showDate($person['finPeriodo']);
        //	return view('person.edit')->with('person', $person);
        $idDistrito = catDistritos::lists('nombre', 'id');
        $idStatusPerson = CatStatusPerson::lists('estatus', 'id');
        $idTipoEleccion = CatTipoEleccion::lists('nombre', 'id');
        $idEstado = Estado::lists('nombre', 'id');
        $idPartido = Partido::lists('partido', 'id');
        $idTipoPersona = CatTipoPersona::lists('nombre', 'id');
        return view('person.edit', array('idEstado' => $idEstado, 'idDistrito' => $idDistrito, 'idStatusPerson' => $idStatusPerson, 'idTipoEleccion' => $idTipoEleccion, 'idPartido' => $idPartido, 'idTipoPersona' => $idTipoPersona))->with('person', $person);
    }

    /**
     * Update the specified Person in storage.
     *
     * @param  int $id
     * @param UpdatePersonRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePersonRequest $request)
    {
        $input = $request->all();
        $input['inicioPeriodo'] = $this->formatDate($input['inicioPeriodo']);
        $input['finPeriodo'] = $this->formatDate($input['finPeriodo']);
        $person = $this->personRepository->find($id);

        if (empty($person)) {
            Flash::error('Persona no encontrada.');

            return redirect(route('person.index'));
        }

        $this->personRepository->updateRich($input, $id);

        Flash::success('Persona actualizada correctamente.');

        return redirect(route('person.index'));
    }

    /**
     * Remove the specified Person from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $person = $this->personRepository->find($id);

        if (empty($person)) {
            Flash::error('Persona no encontrada.');

            return redirect(route('person.index'));
        }

        $this->personRepository->delete($id);

        Flash::success('Persona eliminada correctamente.');

        return redirect(route('person.index'));
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function data()
    {
        $persons = Person::select('*');
        $personsDT = Datatables::of($persons)
            ->editColumn('nombre', '{!! $nombre !!}' . ' ' . '{!! $apellidos !!}')
            ->addColumn('action', function ($person) {
                $buttons = '<div class="text-center button-action">';
                $buttons .= '<a href="' . route('person.edit', [$person->id]) . '"><i class="fa fa-edit"></i></a>';
                $buttons .= '<a href="' . route('person.delete', [$person->id]) . '" onclick="return confirm(\'¿Deseas eliminar: Persona?\')"><i class="fa fa-trash-o icon-red"></i></a>';
                $buttons .= '</div>';
                return $buttons;
            })
            ->make(true);
        return $personsDT;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function updateTracking($id)
    {
        if (Request::ajax()) {
            if (Request::isMethod('post')) {
                $seguimiento = Request::has('seguimiento') ? 1 : 0;
                $update = DB::table('person')->where('id', $id)->update(['seguimiento' => $seguimiento, 'updated_at' => DB::raw('NOW()')]);
            }
        }

        return $seguimiento;
    }

    /**
     * @param $date
     * @return string
     */
    public function formatDate($date)
    {
        if (stripos($date, "/")) {
            $format = explode("/", $date);
            // Special format date because daterangepicker format is MM/DD/YYYY
            return $format[2] . '-' . $format[1] . '-' . $format[0];
        }
        return $date;
    }

    /**
     * @param $date
     * @return string
     */
    public function showDate($date)
    {
        if (stripos($date, "-")) {
            $format = explode("-", $date);
            // Special format date because daterangepicker format is MM/DD/YYYY
            return $format[2] . '/' . $format[1] . '/' . $format[0];
        }
        return $date;
    }

}
