<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatecatTipoEleccionRequest;
use App\Http\Requests\UpdatecatTipoEleccionRequest;
use App\Libraries\Repositories\catTipoEleccionRepository;
use App\Models\CatTipoEleccion;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;

class catTipoEleccionController extends AppBaseController
{

	/** @var  catTipoEleccionRepository */
	private $catTipoEleccionRepository;

	function __construct(catTipoEleccionRepository $catTipoEleccionRepo)
	{
        $this->middleware('rolauth');		
		$this->catTipoEleccionRepository = $catTipoEleccionRepo;
	}

	/**
	 * Display a listing of the catTipoEleccion.
	 *
	 * @return Response
	 */
	public function index()
	{
		$catTipoEleccions = $this->catTipoEleccionRepository->paginate(10);

		return view('catTipoEleccions.index')
			->with('catTipoEleccions', $catTipoEleccions);
	}

	/**
	 * Show the form for creating a new catTipoEleccion.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('catTipoEleccions.create');
	}

	/**
	 * Store a newly created catTipoEleccion in storage.
	 *
	 * @param CreatecatTipoEleccionRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatecatTipoEleccionRequest $request)
	{
		$input = $request->all();

		$catTipoEleccion = $this->catTipoEleccionRepository->create($input);

		Flash::success('Tipo Elección creado correctamente.');

		return redirect(route('catTipoEleccions.index'));
	}

	/**
	 * Display the specified catTipoEleccion.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$catTipoEleccion = $this->catTipoEleccionRepository->find($id);

		if(empty($catTipoEleccion))
		{
			Flash::error('Tipo Elección no encontrado.');

			return redirect(route('catTipoEleccions.index'));
		}

		return view('catTipoEleccions.show')->with('catTipoEleccion', $catTipoEleccion);
	}

	/**
	 * Show the form for editing the specified catTipoEleccion.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$catTipoEleccion = $this->catTipoEleccionRepository->find($id);

		if(empty($catTipoEleccion))
		{
			Flash::error('Tipo Elección no encontrado.');

			return redirect(route('catTipoEleccions.index'));
		}

		return view('catTipoEleccions.edit')->with('catTipoEleccion', $catTipoEleccion);
	}

	/**
	 * Update the specified catTipoEleccion in storage.
	 *
	 * @param  int              $id
	 * @param UpdatecatTipoEleccionRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatecatTipoEleccionRequest $request)
	{
		$catTipoEleccion = $this->catTipoEleccionRepository->find($id);

		if(empty($catTipoEleccion))
		{
			Flash::error('Tipo Elección no encontrado.');

			return redirect(route('catTipoEleccions.index'));
		}

		$this->catTipoEleccionRepository->updateRich($request->all(), $id);

		Flash::success('Tipo Elección actualizado correctamente.');

		return redirect(route('catTipoEleccions.index'));
	}

	/**
	 * Remove the specified catTipoEleccion from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$catTipoEleccion = $this->catTipoEleccionRepository->find($id);

		if(empty($catTipoEleccion))
		{
			Flash::error('Tipo Elección no encontrado.');

			return redirect(route('catTipoEleccions.index'));
		}

		$this->catTipoEleccionRepository->delete($id);

		Flash::success('Tipo Elección eliminado correctamente.');

		return redirect(route('catTipoEleccions.index'));
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function data() {
		$tipos = CatTipoEleccion::select('*');
		$tiposDT = Datatables::of($tipos)
			->addColumn('action', function ($tipo) {
				$actions = '<div class="text-center button-action">';
				$actions .= '<a href="' . route('catTipoEleccions.edit', [$tipo->id]) . '"><i class="fa fa-edit"></i></a>';
				$actions .= '<a href="' . route('catTipoEleccions.delete', [$tipo->id]) . '" onclick="return confirm(\'¿Deseas eliminar: Tipo Elección?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$actions .= '</div>';
				return $actions;
			})
			->make(true);
		return $tiposDT;
	}
}
