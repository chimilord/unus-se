<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateOrganoGobiernoRequest;
use App\Http\Requests\UpdateOrganoGobiernoRequest;
use App\Libraries\Repositories\OrganoGobiernoRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;
use DB;
use Illuminate\Database\Eloquent\Model;

class OrganoGobiernoController extends AppBaseController
{

	/** @var  OrganoGobiernoRepository */
	private $organoGobiernoRepository;

	function __construct(OrganoGobiernoRepository $organoGobiernoRepo)
	{
        $this->middleware('rolauth');		
		$this->organoGobiernoRepository = $organoGobiernoRepo;
	}

	/**
	 * Display a listing of the OrganoGobierno.
	 *
	 * @return Response
	 */
	public function index()
	{
		$organoGobiernos = $this->organoGobiernoRepository->paginate(10);

		return view('organoGobiernos.index')
			->with('organoGobiernos', $organoGobiernos);
	}

	/**
	 * Show the form for creating a new OrganoGobierno.
	 *
	 * @return Response
	 */
	public function create()
	{
		$positions = \DB::table('positions')->lists('nombre', 'id');
		$persons = \DB::table('person')->selectRaw('CONCAT(nombre,\' \',apellidos) as fulln,id' )->lists('fulln', 'id');
		$idCatOrganoGobierno = \DB::table('catOrganoGobiernos')->lists('nombre', 'id');
		$idPeriodo = \DB::table('catPeriodos')->lists('nombre', 'id');
		return view('organoGobiernos.create', array('persons'=>$persons, 'positions' => $positions,  'idCatOrganoGobierno' => $idCatOrganoGobierno, 'idPeriodo'=>$idPeriodo));
	}

	/**
	 * Store a newly created OrganoGobierno in storage.
	 *
	 * @param CreateOrganoGobiernoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateOrganoGobiernoRequest $request)
	{
		$input = $request->all();
		$persons = $input['persons'];
		$positions = $input['positions'];
		$i=0;
		foreach($persons as $key => $value) {
            $persons[$value] =  ["position_id"=>$positions[$i++]];
        }
		$organoGobierno = $this->organoGobiernoRepository->create($input);
		$organoGobierno->persons()->sync($persons);
		Flash::success('órgano de Gobierno creado correctamente.');
		return redirect(route('organoGobiernos.index'));
	}

	/**
	 * Display the specified OrganoGobierno.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$organoGobierno = $this->organoGobiernoRepository->find($id);
		$positions = \DB::table('positions')->lists('nombre', 'id');
		$persons =$organoGobierno->persons()->get();
		if(empty($organoGobierno))
		{
			Flash::error('Órgano de Gobierno no encontrado.');
			return redirect(route('organoGobiernos.index'));
		}
		else {
			$catOrgano = DB::table('catOrganoGobiernos')
				->select('catOrganoGobiernos.*')
				->where('catOrganoGobiernos.id', '=', $organoGobierno->idCatOrganoGobierno)->get();
			$periodo = DB::table('catPeriodos')->where('id', $organoGobierno->idPeriodo)->get();
		}
		return view('organoGobiernos.show',array('persons'=>$persons, 'positions' => $positions,  'catOrgano'=>$catOrgano[0], 'periodo'=>$periodo[0]->nombre))->with('organoGobierno', $organoGobierno);
	}

	/**
	 * Show the form for editing the specified OrganoGobierno.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$organoGobierno = $this->organoGobiernoRepository->find($id);
		$positions = \DB::table('positions')->lists('nombre', 'id');
		$persons = \DB::table('person')->selectRaw('CONCAT(nombre,\' \',apellidos) as fulln,id' )->lists('fulln', 'id');
		$idCatOrganoGobierno = \DB::table('catOrganoGobiernos')->lists('nombre', 'id');
		$selectedPersons =$organoGobierno->persons()->get();
		$idPeriodo = \DB::table('catPeriodos')->lists('nombre', 'id');

		if(empty($organoGobierno))
		{
			Flash::error('Órgano de Gobierno no encontrado.');

			return redirect(route('organoGobiernos.index'));
		}
		else{
			$positions = \DB::table('positions')->lists('nombre', 'id');
			$persons = \DB::table('person')->selectRaw('CONCAT(nombre,\' \',apellidos) as fulln,id' )->lists('fulln', 'id');
			$selectedPersons = DB::table('person_organo')
				->join('person', 'person_organo.person_id', '=', 'person.id')
				->join('organoGobiernos', 'person_organo.organo_id', '=', 'organoGobiernos.id')
				->join('positions', 'person_organo.position_id', '=', 'positions.id')
				->select('person.id', 'person.nombre', 'person.apellidos', DB::raw('positions.nombre as cargo, positions.id as cargo_id'))
				->where('person_organo.organo_id', '=', $organoGobierno->id)
				->get();

		}
		return view('organoGobiernos.edit', array('selectedPersons'=>$selectedPersons,'persons'=>$persons, 'positions' => $positions,'idCatOrganoGobierno' => $idCatOrganoGobierno,'idPeriodo' => $idPeriodo))->with('organoGobierno', $organoGobierno);
	}

	/**
	 * Update the specified OrganoGobierno in storage.
	 *
	 * @param  int              $id
	 * @param UpdateOrganoGobiernoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateOrganoGobiernoRequest $request)
	{
		$input = $request->all();
		$organoGobierno = $this->organoGobiernoRepository->find($id);
		$persons = $input['persons'];
		$positions = $input['positions'];
		$i=0;
		foreach($persons as $key => $value) {
            $persons[$value] =  ["position_id"=>$positions[$i++]];
        }

		if(empty($organoGobierno))
		{
			Flash::error('Órgano de Gobierno no encontrado.');

			return redirect(route('organoGobiernos.index'));
		}
		$organoGobierno->persons()->sync($persons);
		$this->organoGobiernoRepository->updateRich($request->all(), $id);
		Flash::success('Órgano de Gobierno actualizado correctamente.');
		return redirect(route('organoGobiernos.index'));
	}

	/**
	 * Remove the specified OrganoGobierno from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$organoGobierno = $this->organoGobiernoRepository->find($id);

		if(empty($organoGobierno))
		{
			Flash::error('Órgano de Gobierno no encontrado.');

			return redirect(route('organoGobiernos.index'));
		}

		$this->organoGobiernoRepository->delete($id);

		Flash::success('Órgano de Gobierno eliminado correctamente.');

		return redirect(route('organoGobiernos.index'));
	}


	/**
	 * @return mixed
	 */
	public function data() {
		$organo = DB::table('organoGobiernos')
			->join('catOrganoGobiernos', 'organoGobiernos.idCatOrganoGobierno', '=', 'catOrganoGobiernos.id')
			->join('catPeriodos', 'organoGobiernos.idPeriodo', '=', 'catPeriodos.id')
			->select(['organoGobiernos.id',  'catOrganoGobiernos.nombre', DB::raw('catPeriodos.nombre as periodo')]);
		$organosDT = Datatables::of($organo)
			->addColumn('action', function ($organo) {
				$buttons = '<div class="text-center button-action">';
				$buttons .= '<a href="' . route('organoGobiernos.edit', [$organo->id]) . '"><i class="fa fa-edit"></i></a>';
				$buttons .= '<a href="' . route('organoGobiernos.delete', [$organo->id]) . '" onclick="return confirm(\'¿Deseas eliminar: Órgano de Gobierno?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$buttons .= '</div>';
				return $buttons;
			})
			->make(true);
		return $organosDT;
	}
}
