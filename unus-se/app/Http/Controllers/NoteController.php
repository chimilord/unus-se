<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateNoteRequest;
use App\Http\Requests\UpdateNoteRequest;
use App\Libraries\Repositories\NoteRepository;
use App\Models\Note;
use App\Models\Person;
use App\User;
use Flash;
use Illuminate\Support\Facades\Config;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;
use DB;
use Mail;

class NoteController extends AppBaseController
{

	/** @var  NoteRepository */
	private $noteRepository;

	function __construct(NoteRepository $noteRepo)
	{
        $this->middleware('rolauth');		
		$this->noteRepository = $noteRepo;
	}

	/**
	 * Display a listing of the Note.
	 *
	 * @return Response
	 */
	public function index()
	{
		$notes = $this->noteRepository->all();
		return view('notes.index')->with('notes', $notes);
	}

	/**
	 * Show the form for creating a new Note.
	 *
	 * @return Response
	 */
	public function create()
	{
		$person_id = Person::get()->lists('full_name', 'id');
		$user_id = User::get()->lists('full_name', 'id');
		return view('notes.create', array('person_id' => $person_id, 'user_id' => $user_id));
	}

	/**
	 * Store a newly created Note in storage.
	 *
	 * @param CreateNoteRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateNoteRequest $request)
	{
		$input = $request->all();

		$note = $this->noteRepository->create($input);

		if (!empty($note)) {
			$this->sendEmailNotification($note);
		}

		Flash::success('Nota creada correctamente.');

		return redirect(route('notes.index'));
	}

	/**
	 * Display the specified Note.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$note = $this->noteRepository->find($id);

		if(empty($note))
		{
			Flash::error('Nota no encontrada.');

			return redirect(route('notes.index'));
		} else {
			$user = DB::table('users')->where('id', $note->idUser)->get();
			$assign = DB::table('users')->where('id', $note->idSeguimiento)->get();
			$person = DB::table('person')->where('id', $note->idPersona)->get();
		}

        return view('notes.show', array(
            'note' => $note,
			'user_name' => $user[0]->name . ' ' . $user[0]->lastname,
            'person_name' => $person[0]->nombre . ' ' . $person[0]->apellidos,
			'seguimiento' => $assign[0]->name . ' ' . $assign[0]->lastname
        ));
	}

	/**
	 * Show the form for editing the specified Note.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$person_id = Person::get()->lists('full_name', 'id');
		$user_id = User::get()->lists('full_name', 'id');

		$note = $this->noteRepository->find($id);

		if(empty($note))
		{
			Flash::error('Nota no encontrada.');

			return redirect(route('notes.index'));
		}

		return view('notes.edit', array('note' => $note, 'person_id' => $person_id, 'user_id' => $user_id));
	}

	/**
	 * Update the specified Note in storage.
	 *
	 * @param  int              $id
	 * @param UpdateNoteRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateNoteRequest $request)
	{
		$note = $this->noteRepository->find($id);

		if(empty($note))
		{
			Flash::error('Nota no encontrada.');

			return redirect(route('notes.index'));
		}

		$this->noteRepository->updateRich($request->all(), $id);

		Flash::success('Nota actualizada correctamente.');

		return redirect(route('notes.index'));
	}

	/**
	 * Remove the specified Note from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$note = $this->noteRepository->find($id);

		if(empty($note))
		{
			Flash::error('Nota no encontrada.');

			return redirect(route('notes.index'));
		}

		$this->noteRepository->delete($id);

		Flash::success('Nota eliminada correctamente.');

		return redirect(route('notes.index'));
	}

	/**
	 * @return mixed
	 */
	public function data() {
		$notes = DB::table('notes')->join('users', 'notes.idUser', '=', 'users.id')
			->select(['notes.id', 'notes.titulo', 'notes.descripcion', 'notes.created_at', 'users.name', 'users.lastname']);
		$notesDT = Datatables::of($notes)
			->editColumn('name', '{!! $name !!}' . ' ' . '{!! $lastname !!}')
			->editColumn('descripcion', '{!! str_limit($descripcion, 150) !!}')
			->editColumn('created_at', '{!! date(\'d/m/Y H:i:s\', strtotime($created_at)) !!}')
			->addColumn('action', function($note) {
				$actions = '<div class="text-center button-action">';
				$actions .= '<a href="' . route('notes.edit', [$note->id]) . '"><i class="fa fa-edit"></i></a>';
				$actions .= '<a href="' . route('notes.delete', [$note->id]) . '" onclick="return confirm(\'¿Deseas eliminar: Nota?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$actions .= '</div>';
				return $actions;
			})
			->make(true);
		return $notesDT;
	}

	/**
	 * Send email notification from system user.
	 *
	 * @param $note Last note inserted
	 */
	public function sendEmailNotification($note)
	{
		$user = User::find($note->idSeguimiento);
		Mail::send('emails.notification', ['user' => $user, 'note' => $note], function ($m) use ($user) {
			$m->from(Config::get('constants.email.username'), Config::get('constants.email.noreply'));
			$m->to($user->email, $user->name)->subject(Config::get('constants.email.subject'));
		});
	}

}
