<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatecatComisionesRequest;
use App\Http\Requests\UpdatecatComisionesRequest;
use App\Libraries\Repositories\catComisionesRepository;
use App\Models\catComisiones;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;
use App\Models\catTipoComision;
use DB;

class catComisionesController extends AppBaseController
{

	/** @var  catComisionesRepository */
	private $catComisionesRepository;

	function __construct(catComisionesRepository $catComisionesRepo)
	{
        $this->middleware('rolauth');		
		$this->catComisionesRepository = $catComisionesRepo;
	}

	/**
	 * Display a listing of the catComisiones.
	 *
	 * @return Response
	 */
	public function index()
	{
		$catComisiones = $this->catComisionesRepository->all();
		return view('catComisiones.index')->with('catComisiones', $catComisiones);
	}

	/**
	 * Show the form for creating a new catComisiones.
	 *
	 * @return Response
	 */
	public function create()
	{
		$idTipoComision = CatTipoComision::lists('nombre', 'id');
		return view('catComisiones.create', array('idTipoComision'=>$idTipoComision));
	}

	/**
	 * Store a newly created catComisiones in storage.
	 *
	 * @param CreatecatComisionesRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatecatComisionesRequest $request)
	{
		$input = $request->all();

		$catComisiones = $this->catComisionesRepository->create($input);

		Flash::success('Comisión creada correctamente.');

		return redirect(route('catComisiones.index'));
	}

	/**
	 * Display the specified catComisiones.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$catComisiones = $this->catComisionesRepository->find($id);
		$tipoComision = DB::table('catTipoComision')->where('id', $catComisiones->idTipoComision)->get();
		if(empty($catComisiones))
		{
			Flash::error('Comisión no encontrada.');

			return redirect(route('catComisiones.index'));
		}
		return view('catComisiones.show',array('tipoComision'=>$tipoComision[0]->nombre))->with('catComisiones', $catComisiones);
	}

	/**
	 * Show the form for editing the specified catComisiones.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$catComisiones = $this->catComisionesRepository->find($id);
		$idTipoComision = CatTipoComision::lists('nombre', 'id');
		if(empty($catComisiones))
		{
			Flash::error('Comisión no encontrada.');

			return redirect(route('catComisiones.index'));
		}

		return view('catComisiones.edit',array('idTipoComision'=>$idTipoComision))->with('catComisiones', $catComisiones);
	}

	/**
	 * Update the specified catComisiones in storage.
	 *
	 * @param  int              $id
	 * @param UpdatecatComisionesRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatecatComisionesRequest $request)
	{
		$catComisiones = $this->catComisionesRepository->find($id);

		if(empty($catComisiones))
		{
			Flash::error('Comisión no encontrada.');

			return redirect(route('catComisiones.index'));
		}

		$this->catComisionesRepository->updateRich($request->all(), $id);

		Flash::success('Comisión actualizada correctamente.');

		return redirect(route('catComisiones.index'));
	}

	/**
	 * Remove the specified catComisiones from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$catComisiones = $this->catComisionesRepository->find($id);

		if(empty($catComisiones))
		{
			Flash::error('Comisión no encontrada.');

			return redirect(route('catComisiones.index'));
		}

		$this->catComisionesRepository->delete($id);

		Flash::success('Comisión eliminada correctamente.');

		return redirect(route('catComisiones.index'));
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function data() {
		$comisiones = catComisiones::select('*');
		$comisionesDT = Datatables::of($comisiones)
			->addColumn('action', function ($comision) {
				$buttons = '<div class="text-center button-action">';
				$buttons .= '<a href="' . route('catComisiones.edit', [$comision->id]) . '"><i class="fa fa-edit"></i></a>';
				$buttons .= '<a href="' . route('catComisiones.delete', [$comision->id]) . '" onclick="return confirm(\'¿Confirmar la eliminación de la Comisiónn?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$buttons .= '</div>';
				return $buttons;
			})
			->make(true);
		return $comisionesDT;
	}
}
