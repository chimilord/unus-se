<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateComisionRequest;
use App\Http\Requests\UpdateComisionRequest;
use App\Libraries\Repositories\ComisionRepository;
use App\Models\Comision;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;
use DB;
use Illuminate\Database\Eloquent\Model;

class ComisionController extends AppBaseController
{

	/** @var  ComisionRepository */
	private $comisionRepository;

	function __construct(ComisionRepository $comisionRepo)
	{
        $this->middleware('rolauth');		
		$this->comisionRepository = $comisionRepo;
	}

	/**
	 * Display a listing of the Comision.
	 *
	 * @return Response
	 */
	public function index()
	{
		$comisions = $this->comisionRepository->paginate(10);

		return view('comisions.index')
			->with('comisions', $comisions);
	}

	/**
	 * Show the form for creating a new Comision.
	 *
	 * @return Response
	 */
	public function create()
	{
		$positions = \DB::table('positions')->lists('nombre', 'id');
		$persons = \DB::table('person')->selectRaw('CONCAT(nombre,\' \',apellidos) as fulln,id' )->lists('fulln', 'id');
		$idCatComision = \DB::table('catComisiones')->lists('nombre', 'id');
		$idPeriodo = \DB::table('catPeriodos')->lists('nombre', 'id');
		return view('comisions.create', array('persons'=>$persons, 'positions' => $positions,  'idCatComision' => $idCatComision, 'idPeriodo'=>$idPeriodo));
	}

	/**
	 * Store a newly created Comision in storage.
	 *
	 * @param CreateComisionRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateComisionRequest $request)
	{
		$input = $request->all();
		$persons = $input['persons'];
		$positions = $input['positions'];
		$i=0;
		foreach($persons as $key => $value) {
            $persons[$value] =  ["position_id"=>$positions[$i++]];
        }
		$comision = $this->comisionRepository->create($input);
		$comision->persons()->sync($persons);
		Flash::success('Comisión creada correctamente.');
		return redirect(route('comisions.index'));
	}

	/**
	 * Display the specified Comision.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$comision = $this->comisionRepository->find($id);

		if(empty($comision))
		{
			Flash::error('Comisión no encontrada.');

			return redirect(route('comisions.index'));
		}
		else {
			$estatus = [];
			$catComision = DB::table('catComisiones')
				->join('catTipoComision', 'catComisiones.idTipoComision', '=', 'catTipoComision.id')
				->select('catComisiones.*', DB::raw('catTipoComision.nombre as tipoComision'))
				->where('catComisiones.id', '=', $comision->idCatComision)->get();
			$periodo = DB::table('catPeriodos')->where('id', $comision->idPeriodo)->get();
			$iniciativas = $comision->iniciativas()->get();
			foreach ($iniciativas as $iniciativa) {
				$estatus[] = $iniciativa->status_id;
			}
			$nombresEstatus = DB::table('catStatuses')->whereIn('id', $estatus)->get();
			$persons = DB::table('person_commission')
				->join('person', 'person_commission.person_id', '=', 'person.id')
				->join('positions', 'person_commission.position_id', '=', 'positions.id')
				->join('partidos', 'person.idPartido', '=', 'partidos.id')
				->select('person.id', 'person.nombre', 'person.apellidos', 'partidos.siglas', DB::raw('positions.nombre as cargo, positions.id as cargo_id'))
				->where('person_commission.comision_id', '=', $comision->id)
				->orderBy('cargo_id', 'asc')
				->get();
		}
		return view('comisions.show', array('comision'=>$comision, 'persons'=>$persons, 'catComision'=>$catComision[0], 'periodo'=>$periodo[0]->nombre, 'iniciativas' => $iniciativas, 'estatus' => $nombresEstatus));
	}

	/**
	 * Show the form for editing the specified Comision.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$comision = $this->comisionRepository->find($id);

		if(empty($comision))
		{
			Flash::error('Comisión no encontrada.');

			return redirect(route('comisions.index'));
		}
		else{
			$idCatComision = \DB::table('catComisiones')->lists('nombre', 'id');
			$idPeriodo = \DB::table('catPeriodos')->lists('nombre', 'id');
			$positions = \DB::table('positions')->lists('nombre', 'id');
			$persons = \DB::table('person')->selectRaw('CONCAT(nombre,\' \',apellidos) as fulln,id' )->lists('fulln', 'id');
			$selectedPersons = DB::table('person_commission')
				->join('person', 'person_commission.person_id', '=', 'person.id')
				->join('comisions', 'person_commission.comision_id', '=', 'comisions.id')
				->join('positions', 'person_commission.position_id', '=', 'positions.id')
				->select('person.id', 'person.nombre', 'person.apellidos', DB::raw('positions.nombre as cargo, positions.id as cargo_id'))
				->where('person_commission.comision_id', '=', $comision->id)
				->orderBy('cargo_id', 'asc')
				->get();

		}

		return view('comisions.edit', array('selectedPersons'=>$selectedPersons,'persons'=>$persons, 'positions' => $positions,'idCatComision' => $idCatComision,'idPeriodo' => $idPeriodo))->with('comision', $comision);
	}

	/**
	 * Update the specified Comision in storage.
	 *
	 * @param  int              $id
	 * @param UpdateComisionRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateComisionRequest $request)
	{
		$input = $request->all();
		$comision = $this->comisionRepository->find($id);
		$persons = $input['persons'];
		$positions = $input['positions'];
		$i=0;
		foreach($persons as $key => $value) {
            $persons[$value] =  ["position_id"=>$positions[$i++]];
        }


		if(empty($comision))
		{
			Flash::error('Comisión no encontrada.');

			return redirect(route('comisions.index'));
		}

		$comision->persons()->sync($persons);
		$this->comisionRepository->updateRich($request->all(), $id);

		Flash::success('Comisión actualizada correctamente.');

		return redirect(route('comisions.index'));
	}

	/**
	 * Remove the specified Comision from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$comision = $this->comisionRepository->find($id);

		if(empty($comision))
		{
			Flash::error('Comisión no encontrada.');

			return redirect(route('comisions.index'));
		}

		$this->comisionRepository->delete($id);

		Flash::success('Comisión eliminada correctamente.');

		return redirect(route('comisions.index'));
	}

	/**
	 * @return mixed
	 */
	public function data() {
		$comision = DB::table('comisions')->join('catComisiones', 'comisions.idCatComision', '=', 'catComisiones.id')
			->join('catTipoComision', 'catComisiones.idTipoComision', '=', 'catTipoComision.id')
			->select(['comisions.id', 'comisions.keywords', 'catComisiones.descripcion', 'catComisiones.nombre']);
		$comisionsDT = Datatables::of($comision)
			->addColumn('action', function ($comision) {
				$buttons = '<div class="text-center button-action">';
				$buttons .= '<a href="' . route('comisions.edit', [$comision->id]) . '"><i class="fa fa-edit"></i></a>';
				$buttons .= '<a href="' . route('comisions.delete', [$comision->id]) . '" onclick="return confirm(\'¿Deseas eliminar: Comision?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$buttons .= '</div>';
				return $buttons;
			})
			->make(true);
		return $comisionsDT;
	}
}
