<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatecatTipoPersonaRequest;
use App\Http\Requests\UpdatecatTipoPersonaRequest;
use App\Libraries\Repositories\catTipoPersonaRepository;
use App\Models\catTipoPersona;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;

class catTipoPersonaController extends AppBaseController
{

	/** @var  catTipoPersonaRepository */
	private $catTipoPersonaRepository;

	function __construct(catTipoPersonaRepository $catTipoPersonaRepo)
	{
        $this->middleware('rolauth');		
		$this->catTipoPersonaRepository = $catTipoPersonaRepo;
	}

	/**
	 * Display a listing of the catTipoPersona.
	 *
	 * @return Response
	 */
	public function index()
	{
		$catTipoPersonas = $this->catTipoPersonaRepository->paginate(10);

		return view('catTipoPersonas.index')
			->with('catTipoPersonas', $catTipoPersonas);
	}

	/**
	 * Show the form for creating a new catTipoPersona.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('catTipoPersonas.create');
	}

	/**
	 * Store a newly created catTipoPersona in storage.
	 *
	 * @param CreatecatTipoPersonaRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatecatTipoPersonaRequest $request)
	{
		$input = $request->all();

		$catTipoPersona = $this->catTipoPersonaRepository->create($input);

		Flash::success('Tipo Persona creado correctamente.');

		return redirect(route('catTipoPersonas.index'));
	}

	/**
	 * Display the specified catTipoPersona.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$catTipoPersona = $this->catTipoPersonaRepository->find($id);

		if(empty($catTipoPersona))
		{
			Flash::error('Tipo Persona no encontrado.');

			return redirect(route('catTipoPersonas.index'));
		}

		return view('catTipoPersonas.show')->with('catTipoPersona', $catTipoPersona);
	}

	/**
	 * Show the form for editing the specified catTipoPersona.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$catTipoPersona = $this->catTipoPersonaRepository->find($id);

		if(empty($catTipoPersona))
		{
			Flash::error('Tipo Persona no encontrado.');

			return redirect(route('catTipoPersonas.index'));
		}

		return view('catTipoPersonas.edit')->with('catTipoPersona', $catTipoPersona);
	}

	/**
	 * Update the specified catTipoPersona in storage.
	 *
	 * @param  int              $id
	 * @param UpdatecatTipoPersonaRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatecatTipoPersonaRequest $request)
	{
		$catTipoPersona = $this->catTipoPersonaRepository->find($id);

		if(empty($catTipoPersona))
		{
			Flash::error('Tipo Persona no encontrado.');

			return redirect(route('catTipoPersonas.index'));
		}

		$this->catTipoPersonaRepository->updateRich($request->all(), $id);

		Flash::success('Tipo Persona actualizado correctamente.');

		return redirect(route('catTipoPersonas.index'));
	}

	/**
	 * Remove the specified catTipoPersona from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$catTipoPersona = $this->catTipoPersonaRepository->find($id);

		if(empty($catTipoPersona))
		{
			Flash::error('Tipo Persona no encontrado.');

			return redirect(route('catTipoPersonas.index'));
		}

		$this->catTipoPersonaRepository->delete($id);

		Flash::success('Tipo Persona eliminado correctamente.');

		return redirect(route('catTipoPersonas.index'));
	}

	/**
	 * @return mixed
	 */
	public function data() {
		$tipos = catTipoPersona::select('*');
		$tiposDT = Datatables::of($tipos)
			->addColumn('action', function($tipo) {
				$actions = '<div class="text-center button-action">';
				$actions .= '<a href="' . route('catTipoPersonas.edit', [$tipo->id]) . '"><i class="fa fa-edit"></i></a>';
				$actions .= '<a href="' . route('catTipoPersonas.delete', [$tipo->id]) . '" onclick="return confirm(\'¿Deseas eliminar: Tipo Persona?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$actions .= '</div>';
				return $actions;
			})
			->make(true);
		return $tiposDT;
	}
}
