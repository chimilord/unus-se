<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\ComisionRepository;
use App\Models\Comision;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ComisionAPIController extends AppBaseController
{
	/** @var  ComisionRepository */
	private $comisionRepository;

	function __construct(ComisionRepository $comisionRepo)
	{
		$this->comisionRepository = $comisionRepo;
	}

	/**
	 * Display a listing of the Comision.
	 * GET|HEAD /comisions
	 *
	 * @return Response
	 */
	public function index()
	{
		$comisions = $this->comisionRepository->all();

		return $this->sendResponse($comisions->toArray(), "Comisions retrieved successfully");
	}

	/**
	 * Show the form for creating a new Comision.
	 * GET|HEAD /comisions/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Comision in storage.
	 * POST /comisions
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Comision::$rules) > 0)
			$this->validateRequestOrFail($request, Comision::$rules);

		$input = $request->all();

		$comisions = $this->comisionRepository->create($input);

		return $this->sendResponse($comisions->toArray(), "Comision saved successfully");
	}

	/**
	 * Display the specified Comision.
	 * GET|HEAD /comisions/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$comision = $this->comisionRepository->apiFindOrFail($id);

		return $this->sendResponse($comision->toArray(), "Comision retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Comision.
	 * GET|HEAD /comisions/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified Comision in storage.
	 * PUT/PATCH /comisions/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var Comision $comision */
		$comision = $this->comisionRepository->apiFindOrFail($id);

		$result = $this->comisionRepository->updateRich($input, $id);

		$comision = $comision->fresh();

		return $this->sendResponse($comision->toArray(), "Comision updated successfully");
	}

	/**
	 * Remove the specified Comision from storage.
	 * DELETE /comisions/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->comisionRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Comision deleted successfully");
	}
}
