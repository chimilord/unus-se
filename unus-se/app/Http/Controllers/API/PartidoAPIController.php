<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\PartidoRepository;
use App\Models\Partido;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class PartidoAPIController extends AppBaseController
{
	/** @var  PartidoRepository */
	private $partidoRepository;

	function __construct(PartidoRepository $partidoRepo)
	{
		$this->partidoRepository = $partidoRepo;
	}

	/**
	 * Display a listing of the Partido.
	 * GET|HEAD /partidos
	 *
	 * @return Response
	 */
	public function index()
	{
		$partidos = $this->partidoRepository->all();

		return $this->sendResponse($partidos->toArray(), "Partidos retrieved successfully");
	}

	/**
	 * Show the form for creating a new Partido.
	 * GET|HEAD /partidos/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Partido in storage.
	 * POST /partidos
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Partido::$rules) > 0)
			$this->validateRequestOrFail($request, Partido::$rules);

		$input = $request->all();

		$partidos = $this->partidoRepository->create($input);

		return $this->sendResponse($partidos->toArray(), "Partido saved successfully");
	}

	/**
	 * Display the specified Partido.
	 * GET|HEAD /partidos/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$partido = $this->partidoRepository->apiFindOrFail($id);

		return $this->sendResponse($partido->toArray(), "Partido retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Partido.
	 * GET|HEAD /partidos/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified Partido in storage.
	 * PUT/PATCH /partidos/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var Partido $partido */
		$partido = $this->partidoRepository->apiFindOrFail($id);

		$result = $this->partidoRepository->updateRich($input, $id);

		$partido = $partido->fresh();

		return $this->sendResponse($partido->toArray(), "Partido updated successfully");
	}

	/**
	 * Remove the specified Partido from storage.
	 * DELETE /partidos/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->partidoRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Partido deleted successfully");
	}
}
