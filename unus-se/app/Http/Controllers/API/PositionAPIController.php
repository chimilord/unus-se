<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\PositionRepository;
use App\Models\Position;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class PositionAPIController extends AppBaseController
{
	/** @var  PositionRepository */
	private $positionRepository;

	function __construct(PositionRepository $positionRepo)
	{
		$this->positionRepository = $positionRepo;
	}

	/**
	 * Display a listing of the Position.
	 * GET|HEAD /positions
	 *
	 * @return Response
	 */
	public function index()
	{
		$positions = $this->positionRepository->all();

		return $this->sendResponse($positions->toArray(), "Positions retrieved successfully");
	}

	/**
	 * Show the form for creating a new Position.
	 * GET|HEAD /positions/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Position in storage.
	 * POST /positions
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Position::$rules) > 0)
			$this->validateRequestOrFail($request, Position::$rules);

		$input = $request->all();

		$positions = $this->positionRepository->create($input);

		return $this->sendResponse($positions->toArray(), "Position saved successfully");
	}

	/**
	 * Display the specified Position.
	 * GET|HEAD /positions/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$position = $this->positionRepository->apiFindOrFail($id);

		return $this->sendResponse($position->toArray(), "Position retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Position.
	 * GET|HEAD /positions/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified Position in storage.
	 * PUT/PATCH /positions/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var Position $position */
		$position = $this->positionRepository->apiFindOrFail($id);

		$result = $this->positionRepository->updateRich($input, $id);

		$position = $position->fresh();

		return $this->sendResponse($position->toArray(), "Position updated successfully");
	}

	/**
	 * Remove the specified Position from storage.
	 * DELETE /positions/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->positionRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Position deleted successfully");
	}
}
