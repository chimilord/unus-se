<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\PersonRepository;
use App\Models\Person;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class PersonAPIController extends AppBaseController
{
	/** @var  PersonRepository */
	private $personRepository;

	function __construct(PersonRepository $personRepo)
	{
		$this->personRepository = $personRepo;
	}

	/**
	 * Display a listing of the Person.
	 * GET|HEAD /people
	 *
	 * @return Response
	 */
	public function index()
	{
		$people = $this->personRepository->all();

		return $this->sendResponse($people->toArray(), "People retrieved successfully");
	}

	/**
	 * Show the form for creating a new Person.
	 * GET|HEAD /people/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Person in storage.
	 * POST /people
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Person::$rules) > 0)
			$this->validateRequestOrFail($request, Person::$rules);

		$input = $request->all();

		$people = $this->personRepository->create($input);

		return $this->sendResponse($people->toArray(), "Person saved successfully");
	}

	/**
	 * Display the specified Person.
	 * GET|HEAD /people/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$person = $this->personRepository->apiFindOrFail($id);

		return $this->sendResponse($person->toArray(), "Person retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Person.
	 * GET|HEAD /people/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified Person in storage.
	 * PUT/PATCH /people/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var Person $person */
		$person = $this->personRepository->apiFindOrFail($id);

		$result = $this->personRepository->updateRich($input, $id);

		$person = $person->fresh();

		return $this->sendResponse($person->toArray(), "Person updated successfully");
	}

	/**
	 * Remove the specified Person from storage.
	 * DELETE /people/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->personRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Person deleted successfully");
	}
}
