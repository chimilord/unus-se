<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\EstadoRepository;
use App\Models\Estado;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class EstadoAPIController extends AppBaseController
{
	/** @var  EstadoRepository */
	private $estadoRepository;

	function __construct(EstadoRepository $estadoRepo)
	{
		$this->estadoRepository = $estadoRepo;
	}

	/**
	 * Display a listing of the Estado.
	 * GET|HEAD /estados
	 *
	 * @return Response
	 */
	public function index()
	{
		$estados = $this->estadoRepository->all();

		return $this->sendResponse($estados->toArray(), "Estados retrieved successfully");
	}

	/**
	 * Show the form for creating a new Estado.
	 * GET|HEAD /estados/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Estado in storage.
	 * POST /estados
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Estado::$rules) > 0)
			$this->validateRequestOrFail($request, Estado::$rules);

		$input = $request->all();

		$estados = $this->estadoRepository->create($input);

		return $this->sendResponse($estados->toArray(), "Estado saved successfully");
	}

	/**
	 * Display the specified Estado.
	 * GET|HEAD /estados/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$estado = $this->estadoRepository->apiFindOrFail($id);

		return $this->sendResponse($estado->toArray(), "Estado retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Estado.
	 * GET|HEAD /estados/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified Estado in storage.
	 * PUT/PATCH /estados/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var Estado $estado */
		$estado = $this->estadoRepository->apiFindOrFail($id);

		$result = $this->estadoRepository->updateRich($input, $id);

		$estado = $estado->fresh();

		return $this->sendResponse($estado->toArray(), "Estado updated successfully");
	}

	/**
	 * Remove the specified Estado from storage.
	 * DELETE /estados/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->estadoRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Estado deleted successfully");
	}
}
