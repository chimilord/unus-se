<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateEstadoRequest;
use App\Http\Requests\UpdateEstadoRequest;
use App\Libraries\Repositories\EstadoRepository;
use App\Models\Estado;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;

class EstadoController extends AppBaseController
{

	/** @var  EstadoRepository */
	private $estadoRepository;

	function __construct(EstadoRepository $estadoRepo)
	{
        $this->middleware('rolauth');
		$this->estadoRepository = $estadoRepo;
	}

	/**
	 * Display a listing of the Estado.
	 *
	 * @return Response
	 */
	public function index()
	{
		//$estados = $this->estadoRepository->paginate(10);
		//return view('estados.index')->with('estados', $estados);
		$estados = $this->estadoRepository->all();
		return view('estados.index')->with('estados', $estados);
	}

	/**
	 * Show the form for creating a new Estado.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('estados.create');
	}

	/**
	 * Store a newly created Estado in storage.
	 *
	 * @param CreateEstadoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateEstadoRequest $request)
	{
		$input = $request->all();

		$estado = $this->estadoRepository->create($input);

		Flash::success('Estado creado correctamente.');

		return redirect(route('estados.index'));
	}

	/**
	 * Display the specified Estado.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$estado = $this->estadoRepository->find($id);

		if(empty($estado))
		{
			Flash::error('Estado no encontrado.');

			return redirect(route('estados.index'));
		}

		return view('estados.show')->with('estado', $estado);
	}

	/**
	 * Show the form for editing the specified Estado.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$estado = $this->estadoRepository->find($id);

		if(empty($estado))
		{
			Flash::error('Estado no encontrado.');

			return redirect(route('estados.index'));
		}

		return view('estados.edit')->with('estado', $estado);
	}

	/**
	 * Update the specified Estado in storage.
	 *
	 * @param  int              $id
	 * @param UpdateEstadoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateEstadoRequest $request)
	{
		$estado = $this->estadoRepository->find($id);

		if(empty($estado))
		{
			Flash::error('Estado no encontrado.');

			return redirect(route('estados.index'));
		}

		$this->estadoRepository->updateRich($request->all(), $id);

		Flash::success('Estado actualizado correctamente.');

		return redirect(route('estados.index'));
	}

	/**
	 * Remove the specified Estado from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$estado = $this->estadoRepository->find($id);

		if(empty($estado))
		{
			Flash::error('Estado no encontrado.');

			return redirect(route('estados.index'));
		}

		$this->estadoRepository->delete($id);

		Flash::success('Estado eliminado correctamente.');

		return redirect(route('estados.index'));
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function data() {
		$estados = Estado::select('*');
		$estadosDT = Datatables::of($estados)
			->addColumn('action', function ($estado) {
				$actions = '<div class="text-center button-action">';
				$actions .= '<a href="' . route('estados.edit', [$estado->id]) . '"><i class="fa fa-edit"></i></a>';
				$actions .= '<a href="' . route('estados.delete', [$estado->id]) . '" onclick="return confirm(\'¿Confirmar la eliminación de la Entidad Federativa?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$actions .= '</div>';
				return $actions;
			})
			->make(true);
		return $estadosDT;
	}
}
