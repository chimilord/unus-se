<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatecatComiteRequest;
use App\Http\Requests\UpdatecatComiteRequest;
use App\Libraries\Repositories\catComiteRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use DB;
use Yajra\Datatables\Datatables;
use App\Models\catComite;


class catComiteController extends AppBaseController
{

	/** @var  catComiteRepository */
	private $catComiteRepository;

	function __construct(catComiteRepository $catComiteRepo)
	{
		$this->middleware('rolauth');
		$this->catComiteRepository = $catComiteRepo;
	}

	/**
	 * Display a listing of the catComite.
	 *
	 * @return Response
	 */
	public function index()
	{
		$catComites = $this->catComiteRepository->paginate(10);

		return view('catComites.index')
			->with('catComites', $catComites);
	}

	/**
	 * Show the form for creating a new catComite.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('catComites.create');
	}

	/**
	 * Store a newly created catComite in storage.
	 *
	 * @param CreatecatComiteRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatecatComiteRequest $request)
	{
		$input = $request->all();

		$catComite = $this->catComiteRepository->create($input);

		Flash::success('Comité creado correctamente.');

		return redirect(route('catComites.index'));
	}

	/**
	 * Display the specified catComite.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$catComite = $this->catComiteRepository->find($id);

		if(empty($catComite))
		{
			Flash::error('Comité no encontrado.');

			return redirect(route('catComites.index'));
		}

		return view('catComites.show')->with('catComite', $catComite);
	}

	/**
	 * Show the form for editing the specified catComite.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$catComite = $this->catComiteRepository->find($id);

		if(empty($catComite))
		{
			Flash::error('Comité no encontrado.');

			return redirect(route('catComites.index'));
		}

		return view('catComites.edit')->with('catComite', $catComite);
	}

	/**
	 * Update the specified catComite in storage.
	 *
	 * @param  int              $id
	 * @param UpdatecatComiteRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatecatComiteRequest $request)
	{
		$catComite = $this->catComiteRepository->find($id);

		if(empty($catComite))
		{
			Flash::error('Comité no encontrado.');

			return redirect(route('catComites.index'));
		}

		$this->catComiteRepository->updateRich($request->all(), $id);

		Flash::success('Comité actualizado correctamente.');

		return redirect(route('catComites.index'));
	}

	/**
	 * Remove the specified catComite from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$catComite = $this->catComiteRepository->find($id);

		if(empty($catComite))
		{
			Flash::error('Comité no encontrado.');

			return redirect(route('catComites.index'));
		}

		$this->catComiteRepository->delete($id);

		Flash::success('Comité eliminado correctamente.');

		return redirect(route('catComites.index'));
	}
	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function data() {
		$comites = catComite::select('*');
		$comitesDT = Datatables::of($comites)
			->addColumn('action', function ($comite) {
				$buttons = '<div class="text-center button-action">';
				$buttons .= '<a href="' . route('catComites.edit', [$comite->id]) . '"><i class="fa fa-edit"></i></a>';
				$buttons .= '<a href="' . route('catComites.delete', [$comite->id]) . '" onclick="return confirm(\'¿Confirmar la eliminación del Comite?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$buttons .= '</div>';
				return $buttons;
			})
			->make(true);
		return $comitesDT;
	}
}
