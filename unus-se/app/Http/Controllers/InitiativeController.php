<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateInitiativeRequest;
use App\Http\Requests\UpdateInitiativeRequest;
use App\Libraries\Repositories\InitiativeRepository;
use App\Models\Initiative;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\Person;
use App\Models\CatStatus;
use App\Models\catCamara;
use Yajra\Datatables\Datatables;
use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Comision;
use App\Models\catStatusTurnado;



class InitiativeController extends AppBaseController
{
	/** @var  InitiativeRepository */
	private $initiativeRepository;

	function __construct(InitiativeRepository $initiativeRepo)
	{
		$this->middleware('rolauth');
		$this->initiativeRepository = $initiativeRepo;
	}

	/**
	 * Display a listing of the Initiative.
	 *
	 * @return Response
	 */
	public function index()
	{
		$initiatives = $this->initiativeRepository->all();
		return view('initiatives.index')->with('initiatives', $initiatives);
	}

	/**
	 * Show the form for creating a new Initiative.
	 *
	 * @return Response
	 */
	public function create()
	{
		$status_id = CatStatus::lists('name', 'id');
		$idCamara = CatCamara::lists('name', 'id');
		$positions = \DB::table('positions')->lists('nombre', 'id');
		$persons = \DB::table('person')->selectRaw('CONCAT(nombre,\' \',apellidos) as fulln,id' )->lists('fulln', 'id');
		$turnados = \DB::table('catStatusTurnados')->lists('nombre', 'id');
		$comisiones=DB::table('comisions')
            ->join('catComisiones', 'comisions.idCatComision', '=', 'catComisiones.id')
            ->lists('catComisiones.nombre','comisions.id');
		return view('initiatives.create',array('persons'=>$persons,'status_id'=>$status_id,'idCamara'=>$idCamara, 'positions' => $positions, 'turnados'=>$turnados,'comisiones'=>$comisiones));
	}

	/**
	 * @param $date
	 * @return string
	 */
	public function formatDate($date)
	{
		if (stripos($date, "/")) {
			$format = explode("/", $date);
			return $format[2] . '-' . $format[1] . '-' . $format[0];
		}
		return $date;
	}

	/**
	 * @param $date
	 * @return string
	 */
	public function showDate($date)
	{
		if (stripos($date, "-")) {
			$format = explode("-", $date);
			return $format[2] . '/' . $format[1] . '/' . $format[0];
		}
		return $date;
	}

	/**
	 * Store a newly created Initiative in storage.
	 *
	 * @param CreateInitiativeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateInitiativeRequest $request)
	{
		$input = $request->all();
		$input['submit_date'] = $this->formatDate($input['submit_date']);
		$input['publication_date'] = $this->formatDate($input['publication_date']);
		$input['fechaDecreto'] = $this->formatDate($input['fechaDecreto']);
		$persons = $input['persons'];
		$positions = $input['positions'];
		$comisiones = $input['comisiones'];
		$turnados = $input['turnados'];
		$i=0;
		foreach($persons as $key => $value) {
            $persons[$value] =  ["position_id"=>$positions[$i++]];
        }
        $j=0;
		foreach($comisiones as $key => $value) {
            $comisiones[$value] =  ["idStatusTurnado"=>$turnados[$j++]];
        }
		$initiative = $this->initiativeRepository->create($input);
		$initiative->persons()->sync($persons);
		$initiative->comisiones()->sync($comisiones);
		Flash::success('Iniciativa creada correctamente.');

		return redirect(route('initiatives.index'));
	}

	/**
	 * Display the specified Initiative.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$initiative = $this->initiativeRepository->find($id);
		$status_id = CatStatus::lists('name', 'id');
		$idCamara = CatCamara::lists('name', 'id');
		$persons = $initiative->persons()->get();
		$comisiones =$initiative->comisiones()->get();

		if(empty($initiative))
		{
			Flash::error('Iniciativa no encontrada.');

			return redirect(route('initiatives.index'));
		} else {
			$status_name = DB::table('catStatuses')->where('id', $initiative->status_id)->get();
			$comisiones_id = [];
			$partidos = [];
			foreach($comisiones as $comision) {
				$comisiones_id[] = $comision->idCatComision;
			}
			foreach ($persons as $person) {
				$partidos_id[] = $person->idPartido;
			}
			$nombres = DB::table('catComisiones')->whereIn('id', $comisiones_id)->get();
			$partidosNombres = DB::table('partidos')->whereIn('id', $partidos_id)->get();
			$initiative->publication_date = $this->showDate($initiative->publication_date);
			$initiative->submit_date = $this->showDate($initiative->submit_date);
			$initiative->fechaDecreto = $this->showDate($initiative->fechaDecreto);
		}
		return view('initiatives.show',array('initiative'=>$initiative,'persons'=>$persons,'status_id'=>$status_id,'idCamara'=>$idCamara, 'status_name'=>$status_name[0]->name, 'comisiones'=>$comisiones, 'nombres'=>$nombres, 'partidos' => $partidosNombres));


	}

	/**
	 * Show the form for editing the specified Initiative.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$initiative = $this->initiativeRepository->find($id);
		$status_id = CatStatus::lists('name', 'id');
		$idCamara = CatCamara::lists('name', 'id');
		$positions = \DB::table('positions')->lists('nombre', 'id');
		$persons = \DB::table('person')->selectRaw('CONCAT(nombre,\' \',apellidos) as fulln,id' )->lists('fulln', 'id');
		$selectedPersons =$initiative->persons()->get();
		
		$turnados = \DB::table('catStatusTurnados')->lists('nombre', 'id');
		$comisiones=DB::table('comisions')
            ->join('catComisiones', 'comisions.idCatComision', '=', 'catComisiones.id')
            ->lists('catComisiones.nombre','comisions.id');
		

		$selectedComisions=DB::table('comisions')
            ->join('catComisiones', 'comisions.idCatComision', '=', 'catComisiones.id')
            ->join('iniciativa_comision', 'iniciativa_comision.comision_id', '=', 'comisions.id')
            ->join('catStatusTurnados', 'catStatusTurnados.id', '=', 'iniciativa_comision.idStatusTurnado')
            ->select('catComisiones.nombre as comision','comisions.id as idComision','catStatusTurnados.nombre as status','catStatusTurnados.id as idStatus')
            ->where('iniciativa_comision.iniciativa_id','=',$id)
			->get();


		if(empty($initiative))
		{
			Flash::error('Iniciativa no encontrada.');

			return redirect(route('initiatives.index'));
		} else {
			$status_id = CatStatus::lists('name', 'id');
			$idCamara = CatCamara::lists('name', 'id');
			$positions = \DB::table('positions')->lists('nombre', 'id');
			$persons = \DB::table('person')->selectRaw('CONCAT(nombre,\' \',apellidos) as fulln,id' )->lists('fulln', 'id');
			$selectedPersons = DB::table('person_iniciativa')
				->join('person', 'person_iniciativa.person_id', '=', 'person.id')
				->join('initiatives', 'person_iniciativa.initiatives_id', '=', 'initiatives.id')
				->join('positions', 'person_iniciativa.position_id', '=', 'positions.id')
				->select('person.id', 'person.nombre', 'person.apellidos', DB::raw('positions.nombre as cargo, positions.id as cargo_id'))
				->where('person_iniciativa.initiatives_id', '=', $initiative->id)
				->get();
			$initiative->publication_date = $this->showDate($initiative->publication_date);
			$initiative->submit_date = $this->showDate($initiative->submit_date);
			$initiative->fechaDecreto = $this->showDate($initiative->fechaDecreto);
		}
		return view('initiatives.edit',array('selectedPersons'=>$selectedPersons,'persons'=>$persons,'status_id'=>$status_id,'idCamara'=>$idCamara,'positions' => $positions, 'turnados'=>$turnados,'comisiones'=>$comisiones, 'selectedComisions'=>$selectedComisions))->with('initiative', $initiative);
	}

	/**
	 * Update the specified Initiative in storage.
	 *
	 * @param  int              $id
	 * @param UpdateInitiativeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateInitiativeRequest $request)
	{
		$input = $request->all();
		$input['submit_date'] = $this->formatDate($input['submit_date']);
		$input['publication_date'] = $this->formatDate($input['publication_date']);
		$input['fechaDecreto'] = $this->formatDate($input['fechaDecreto']);
		$persons = $input['persons'];
		$positions = $input['positions'];
		$i=0;
		foreach($persons as $key => $value) {
            $persons[$value] =  ["position_id"=>$positions[$i++]];
        }

        $comisiones = $input['comisiones'];
		$turnados = $input['turnados'];
		$i=0;
		foreach($comisiones as $key => $value) {
            $comisiones[$value] =  ["idStatusTurnado"=>$turnados[$i++]];
        }
		$initiative = $this->initiativeRepository->find($id);
		$initiative->persons()->sync($persons);
		$initiative->comisiones()->sync($comisiones);

		if(empty($initiative))
		{
			Flash::error('Iniciativa no encontrada.');

			return redirect(route('initiatives.index'));
		}

		//$this->initiativeRepository->updateRich($request->all(), $id);
		$this->initiativeRepository->updateRich($input, $id);

		Flash::success('Iniciativa actualizada correctamente.');

		return redirect(route('initiatives.index'));
	}

	/**
	 * Remove the specified Initiative from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$initiative = $this->initiativeRepository->find($id);

		if(empty($initiative))
		{
			Flash::error('Iniciativa no encontrada.');

			return redirect(route('initiatives.index'));
		}

		$this->initiativeRepository->delete($id);

		Flash::success('Iniciativa eliminada correctamente.');

		return redirect(route('initiatives.index'));
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function data() {
		$initiatives = Initiative::select('*');
		$initiativesDT = Datatables::of($initiatives)
			->editColumn('description', '{!! str_limit($description, 200) !!}')
			->addColumn('action', function ($initiative) {
				$buttons = '<div class="text-center button-action">';
				$buttons .= '<a href="' . route('initiatives.edit', [$initiative->id]) . '"><i class="fa fa-edit"></i></a>';
				$buttons .= '<a href="' . route('initiatives.delete', [$initiative->id]) . '" onclick="return confirm(\'¿Deseas eliminar: Iniciativa?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$buttons .= '</div>';
				return $buttons;
			})
			->make(true);
		return $initiativesDT;
	}

}
