<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatecatStatusTurnadoRequest;
use App\Http\Requests\UpdatecatStatusTurnadoRequest;
use App\Libraries\Repositories\catStatusTurnadoRepository;
use App\Models\catStatusTurnado;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;

class catStatusTurnadoController extends AppBaseController
{

	/** @var  catStatusTurnadoRepository */
	private $catStatusTurnadoRepository;

	function __construct(catStatusTurnadoRepository $catStatusTurnadoRepo)
	{
        $this->middleware('rolauth');		
		$this->catStatusTurnadoRepository = $catStatusTurnadoRepo;
	}

	/**
	 * Display a listing of the catStatusTurnado.
	 *
	 * @return Response
	 */
	public function index()
	{
		$catStatusTurnados = $this->catStatusTurnadoRepository->paginate(10);

		return view('catStatusTurnados.index')
			->with('catStatusTurnados', $catStatusTurnados);
	}

	/**
	 * Show the form for creating a new catStatusTurnado.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('catStatusTurnados.create');
	}

	/**
	 * Store a newly created catStatusTurnado in storage.
	 *
	 * @param CreatecatStatusTurnadoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatecatStatusTurnadoRequest $request)
	{
		$input = $request->all();

		$catStatusTurnado = $this->catStatusTurnadoRepository->create($input);

		Flash::success('Turno creado correctamente.');

		return redirect(route('catStatusTurnados.index'));
	}

	/**
	 * Display the specified catStatusTurnado.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$catStatusTurnado = $this->catStatusTurnadoRepository->find($id);

		if(empty($catStatusTurnado))
		{
			Flash::error('Turno no encontrado.');

			return redirect(route('catStatusTurnados.index'));
		}

		return view('catStatusTurnados.show')->with('catStatusTurnado', $catStatusTurnado);
	}

	/**
	 * Show the form for editing the specified catStatusTurnado.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$catStatusTurnado = $this->catStatusTurnadoRepository->find($id);

		if(empty($catStatusTurnado))
		{
			Flash::error('Turno no encontrado.');

			return redirect(route('catStatusTurnados.index'));
		}

		return view('catStatusTurnados.edit')->with('catStatusTurnado', $catStatusTurnado);
	}

	/**
	 * Update the specified catStatusTurnado in storage.
	 *
	 * @param  int              $id
	 * @param UpdatecatStatusTurnadoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatecatStatusTurnadoRequest $request)
	{
		$catStatusTurnado = $this->catStatusTurnadoRepository->find($id);

		if(empty($catStatusTurnado))
		{
			Flash::error('Turno no encontrado.');

			return redirect(route('catStatusTurnados.index'));
		}

		$this->catStatusTurnadoRepository->updateRich($request->all(), $id);

		Flash::success('Turno actualizado correctamente.');

		return redirect(route('catStatusTurnados.index'));
	}

	/**
	 * Remove the specified catStatusTurnado from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$catStatusTurnado = $this->catStatusTurnadoRepository->find($id);

		if(empty($catStatusTurnado))
		{
			Flash::error('Turno no encontrado.');

			return redirect(route('catStatusTurnados.index'));
		}

		$this->catStatusTurnadoRepository->delete($id);

		Flash::success('Turno eliminado correctamente.');

		return redirect(route('catStatusTurnados.index'));
	}

	/**
	 * @return mixed
	 */
	public function data() {
		$turnados = catStatusTurnado::select('*');
		$turnadosDT = Datatables::of($turnados)
			->addColumn('action', function($turnado) {
				$actions = '<div class="text-center button-action">';
				$actions .= '<a href="' . route('catStatusTurnados.edit', [$turnado->id]) . '"><i class="fa fa-edit"></i></a>';
				$actions .= '<a href="' . route('catStatusTurnados.delete', [$turnado->id]) . '" onclick="return confirm(\'¿Deseas eliminar: Estatus Turnado?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$actions .= '</div>';
				return $actions;
			})
			->make(true);
		return $turnadosDT;
	}

}
