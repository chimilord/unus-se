<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatecatOrganoGobiernoRequest;
use App\Http\Requests\UpdatecatOrganoGobiernoRequest;
use App\Libraries\Repositories\catOrganoGobiernoRepository;
use App\Models\catOrganoGobierno;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;

class catOrganoGobiernoController extends AppBaseController
{

	/** @var  catOrganoGobiernoRepository */
	private $catOrganoGobiernoRepository;

	function __construct(catOrganoGobiernoRepository $catOrganoGobiernoRepo)
	{
        $this->middleware('rolauth');		
		$this->catOrganoGobiernoRepository = $catOrganoGobiernoRepo;
	}

	/**
	 * Display a listing of the catOrganoGobierno.
	 *
	 * @return Response
	 */
	public function index()
	{
		$catOrganoGobiernos = $this->catOrganoGobiernoRepository->paginate(10);

		return view('catOrganoGobiernos.index')
			->with('catOrganoGobiernos', $catOrganoGobiernos);
	}

	/**
	 * Show the form for creating a new catOrganoGobierno.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('catOrganoGobiernos.create');
	}

	/**
	 * Store a newly created catOrganoGobierno in storage.
	 *
	 * @param CreatecatOrganoGobiernoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatecatOrganoGobiernoRequest $request)
	{
		$input = $request->all();

		$catOrganoGobierno = $this->catOrganoGobiernoRepository->create($input);

		Flash::success('Órgano de Gobierno creado correctamente.');

		return redirect(route('catOrganoGobiernos.index'));
	}

	/**
	 * Display the specified catOrganoGobierno.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$catOrganoGobierno = $this->catOrganoGobiernoRepository->find($id);

		if(empty($catOrganoGobierno))
		{
			Flash::error('Órgano de Gobierno no encontrado.');

			return redirect(route('catOrganoGobiernos.index'));
		}

		return view('catOrganoGobiernos.show')->with('catOrganoGobierno', $catOrganoGobierno);
	}

	/**
	 * Show the form for editing the specified catOrganoGobierno.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$catOrganoGobierno = $this->catOrganoGobiernoRepository->find($id);

		if(empty($catOrganoGobierno))
		{
			Flash::error('Órgano de Gobierno no encontrado.');

			return redirect(route('catOrganoGobiernos.index'));
		}

		return view('catOrganoGobiernos.edit')->with('catOrganoGobierno', $catOrganoGobierno);
	}

	/**
	 * Update the specified catOrganoGobierno in storage.
	 *
	 * @param  int              $id
	 * @param UpdatecatOrganoGobiernoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatecatOrganoGobiernoRequest $request)
	{
		$catOrganoGobierno = $this->catOrganoGobiernoRepository->find($id);

		if(empty($catOrganoGobierno))
		{
			Flash::error('Órgano de Gobierno no encontrado.');

			return redirect(route('catOrganoGobiernos.index'));
		}

		$this->catOrganoGobiernoRepository->updateRich($request->all(), $id);

		Flash::success('Órgano de Gobierno actualizado correctamente.');

		return redirect(route('catOrganoGobiernos.index'));
	}

	/**
	 * Remove the specified catOrganoGobierno from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$catOrganoGobierno = $this->catOrganoGobiernoRepository->find($id);

		if(empty($catOrganoGobierno))
		{
			Flash::error('Órgano de Gobierno no encontrado.');

			return redirect(route('catOrganoGobiernos.index'));
		}

		$this->catOrganoGobiernoRepository->delete($id);

		Flash::success('Órgano de Gobierno eliminado correctamente.');

		return redirect(route('catOrganoGobiernos.index'));
	}

	/**
	 * @return mixed
	 */
	public function data() {
		$odg = catOrganoGobierno::select('*');
		$odgDT = Datatables::of($odg)
			->addColumn('action', function($odg) {
				$actions = '<div class="text-center button-action">';
				$actions .= '<a href="' . route('catOrganoGobiernos.edit', [$odg->id]) . '"><i class="fa fa-edit"></i></a>';
				$actions .= '<a href="' . route('catOrganoGobiernos.delete', [$odg->id]) . '" onclick="return confirm(\'¿Deseas eliminar: Órgano de Gobierno?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$actions .= '</div>';
				return $actions;
			})
			->make(true);
		return $odgDT;
	}

}
