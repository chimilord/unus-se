<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatecatPeriodoRequest;
use App\Http\Requests\UpdatecatPeriodoRequest;
use App\Libraries\Repositories\catPeriodoRepository;
use App\Models\catPeriodo;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;

class catPeriodoController extends AppBaseController
{

	/** @var  catPeriodoRepository */
	private $catPeriodoRepository;

	function __construct(catPeriodoRepository $catPeriodoRepo)
	{
        $this->middleware('rolauth');		
		$this->catPeriodoRepository = $catPeriodoRepo;
	}

	/**
	 * Display a listing of the catPeriodo.
	 *
	 * @return Response
	 */
	public function index()
	{
		$catPeriodos = $this->catPeriodoRepository->paginate(10);

		return view('catPeriodos.index')
			->with('catPeriodos', $catPeriodos);
	}

	/**
	 * Show the form for creating a new catPeriodo.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('catPeriodos.create');
	}

	/**
	 * Store a newly created catPeriodo in storage.
	 *
	 * @param CreatecatPeriodoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatecatPeriodoRequest $request)
	{
		$input = $request->all();
		$input['fechaInicio'] = $this->formatDate($input['fechaInicio']);
		$input['fechaFin'] = $this->formatDate($input['fechaFin']);

		$catPeriodo = $this->catPeriodoRepository->create($input);

		Flash::success('Periodo creado correctamente.');

		return redirect(route('catPeriodos.index'));
	}

	/**
	 * Display the specified catPeriodo.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$catPeriodo = $this->catPeriodoRepository->find($id);

		if(empty($catPeriodo))
		{
			Flash::error('Periodo no encontrado.');

			return redirect(route('catPeriodos.index'));
		}

		return view('catPeriodos.show')->with('catPeriodo', $catPeriodo);
	}

	/**
	 * Show the form for editing the specified catPeriodo.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$catPeriodo = $this->catPeriodoRepository->find($id);

		if(empty($catPeriodo))
		{
			Flash::error('Periodo no encontrado.');

			return redirect(route('catPeriodos.index'));
		} else {
			$catPeriodo['fechaInicio'] = $this->showDate($catPeriodo['fechaInicio']);
			$catPeriodo['fechaFin'] = $this->showDate($catPeriodo['fechaFin']);
		}

		return view('catPeriodos.edit')->with('catPeriodo', $catPeriodo);
	}

	/**
	 * Update the specified catPeriodo in storage.
	 *
	 * @param  int              $id
	 * @param UpdatecatPeriodoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatecatPeriodoRequest $request)
	{
		$input = $request->all();
		$input['fechaInicio'] = $this->formatDate($input['fechaInicio']);
		$input['fechaFin'] = $this->formatDate($input['fechaFin']);

		$catPeriodo = $this->catPeriodoRepository->find($id);

		if(empty($catPeriodo))
		{
			Flash::error('Periodo no encontrado.');

			return redirect(route('catPeriodos.index'));
		}

		$this->catPeriodoRepository->updateRich($input, $id);

		Flash::success('Periodo actualizado correctamente.');

		return redirect(route('catPeriodos.index'));
	}

	/**
	 * Remove the specified catPeriodo from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$catPeriodo = $this->catPeriodoRepository->find($id);

		if(empty($catPeriodo))
		{
			Flash::error('Periodo no encontrado.');

			return redirect(route('catPeriodos.index'));
		}

		$this->catPeriodoRepository->delete($id);

		Flash::success('Periodo eliminado correctamente.');

		return redirect(route('catPeriodos.index'));
	}

	/**
	 * @return mixed
	 */
	public function data() {
		$periodos = catPeriodo::select('*');
		$periodosDT = Datatables::of($periodos)
			->editColumn('fechaInicio', function ($periodo) {
				return $this->showDate($periodo->fechaInicio);
			})
			->editColumn('fechaFin', function ($periodo) {
				return $this->showDate($periodo->fechaFin);
			})
			->addColumn('action', function($periodo) {
				$actions = '<div class="text-center button-action">';
				$actions .= '<a href="' . route('catPeriodos.edit', [$periodo->id]) . '"><i class="fa fa-edit"></i></a>';
				$actions .= '<a href="' . route('catPeriodos.delete', [$periodo->id]) . '" onclick="return confirm(\'¿Deseas eliminar: Periodo?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$actions .= '</div>';
				return $actions;
			})
			->make(true);
		return $periodosDT;
	}

	/**
	 * @param $date
	 * @return string
	 */
	public function formatDate($date)
	{
		if (stripos($date, "/")) {
			$format = explode("/", $date);
			// Special format date because daterangepicker format is MM/DD/YYYY
			return $format[2] . '-' . $format[1] . '-' . $format[0];
		}
		return $date;
	}

	/**
	 * @param $date
	 * @return string
	 */
	public function showDate($date)
	{
		if (stripos($date, "-")) {
			$format = explode("-", $date);
			// Special format date because daterangepicker format is MM/DD/YYYY
			return $format[2] . '/' . $format[1] . '/' . $format[0];
		}
		return $date;
	}

}
