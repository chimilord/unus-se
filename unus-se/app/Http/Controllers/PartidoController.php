<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePartidoRequest;
use App\Http\Requests\UpdatePartidoRequest;
use App\Libraries\Repositories\PartidoRepository;
use App\Models\Partido;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;

class PartidoController extends AppBaseController
{

	/** @var  PartidoRepository */
	private $partidoRepository;

	function __construct(PartidoRepository $partidoRepo)
	{
		$this->middleware('rolauth');
		$this->partidoRepository = $partidoRepo;
	}

	/**
	 * Display a listing of the Partido.
	 *
	 * @return Response
	 */
	public function index()
	{
		$partidos = $this->partidoRepository->all();

		return view('partidos.index')->with('partidos', $partidos);
	}

	/**
	 * Show the form for creating a new Partido.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('partidos.create');
	}

	/**
	 * Store a newly created Partido in storage.
	 *
	 * @param CreatePartidoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePartidoRequest $request)
	{
		$input = $request->all();

		$partido = $this->partidoRepository->create($input);

		Flash::success('Partido creado correctamente.');

		return redirect(route('partidos.index'));
	}

	/**
	 * Display the specified Partido.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$partido = $this->partidoRepository->find($id);

		if(empty($partido))
		{
			Flash::error('Partido no encontrado');

			return redirect(route('partidos.index'));
		}

		return view('partidos.show')->with('partido', $partido);
	}

	/**
	 * Show the form for editing the specified Partido.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$partido = $this->partidoRepository->find($id);

		if(empty($partido))
		{
			Flash::error('Partido no encontrado');

			return redirect(route('partidos.index'));
		}

		return view('partidos.edit')->with('partido', $partido);
	}

	/**
	 * Update the specified Partido in storage.
	 *
	 * @param  int              $id
	 * @param UpdatePartidoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatePartidoRequest $request)
	{
		$partido = $this->partidoRepository->find($id);

		if(empty($partido))
		{
			Flash::error('Partido no encontrado');

			return redirect(route('partidos.index'));
		}

		$this->partidoRepository->updateRich($request->all(), $id);

		Flash::success('Partido actualizado correctamente.');

		return redirect(route('partidos.index'));
	}

	/**
	 * Remove the specified Partido from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$partido = $this->partidoRepository->find($id);

		if(empty($partido))
		{
			Flash::error('Partido no encontrado');

			return redirect(route('partidos.index'));
		}

		$this->partidoRepository->delete($id);

		Flash::success('Partido borrado correctamente.');

		return redirect(route('partidos.index'));
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function data() {
		$partidos = Partido::select('*');
		$partidosDT = Datatables::of($partidos)
			->addColumn('action', function ($partido) {
				$buttons = '<div class="text-center button-action">';
				$buttons .= '<a href="' . route('partidos.edit', [$partido->id]) . '"><i class="fa fa-edit"></i></a>';
				$buttons .= '<a href="' . route('partidos.delete', [$partido->id]) . '" onclick="return confirm(\'¿Confirmar la eliminación del Partido Político?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$buttons .= '</div>';
				return $buttons;
			})
			->make(true);
		return $partidosDT;
	}
}
