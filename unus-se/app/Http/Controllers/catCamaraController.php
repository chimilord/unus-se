<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatecatCamaraRequest;
use App\Http\Requests\UpdatecatCamaraRequest;
use App\Libraries\Repositories\catCamaraRepository;
use App\Models\catCamara;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;

class catCamaraController extends AppBaseController
{

	/** @var  catCamaraRepository */
	private $catCamaraRepository;

	function __construct(catCamaraRepository $catCamaraRepo)
	{
        $this->middleware('auth');
		$this->catCamaraRepository = $catCamaraRepo;
	}

	/**
	 * Display a listing of the catCamara.
	 *
	 * @return Response
	 */
	public function index()
	{
		$catCamaras = $this->catCamaraRepository->paginate(10);

		return view('catCamaras.index')
			->with('catCamaras', $catCamaras);
	}

	/**
	 * Show the form for creating a new catCamara.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('catCamaras.create');
	}

	/**
	 * Store a newly created catCamara in storage.
	 *
	 * @param CreatecatCamaraRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatecatCamaraRequest $request)
	{
		$input = $request->all();

		$catCamara = $this->catCamaraRepository->create($input);

		Flash::success('Cámara creada correctamente.');

		return redirect(route('catCamaras.index'));
	}

	/**
	 * Display the specified catCamara.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$catCamara = $this->catCamaraRepository->find($id);

		if(empty($catCamara))
		{
			Flash::error('Cámara no encontrada.');

			return redirect(route('catCamaras.index'));
		}

		return view('catCamaras.show')->with('catCamara', $catCamara);
	}

	/**
	 * Show the form for editing the specified catCamara.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$catCamara = $this->catCamaraRepository->find($id);

		if(empty($catCamara))
		{
			Flash::error('Cámara no encontrada.');

			return redirect(route('catCamaras.index'));
		}

		return view('catCamaras.edit')->with('catCamara', $catCamara);
	}

	/**
	 * Update the specified catCamara in storage.
	 *
	 * @param  int              $id
	 * @param UpdatecatCamaraRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatecatCamaraRequest $request)
	{
		$catCamara = $this->catCamaraRepository->find($id);

		if(empty($catCamara))
		{
			Flash::error('Cámara no encontrada.');

			return redirect(route('catCamaras.index'));
		}

		$this->catCamaraRepository->updateRich($request->all(), $id);

		Flash::success('Cámara actualizada correctamente.');

		return redirect(route('catCamaras.index'));
	}

	/**
	 * Remove the specified catCamara from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$catCamara = $this->catCamaraRepository->find($id);

		if(empty($catCamara))
		{
			Flash::error('Cámara no encontrada.');

			return redirect(route('catCamaras.index'));
		}

		$this->catCamaraRepository->delete($id);

		Flash::success('Cámara eliminada correctamente.');

		return redirect(route('catCamaras.index'));
	}

	/**
	 * @return mixed
	 */
	public function data() {
		$camaras = catCamara::select('*');
		$camarasDT = Datatables::of($camaras)
			->addColumn('action', function($camara) {
				$actions = '<div class="text-center button-action">';
				$actions .= '<a href="' . route('catCamaras.edit', [$camara->id]) . '"><i class="fa fa-edit"></i></a>';
				$actions .= '<a href="' . route('catCamaras.delete', [$camara->id]) . '" onclick="return confirm(\'¿Confirmar la eliminación de cámara?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$actions .= '</div>';
				return $actions;
			})
			->make(true);
		return $camarasDT;
	}

}
