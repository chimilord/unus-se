<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\catComisiones;
use App\Models\CatStatus;
use App\Models\CatStatusPerson;
use App\Models\CatTipoEleccion;
use App\Models\catTipoPersona;
use App\Models\Comision;
use App\Models\Estado;
use App\Models\Initiative;
use App\Models\Partido;
use DB;
use Request;
use Response;
use Mitul\Controller\AppBaseController as AppBaseController;

class SearchController extends AppBaseController
{

    /**
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search()
    {
        $estados = Estado::lists('nombre', 'id')->all();
        $estatus = CatStatusPerson::lists('estatus', 'id')->all();
        $partidos = Partido::lists('siglas', 'id')->all();
        $elecciones = CatTipoEleccion::lists('nombre', 'id')->all();
        $tipos = catTipoPersona::lists('nombre', 'id')->all();
        return view('search.index', array(
            'estados' => $estados,
            'estatus' => $estatus,
            'partidos' => $partidos,
            'elecciones' => $elecciones,
            'tipos' => $tipos
        ));
    }

    /**
     * @return mixed
     */
    public function find()
    {
        $persons = DB::table('person')->select('person.*');

        if (Request::has('nombre')) {
            $nombre = Request::input('nombre');
            $persons->where('person.nombre', 'like', '%' . $nombre . '%');
        }

        if (Request::has('apellidos')) {
            $apelidos = Request::input('apellidos');
            $persons->where('person.apellidos', 'like', '%' . $apelidos . '%');
        }

        if (Request::has('idTipoPersona')) {
            $tipo = Request::input('idTipoPersona');
            $persons->join('catTipoPersonas', 'person.idTipoPersona', '=', 'catTipoPersonas.id')
                ->where('person.idTipoPersona', '=', $tipo);
        }

        if (Request::has('idEstado') && Request::input('idEstado') > 0) {
            $estado = Request::input('idEstado');
            $persons->join('estados', 'person.idEstado', '=', 'estados.id')
                ->where('person.idEstado', '=', $estado);
        }

        if (Request::has('idPartido') && Request::input('idPartido') > 0) {
            $partido = Request::input('idPartido');
            $persons->join('partidos', 'person.idPartido', '=', 'partidos.id')
                ->where('person.idPartido', '=', $partido);
        }

        if (Request::has('idEstatus') && Request::input('idEstatus') > 0) {
            $estatus = Request::input('idEstatus');
            $persons->join('catStatusPeople', 'person.idStatusPerson', '=', 'catStatusPeople.id')
                ->where('person.idStatusPerson', '=', $estatus);
        }

        if (Request::has('idTipoEleccion') && Request::input('idTipoEleccion') > 0) {
            $eleccion = Request::input('idTipoEleccion');
            $persons->join('catTipoEleccions', 'person.idTipoEleccion', '=', 'catTipoEleccions.id')
                ->where('person.idTipoEleccion', '=', $eleccion);
        }

        $result = $persons->get();
        return response()->json($result);
    }

    /**
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function entity()
    {
        return view('search.entity.index', array());
    }

    /**
     * @return mixed
     */
    public function findInEntities()
    {

        $ids = [];

        if (Request::has('entity')) {
            $entity = Request::input('entity');
        }

        $searchEntity = DB::table($entity)->select('id', 'keywords')->get();

        if (Request::has('keywords')) {
            $keywords = str_replace(", ", ",", strtolower(Request::input('keywords')));
            $keywords = $this->removeESChars($keywords);
            if (strpos($keywords, ",")) {
                $keysToSearch = explode(",", $keywords);
                foreach ($keysToSearch as $keyword) {
                    foreach ($searchEntity as $entityKey) {
                        if (strpos(strtolower($this->removeESChars($entityKey->keywords)), $keyword) !== false) {
                            array_push($ids, $entityKey->id);
                        }
                    }
                }
            } else {
                foreach ($searchEntity as $entityKey) {
                    if (strpos(strtolower($this->removeESChars($entityKey->keywords)), $keywords) !== false) {
                        array_push($ids, $entityKey->id);
                    }
                }
            }
        }

        if (Request::has('logical')) {
            $entityIds = array_unique(array_diff_assoc($ids, array_unique($ids)));
        } else {
            $entityIds = $ids;
        }

        $results = array();
        $resultEntity = DB::table($entity)->whereIn('id', $entityIds)->get();

        if ($entity === 'initiatives') {

            $comisiones_id = [];
            $partidos_id   = [];
            $cargos_id     = [];
            $estatus_id    = [];

            foreach ($resultEntity as $entityRecord) {
                $findInitiative = Initiative::find($entityRecord->id);
                $persons = $findInitiative->persons()->get();
                $comisiones = $findInitiative->comisiones()->get();
                $status = CatStatus::find($entityRecord->status_id);
                foreach($comisiones as $comision) {
                    $comisiones_id[] = $comision->idCatComision;
                }
                foreach ($persons as $person) {
                    $partidos_id[] = $person->idPartido;
                    $cargos_id[] = $person->pivot->position_id;
                }
                $turnados = DB::table('catComisiones')->whereIn('id', $comisiones_id)->get();
                $partidos = DB::table('partidos')->whereIn('id', $partidos_id)->get();
                $cargos = DB::table('positions')->whereIn('id', $cargos_id)->get();
                $findInitiative->persons = $persons;
                $findInitiative->status = $status->name;
                $findInitiative->submit_date = $this->showDate($findInitiative->submit_date);
                $findInitiative->publication_date = $this->showDate($findInitiative->publication_date);
                $findInitiative->fechaDecreto = $this->showDate($findInitiative->fechaDecreto);
                $findInitiative->turnados = $turnados;
                $findInitiative->partidos = $partidos;
                $findInitiative->comisiones = $comisiones;
                $findInitiative->cargos = $cargos;
                $results[] = $findInitiative;
            }
        } else if ($entity === 'notes') {
            foreach ($resultEntity as $entityRecord) {
                $persons = DB::table('person')->where('id', $entityRecord->idPersona)->get();
                $entityRecord->persons = $persons;
                $results[] = $entityRecord;
            }
        } else if ($entity === 'comisions') {
            foreach ($resultEntity as $entityRecord) {
                $findCommission = Comision::find($entityRecord->id);
                $nameCommission = catComisiones::find($entityRecord->idCatComision);
                $persons = $findCommission->persons()->get();
                $iniciativas = $findCommission->iniciativas()->get();
                foreach ($persons as $person) {
                    $partidos_id[]  = $person->idPartido;
                    $cargos_id[] = $person->pivot->position_id;
                }
                foreach ($iniciativas as $iniciativa) {
                    $estatus_id[] = $iniciativa->status_id;
                }
                $partidos = DB::table('partidos')->whereIn('id', $partidos_id)->get();
                $cargos = DB::table('positions')->whereIn('id', $cargos_id)->get();
                $estatus = DB::table('catStatuses')->whereIn('id', $estatus_id)->get();
                $findCommission->persons = $persons;
                $findCommission->iniciativas = $iniciativas;
                $findCommission->nameCommission = $nameCommission->nombre;
                $findCommission->partidos = $partidos;
                $findCommission->cargos = $cargos;
                $findCommission->estatus = $estatus;
                $results[] = $findCommission;
            }
        }
        return response()->json(['entities' => $results, 'type' => $entity]);
    }

    /**
     * @param $date
     * @return string
     */
    public function formatDate($date)
    {
        if (stripos($date, "/")) {
            $format = explode("/", $date);
            return $format[2] . '-' . $format[0] . '-' . $format[1];
        }
        return $date;
    }

    /**
     * @param $date
     * @return string
     */
    public function showDate($date)
    {
        if (stripos($date, "-")) {
            $format = explode("-", $date);
            return $format[1] . '/' . $format[2] . '/' . $format[0];
        }
        return $date;
    }

    /**
     * @param $eswrod
     * @return clean word
     */
    public function removeESChars($eswrod)
    {
        $unwanted_array = array(
            'Á' => 'A', 'É' => 'E', 'Í' => 'I', 'Ó' => 'O', 'Ú' => 'U',
            'á' => 'a', 'é' => 'e', 'í' => 'i', 'ó' => 'o', 'ú' => 'u',
            'ñ' => 'n');
        $str = strtr($eswrod, $unwanted_array);
        return $str;
    }

}