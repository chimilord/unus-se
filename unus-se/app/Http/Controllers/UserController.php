<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\UpdateUserRequest;
use App\Libraries\Repositories\UserRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;
use DB;
use App\User;
use App\Role;
use Validator;
use Illuminate\Support\Facades\Input;
use Hash;



class UserController extends AppBaseController
{

	/** @var  userRepository*/
	private $userRepository;

	function __construct(UserRepository $userRepository)
	{
        $this->middleware('auth');
		$this->userRepository = $userRepository;
	}


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data,$id)
    {
		$user = $this->userRepository->find($id);
		Validator::extend('checkHashedPass',function($attribute, $value, $parameters)
		{
		    if(!Hash::check($value,$parameters[0]))
		    {
		        return false;
		    }
		    return true;
		});
		$messages = array('oldpassword.check_hashed_pass' => 'El password actual no coincide' ,'oldpassword.required'=>"El campo password actual es requerido");
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'lastname' => 'required|min:4',
            'avatar' => 'min:10|max:255',
            'phone' => 'min:10',
            'role_id' => 'required',
            'password' => 'sometimes|min:6',
            'oldpassword' => 'required|checkHashedPass:'.$user->password,
        ], $messages);
    }

	/**
	 * Display a listing of the catCamara.
	 *
	 * @return Response
	 */
	public function index()
	{
		$usuarios = $this->userRepository->paginate(10);
		return view('users.index')
			->with('usuarios', $usuarios);
	}

	/**
	 * Show the form for creating a new catCamara.
	 *
	 * @return Response
	 */
	public function create()
	{
		$roles = Role::lists('name', 'id');

		return view('users.create',array('roles'=>$roles));
	}

	/**
	 * Store a newly created catCamara in storage.
	 *
	 * @param CreatecatCamaraRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateUserRequest $request)
	{
		$input = $request->all();

		$user = $this->userRepository->create($input);

		Flash::success('Usuario de sistema creado correctamente.');

		return redirect(route('users.index'));
	}

	/**
	 * Display the specified catCamara.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$roles = Role::lists('name', 'id');
        unset($roles[1]);
		$user = $this->userRepository->find($id);
		if(empty($user))
		{
			Flash::error('Usuario de sistema no encontrado.');

			return redirect(route('users.index'));
		}
		return view('users.show',array('roles'=>$roles))->with('user', $user);
	}

	/**
	 * Show the form for editing the specified catCamara.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$user = $this->userRepository->find($id);
		$roles = Role::lists('name', 'id');
        unset($roles[1]);
		if(empty($user))
		{
			Flash::error('Usuario de sistema no encontrado.');
			return redirect(route('users.index'));
		}
		return view('users.edit',array('roles'=>$roles,'old'=>$user))->with('user', $user);
	}

	/**
	 * Update the specified catCamara in storage.
	 *
	 * @param  int              $id
	 * @param UpdatecatCamaraRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateUserRequest $request)
	{
		$data=$request->all();
		$validador = $this->validator($request->all(),$id);
        if ($validador->fails()) {
            $this->throwValidationException(
                $request, $validador
            );
        }
		$user = $this->userRepository->find($id);
		if(empty($user))
		{
			Flash::error('Usuario de sistema no encontrado.');

			return redirect(route('users.index'));
		}
		
		if (isset($data['password']) && $data['password']!=''){
			$data['password']=Hash::make($data['password']);
		}
		else{
			$data['password']=Hash::make($data['oldpassword']);	
		}

		$this->userRepository->updateRich($data, $id);
       if (array_key_exists('avatar', $data) && $data['avatar']!='anterior') {
            $destinationPath = 'uploads'; // upload path
            Input::file('avatar')->move($destinationPath, "user_".$user->id . ".png"); // uploading file to given path
        }
		Flash::success('Usuario de sistema actualizado correctamente.');
		return redirect(route('users.index'));
	}

	/**
	 * Remove the specified catCamara from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = $this->userRepository->find($id);

		if(empty($user))
		{
			Flash::error('Usuario de sistema no encontrado.');

			return redirect(route('users.index'));
		}

		$this->userRepository->delete($id);

		Flash::success('Usuario de sistema eliminado correctamente.');

		return redirect(route('users.index'));
	}

	/**
	 * @return mixed
	 */
	public function data() {
		$usuarios = DB::table('users')->join('role', 'users.role_id', '=', 'role.id')
			->select(['users.id', 'users.name', 'users.lastname', 'users.email', 'users.phone', DB::raw('role.name as roleName')]);
		$usuariosDT = Datatables::of($usuarios)
			->editColumn('name', '{!! $name !!}' . ' ' . '{!! $lastname !!}')
			->addColumn('action', function($usuario) {
				$actions = '<div class="text-center button-action">';
				$actions .= '<a href="' . route('users.edit', [$usuario->id]) . '"><i class="fa fa-edit"></i></a>';
				$actions .= '<a href="' . route('users.delete', [$usuario->id]) . '" onclick="return confirm(\'¿Confirmar la eliminación del Usuario?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$actions .= '</div>';
				return $actions;
			})
			->make(true);

		return $usuariosDT;
	}
}