<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatecatDistritosRequest;
use App\Http\Requests\UpdatecatDistritosRequest;
use App\Libraries\Repositories\catDistritosRepository;
use App\Models\catDistritos;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;

class catDistritosController extends AppBaseController
{

	/** @var  catDistritosRepository */
	private $catDistritosRepository;

	function __construct(catDistritosRepository $catDistritosRepo)
	{
        $this->middleware('rolauth');		
		$this->catDistritosRepository = $catDistritosRepo;
	}

	/**
	 * Display a listing of the catDistritos.
	 *
	 * @return Response
	 */
	public function index()
	{
		$catDistritos = $this->catDistritosRepository->paginate(10);

		return view('catDistritos.index')
			->with('catDistritos', $catDistritos);
	}

	/**
	 * Show the form for creating a new catDistritos.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('catDistritos.create');
	}

	/**
	 * Store a newly created catDistritos in storage.
	 *
	 * @param CreatecatDistritosRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatecatDistritosRequest $request)
	{
		$input = $request->all();

		$catDistritos = $this->catDistritosRepository->create($input);

		Flash::success('Distrito creado correctamente.');

		return redirect(route('catDistritos.index'));
	}

	/**
	 * Display the specified catDistritos.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$catDistritos = $this->catDistritosRepository->find($id);

		if(empty($catDistritos))
		{
			Flash::error('Distrito no encontrado.');

			return redirect(route('catDistritos.index'));
		}

		return view('catDistritos.show')->with('catDistritos', $catDistritos);
	}

	/**
	 * Show the form for editing the specified catDistritos.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$catDistritos = $this->catDistritosRepository->find($id);

		if(empty($catDistritos))
		{
			Flash::error('Distrito no encontrado.');

			return redirect(route('catDistritos.index'));
		}

		return view('catDistritos.edit')->with('catDistritos', $catDistritos);
	}

	/**
	 * Update the specified catDistritos in storage.
	 *
	 * @param  int              $id
	 * @param UpdatecatDistritosRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatecatDistritosRequest $request)
	{
		$catDistritos = $this->catDistritosRepository->find($id);

		if(empty($catDistritos))
		{
			Flash::error('Distrito no encontrado.');

			return redirect(route('catDistritos.index'));
		}

		$this->catDistritosRepository->updateRich($request->all(), $id);

		Flash::success('Distrito actualizado correctamente.');

		return redirect(route('catDistritos.index'));
	}

	/**
	 * Remove the specified catDistritos from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$catDistritos = $this->catDistritosRepository->find($id);

		if(empty($catDistritos))
		{
			Flash::error('Distrito no encontrado.');

			return redirect(route('catDistritos.index'));
		}

		$this->catDistritosRepository->delete($id);

		Flash::success('Distrito eliminado correctamente.');

		return redirect(route('catDistritos.index'));
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function data() {
		$distritos = catDistritos::select('*');
		$distritosDT = Datatables::of($distritos)
			->addColumn('action', function ($distrito) {
				$actions = '<div class="text-center button-action">';
				$actions .= '<a href="' . route('catDistritos.edit', [$distrito->id]) . '"><i class="fa fa-edit"></i></a>';
				$actions .= '<a href="' . route('catDistritos.delete', [$distrito->id]) . '" onclick="return confirm(\'¿Confirmar la eliminación de Distrito?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$actions .= '</div>';
				return $actions;
			})
			->make(true);
		return $distritosDT;
	}

}
