<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatecomiteRequest;
use App\Http\Requests\UpdatecomiteRequest;
use App\Libraries\Repositories\comiteRepository;
use App\Models\comite;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Yajra\Datatables\Datatables;
use DB;
use Illuminate\Database\Eloquent\Model;

class comiteController extends AppBaseController
{

	/** @var  comiteRepository */
	private $comiteRepository;

	function __construct(comiteRepository $comiteRepo)
	{
        $this->middleware('rolauth');		
		$this->comiteRepository = $comiteRepo;
	}

	/**
	 * Display a listing of the comite.
	 *
	 * @return Response
	 */
	public function index()
	{
		$comites = $this->comiteRepository->paginate(10);

		return view('comites.index')
			->with('comites', $comites);
	}

	/**
	 * Show the form for creating a new comite.
	 *
	 * @return Response
	 */
	public function create()
	{
		$positions = \DB::table('positions')->lists('nombre', 'id');
		$persons = \DB::table('person')->selectRaw('CONCAT(nombre,\' \',apellidos) as fulln,id' )->lists('fulln', 'id');
		$idCatComite = \DB::table('catComites')->lists('nombre', 'id');
		$idPeriodo = \DB::table('catPeriodos')->lists('nombre', 'id');
		return view('comites.create', array('persons'=>$persons, 'positions' => $positions,  'idCatComite' => $idCatComite, 'idPeriodo'=>$idPeriodo));
	}


	/**
	 * Store a newly created comite in storage.
	 *
	 * @param CreatecomiteRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatecomiteRequest $request)
	{
		$input = $request->all();
		$persons = $input['persons'];
		$positions = $input['positions'];
		$i=0;
		foreach($persons as $key => $value) {
            $persons[$value] =  ["position_id"=>$positions[$i++]];
        }
		$comite = $this->comiteRepository->create($input);
		$comite->persons()->sync($persons);
		Flash::success('Comité creado correctamente.');
		return redirect(route('comites.index'));
	}

	/**
	 * Display the specified comite.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{

		$comite = $this->comiteRepository->find($id);
		$positions = \DB::table('positions')->lists('nombre', 'id');
		$persons =$comite->persons()->get();
		if(empty($comite))
		{
			Flash::error('Comité no encontrado.');

			return redirect(route('comites.index'));
		}
		else {
			$catComite = DB::table('catComites')
				->select('catComites.*')
				->where('catComites.id', '=', $comite->idCatComite)->get();
			$periodo = DB::table('catPeriodos')->where('id', $comite->idPeriodo)->get();
		}
		return view('comites.show', array('comite'=>$comite, 'persons'=>$persons, 'positions' => $positions,  'catComite'=>$catComite[0], 'periodo'=>$periodo[0]->nombre));
	}

	/**
	 * Show the form for editing the specified comite.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{


		$comite = $this->comiteRepository->find($id);

		$positions = \DB::table('positions')->lists('nombre', 'id');
		$persons = \DB::table('person')->selectRaw('CONCAT(nombre,\' \',apellidos) as fulln,id' )->lists('fulln', 'id');
		$idCatComite = \DB::table('catComites')->lists('nombre', 'id');
		$selectedPersons =$comite->persons()->get();
		$idPeriodo = \DB::table('catPeriodos')->lists('nombre', 'id');

		if(empty($comite))
		{
			Flash::error('Comité no encontrado.');

			return redirect(route('comites.index'));
		}
		else{
			$positions = \DB::table('positions')->lists('nombre', 'id');
			$persons = \DB::table('person')->selectRaw('CONCAT(nombre,\' \',apellidos) as fulln,id' )->lists('fulln', 'id');
			$selectedPersons = DB::table('person_comite')
				->join('person', 'person_comite.person_id', '=', 'person.id')
				->join('comites', 'person_comite.comite_id', '=', 'comites.id')
				->join('positions', 'person_comite.position_id', '=', 'positions.id')
				->select('person.id', 'person.nombre', 'person.apellidos', DB::raw('positions.nombre as cargo, positions.id as cargo_id'))
				->where('person_comite.comite_id', '=', $comite->id)
				->get();

		}
		return view('comites.edit', array('selectedPersons'=>$selectedPersons,'persons'=>$persons, 'positions' => $positions,'idCatComite' => $idCatComite,'idPeriodo' => $idPeriodo))->with('comite', $comite);
	}

	/**
	 * Update the specified comite in storage.
	 *
	 * @param  int              $id
	 * @param UpdatecomiteRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatecomiteRequest $request)
	{
		$input = $request->all();
		$comite = $this->comiteRepository->find($id);
		$persons = $input['persons'];
		$positions = $input['positions'];
		$i=0;
		foreach($persons as $key => $value) {
            $persons[$value] =  ["position_id"=>$positions[$i++]];
        }

		if(empty($comite))
		{
			Flash::error('Comité no encontrado.');

			return redirect(route('comites.index'));
		}
		$comite->persons()->sync($persons);
		$this->comiteRepository->updateRich($request->all(), $id);

		Flash::success('Comité actualizado correctamente.');

		return redirect(route('comites.index'));
	}

	/**
	 * Remove the specified comite from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$comite = $this->comiteRepository->find($id);

		if(empty($comite))
		{
			Flash::error('Comité no encontrado.');

			return redirect(route('comites.index'));
		}

		$this->comiteRepository->delete($id);

		Flash::success('Comité eliminado correctamente.');

		return redirect(route('comites.index'));
	}

	/**
	 * @return mixed
	 */
	public function data() {
		$comite = DB::table('comites')
			->join('catComites', 'comites.idCatComite', '=', 'catComites.id')
			->join('catPeriodos', 'comites.idPeriodo', '=', 'catPeriodos.id')
			->select(['comites.id',  'catComites.nombre', DB::raw('catPeriodos.nombre as periodo')]);
		$comitesDT = Datatables::of($comite)
			->addColumn('action', function ($comite) {
				$buttons = '<div class="text-center button-action">';
				$buttons .= '<a href="' . route('comites.edit', [$comite->id]) . '"><i class="fa fa-edit"></i></a>';
				$buttons .= '<a href="' . route('comites.delete', [$comite->id]) . '" onclick="return confirm(\'¿Confirmar la eliminación del Comite?\')"><i class="fa fa-trash-o icon-red"></i></a>';
				$buttons .= '</div>';
				return $buttons;
			})
			->make(true);
		return $comitesDT;
	}
}
