<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where all API routes are defined.
|
*/

Route::resource("estados", "EstadoAPIController");

Route::resource("partidos", "PartidoAPIController");

Route::resource("persons", "PersonAPIController");

Route::resource("documentTypes", "DocumentTypeAPIController");

Route::resource("positions", "PositionAPIController");

Route::resource("comisions", "ComisionAPIController");

Route::resource("notes", "NoteAPIController");