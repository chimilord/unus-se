<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => 'web'], function () {
    Route::group(['middleware' => 'rolauth'], function () {
        Route::get('/register', [
            'as' => 'auth.register',
            'uses' => 'Auth\AuthController@getRegister',
            'roles' => ['administrator'],
        ]);

    });

    Route::auth();
    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/home', 'HomeController@index');
    Route::get('/home/notes', 'HomeController@calendarNotes');

    Route::post('/register', [
        'as' => 'auth.register',
        'uses' => 'Auth\AuthController@postRegister',
        'roles' => ['administrator'],
    ]);
    Route::get('/logout', [
        'as' => 'auth.logout',
        'uses' => 'Auth\AuthController@getLogout',
        'roles' => ['administrator', 'manager', 'user'],
    ]);
    Route::get('/login', [
        'as' => 'auth.login',
        'uses' => 'Auth\AuthController@getLogin',
    ]);
    Route::post('/login', [
        'as' => 'auth.login',
        'uses' => 'Auth\AuthController@postLogin',
    ]);

    Route::get('estados/data', [
        'as' => 'estados.data',
        'uses' => 'EstadoController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('estados', 'EstadoController', ['except' => [
        'data'
    ]]);

    Route::get('estados/{id}/delete', [
        'as' => 'estados.delete',
        'uses' => 'EstadoController@destroy',
        'roles' => ['administrator'],

    ]);
    Route::get('estados/create', [
        'as' => 'estados.create',
        'uses' => 'EstadoController@create',
        'roles' => ['administrator', 'manager'],

    ]);

    Route::get('partidos/data', [
        'as' => 'partidos.data',
        'uses' => 'PartidoController@data',
        'roles' => ['administrator', 'manager', 'user'],

    ]);

    Route::resource('partidos', 'PartidoController', ['except' => [
        'data']]);

    Route::get('partidos/{id}/delete', [
        'as' => 'partidos.delete',
        'uses' => 'PartidoController@destroy',
        'roles' => ['administrator'],

    ]);
    Route::get('partidos/create', [
        'as' => 'partidos.create',
        'uses' => 'PartidoController@create',
        'roles' => ['administrator', 'manager'],
    ]);

    Route::get('catStatus/data', [
        'as' => 'catStatus.data',
        'uses' => 'CatStatusController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('catStatus', 'CatStatusController');

    Route::get('catStatus/{id}/delete', [
        'as' => 'catStatus.delete',
        'uses' => 'CatStatusController@destroy',
        'roles' => ['administrator'],
    ]);

    Route::get('catStatus/create', [
        'as' => 'catStatus.create',
        'uses' => 'CatStatusController@create',
        'roles' => ['administrator', 'manager'],
    ]);

    Route::get('person/data', [
        'as' => 'person.data',
        'uses' => 'PersonController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('person', 'PersonController', ['except' => [
        'data'
    ]]);

    Route::get('person/{id}/delete', [
        'as' => 'person.delete',
        'uses' => 'PersonController@destroy',
        'roles' => ['administrator'],
    ]);

    Route::get('person/create', [
        'as' => 'person.create',
        'uses' => 'PersonController@create',
        'roles' => ['administrator', 'manager'],
    ]);

    Route::post('person/{id}/tracking', 'PersonController@updateTracking');

    Route::get('initiatives/data', [
        'as' => 'initiatives.data',
        'uses' => 'InitiativeController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);


    Route::resource('initiatives', 'InitiativeController', ['except' => [
        'data']]);

    Route::get('initiatives/{id}/delete', [
        'as' => 'initiatives.delete',
        'uses' => 'InitiativeController@destroy',
        'roles' => ['administrator'],
    ]);

    Route::get('initiatives/create', [
        'as' => 'initiatives.create',
        'uses' => 'InitiativeController@create',
        'roles' => ['administrator', 'manager'],
    ]);

    Route::get('catStatusPeople/data', [
        'as' => 'catStatusPeople.data',
        'uses' => 'CatStatusPersonController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('catStatusPeople', 'CatStatusPersonController');

    Route::get('catStatusPeople/{id}/delete', [
        'as' => 'catStatusPeople.delete',
        'uses' => 'CatStatusPersonController@destroy',
        'roles' => ['administrator'],
    ]);

    Route::get('catStatusPeople/create', [
        'as' => 'catStatusPeople.create',
        'uses' => 'CatStatusPersonController@create',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::get('catTipoEleccions/data', [
        'as' => 'catTipoEleccions.data',
        'uses' => 'catTipoEleccionController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('catTipoEleccions', 'catTipoEleccionController');

    Route::get('catTipoEleccions/{id}/delete', [
        'as' => 'catTipoEleccions.delete',
        'uses' => 'catTipoEleccionController@destroy',
        'roles' => ['administrator'],

    ]);

    Route::get('catTipoEleccions/create', [
        'as' => 'catTipoEleccions.create',
        'uses' => 'catTipoEleccionController@create',
        'roles' => ['administrator', 'manager'],

    ]);


    Route::get('positions/data', [
        'as' => 'positions.data',
        'uses' => 'PositionController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('positions', 'PositionController');

    Route::get('positions/{id}/delete', [
        'as' => 'positions.delete',
        'uses' => 'PositionController@destroy',
        'roles' => ['administrator'],

    ]);

    Route::get('positions/create', [
        'as' => 'positions.create',
        'uses' => 'PositionController@create',
        'roles' => ['administrator', 'manager'],
    ]);

    Route::get('catComisiones/data', [
        'as' => 'catComisiones.data',
        'uses' => 'catComisionesController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('catComisiones', 'catComisionesController', ['except' => [
        'data'
    ]]);

    Route::get('catComisiones/{id}/delete', [
        'as' => 'catComisiones.delete',
        'uses' => 'catComisionesController@destroy',
        'roles' => ['administrator'],

    ]);

    Route::get('catComisiones/create', [
        'as' => 'catComisiones.create',
        'uses' => 'catComisionesController@create',
        'roles' => ['administrator', 'manager'],

    ]);

    Route::get('catDistritos/data', [
        'as' => 'catDistritos.data',
        'uses' => 'catDistritosController@data',
        'roles' => ['administrator', 'manager', 'user'],

    ]);
    Route::resource('catDistritos', 'catDistritosController');

    Route::get('catDistritos/{id}/delete', [
        'as' => 'catDistritos.delete',
        'uses' => 'catDistritosController@destroy',
        'roles' => ['administrator'],

    ]);
    Route::get('catDistritos/create', [
        'as' => 'catDistritos.create',
        'uses' => 'catDistritosController@create',
        'roles' => ['administrator', 'manager'],

    ]);

    Route::get('catCamaras/data', [
        'as' => 'catCamaras.data',
        'uses' => 'catCamaraController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('catCamaras', 'catCamaraController');

    Route::get('catCamaras/{id}/delete', [
        'as' => 'catCamaras.delete',
        'uses' => 'catCamaraController@destroy',
        'roles' => ['administrator'],

    ]);

    Route::get('catCamaras/create', [
        'as' => 'catCamaras.create',
        'uses' => 'catCamaraController@create',
        'roles' => ['administrator', 'manager'],

    ]);

    Route::get('catTipoPersonas/data', [
        'as' => 'catTipoPersonas.data',
        'uses' => 'catTipoPersonaController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('catTipoPersonas', 'catTipoPersonaController', ['except' => ['data']]);

    Route::get('catTipoPersonas/{id}/delete', [
        'as' => 'catTipoPersonas.delete',
        'uses' => 'catTipoPersonaController@destroy',
        'roles' => ['administrator'],

    ]);
    Route::get('catTipoPersonas/create', [
        'as' => 'catTipoPersonas.create',
        'uses' => 'catTipoPersonaController@create',
        'roles' => ['administrator', 'manager'],

    ]);

    Route::get('comisions/data', [
        'as' => 'comisions.data',
        'uses' => 'ComisionController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('comisions', 'ComisionController');

    Route::get('comisions/{id}/delete', [
        'as' => 'comisions.delete',
        'uses' => 'ComisionController@destroy',
        'roles' => ['administrator'],

    ]);

    Route::get('comisions/create', [
        'as' => 'comisions.create',
        'uses' => 'ComisionController@create',
        'roles' => ['administrator', 'manager'],

    ]);

    Route::get('notes/data', [
        'as' => 'notes.data',
        'uses' => 'NoteController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('notes', 'NoteController');

    Route::get('notes/{id}/delete', [
        'as' => 'notes.delete',
        'uses' => 'NoteController@destroy',
        'roles' => ['administrator'],

    ]);
    Route::get('notes/create', [
        'as' => 'notes.create',
        'uses' => 'NoteController@create',
        'roles' => ['administrator', 'manager'],

    ]);

    Route::get('catTipoComisions/data', [
        'as' => 'catTipoComisions.data',
        'uses' => 'catTipoComisionController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('catTipoComisions', 'catTipoComisionController');

    Route::get('catTipoComisions/{id}/delete', [
        'as' => 'catTipoComisions.delete',
        'uses' => 'catTipoComisionController@destroy',
        'roles' => ['administrator'],

    ]);

    Route::get('catTipoComisions/create', [
        'as' => 'catTipoComisions.create',
        'uses' => 'catTipoComisionController@create',
        'roles' => ['administrator', 'manager'],

    ]);

    Route::get('catPeriodos/data', [
        'as' => 'catPeriodos.data',
        'uses' => 'catPeriodoController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('catPeriodos', 'catPeriodoController');

    Route::get('catPeriodos/{id}/delete', [
        'as' => 'catPeriodos.delete',
        'uses' => 'catPeriodoController@destroy',
        'roles' => ['administrator'],
    ]);
    Route::get('catPeriodos/create', [
        'as' => 'catPeriodos.create',
        'uses' => 'catPeriodoController@create',
        'roles' => ['administrator', 'manager'],

    ]);


    Route::get('catStatusTurnados/data', [
        'as' => 'catStatusTurnados.data',
        'uses' => 'catStatusTurnadoController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('catStatusTurnados', 'catStatusTurnadoController');

    Route::get('catStatusTurnados/{id}/delete', [
        'as' => 'catStatusTurnados.delete',
        'uses' => 'catStatusTurnadoController@destroy',
        'roles' => ['administrator'],
    ]);

    Route::get('catStatusTurnados/create', [
        'as' => 'catStatusTurnados.create',
        'uses' => 'catStatusTurnadoController@create',
        'roles' => ['administrator', 'manager'],

    ]);

    Route::get('catOrganoGobiernos/data', [
        'as' => 'catOrganoGobiernos.data',
        'uses' => 'catOrganoGobiernoController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('catOrganoGobiernos', 'catOrganoGobiernoController');

    Route::get('catOrganoGobiernos/{id}/delete', [
        'as' => 'catOrganoGobiernos.delete',
        'uses' => 'catOrganoGobiernoController@destroy',
        'roles' => ['administrator'],
    ]);


    Route::get('catOrganoGobiernos/create', [
        'as' => 'catOrganoGobiernos.create',
        'uses' => 'catOrganoGobiernoController@create',
        'roles' => ['administrator', 'manager'],

    ]);


    Route::get('organoGobiernos/data', [
        'as' => 'organoGobiernos.data',
        'uses' => 'OrganoGobiernoController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('organoGobiernos', 'OrganoGobiernoController');

    Route::get('organoGobiernos/{id}/delete', [
        'as' => 'organoGobiernos.delete',
        'uses' => 'OrganoGobiernoController@destroy',
        'roles' => ['administrator'],

    ]);


    Route::get('organoGobiernos/create', [
        'as' => 'organoGobiernos.create',
        'uses' => 'OrganoGobiernoController@create',
        'roles' => ['administrator', 'manager'],
    ]);


    Route::get('catComites/data', [
        'as' => 'catComites.data',
        'uses' => 'catComiteController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);


    Route::resource('catComites', 'catComiteController');

    Route::get('catComites/{id}/delete', [
        'as' => 'catComites.delete',
        'uses' => 'catComiteController@destroy',
        'roles' => ['administrator'],
    ]);

    Route::get('catComites/create', [
        'as' => 'catComites.create',
        'uses' => 'catComiteController@create',
        'roles' => ['administrator', 'manager'],
    ]);

    Route::get('comites/data', [
        'as' => 'comites.data',
        'uses' => 'comiteController@data',
        'roles' => ['administrator', 'manager', 'user'],
    ]);

    Route::resource('comites', 'comiteController');

    Route::get('comites/{id}/delete', [
        'as' => 'comites.delete',
        'uses' => 'comiteController@destroy',
        'roles' => ['administrator'],
    ]);

    Route::get('comites/create', [
        'as' => 'comites.create',
        'uses' => 'comiteController@create',
        'roles' => ['administrator', 'manager'],
    ]);


    Route::get('users/data', [
        'as' => 'users.data',
        'uses' => 'UserController@data',
        'roles' => ['administrator', 'manager'],
    ]);

    Route::resource('users', 'UserController');

    Route::get('users/{id}/delete', [
        'as' => 'users.delete',
        'uses' => 'UserController@destroy',
        'roles' => ['administrator'],        

    ]);

    Route::get('users/create', [
        'as' => 'users.create',
        'uses' => 'UserController@create',
        'roles' => ['administrator'],

    ]);

    // Search Controller
    Route::get('/search', [
        'middleware' => 'auth',
        'uses' => 'SearchController@search'
    ]);
    Route::post('/search', 'SearchController@find');
    Route::get('/search/entity', [
        'middleware' => 'auth',
        'uses' => 'SearchController@entity'
    ]);
    Route::post('/search/entity', 'SearchController@findInEntities');

});

Route::get('/', function () {
    return redirect('login');
});

/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'api', 'namespace' => 'API'], function () {
    Route::group(['prefix' => 'v1'], function () {
        require Config::get('generator.path_api_routes');
    });
});


