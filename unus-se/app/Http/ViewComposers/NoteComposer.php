<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Libraries\Repositories\NoteRepository;
use Illuminate\Support\Facades\Auth;
use DB;

class NoteComposer
{

    protected $notes;

    public function __construct(NoteRepository $notes)
    {
        $this->notes = $notes;
    }

    public function compose(View $view)
    {
        if (Auth::check()) {
            $countNotesByUser = DB::table('notes')->where([
                ['idUser', Auth::user()->id],
                ['status', '>', 0]
            ])->count();
            $notesByUser = DB::table('notes')->where([
                ['idUser', Auth::user()->id],
                ['status', '>', 0]
            ])->get();
            $view->with(['countNotes' => $countNotesByUser, 'userNotes' => $notesByUser]);
        }
    }

}